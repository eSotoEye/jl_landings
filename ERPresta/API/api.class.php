<?php

    ////
    ////	Joshua Leiva - soporte@teletiendadirecto.com
    ////	
    ////	API de la aplicación ERPresta 
    ////
    ////    Todos los derechos reservados
    ////    Copyright 2015
    ////    
    
    //Clase principal
	class ERPrestaAPI
	{  
	 
        //Parámetros
		const CURRENTVERSION = "1.00";                  //Versión de la API
        const DEBUGMODE = TRUE;                         //Indica si está o no en versión DEBUG y muestra o no los errores
        
        //Parámetros de la web
        const APIKEY = "MLHAoxLbDQdOcTW";                   //Clave de acceso para la API
        const PS = "ps_";                                   //Cabecera de las tablas de Prestashop en la BBDD
        const COUNTRY = "ES";                               //País donde estará alojada la API
            
        private $tablasC;                                   //Tablas que se pueden consultar
        private $camposC;                                   //Campos que se pueden consultar
        private $tablasU;                                   //Tablas que se pueden actualizar
        private $camposU;                                   //Campos que se pueden actualizar
                
        //Constructor   
		public function __construct()
		{   
            if($this::DEBUGMODE){
                error_reporting(E_ALL & ~E_NOTICE);
                ini_set("display_errors", 1);
            }
            
            require('config.php');
			$this->link = mysqli_connect($hostname, $user, $password, $dbname) or DIE( "Error 1000. Error en la conexión con la base de datos." );
			$this->link->set_charset("utf8");
            $this->initArrays();
		}
        
        
        //Recoge excepciones
		public function throwException($message = null, $code = null) {
			throw new Exception($message, $code);
		}
        
        /**
		 * Función que inicializa los arrays de tablas y campos		
		 *
		**/
		protected function initArrays() {
		  
            //Consultas
            $this->tablasC = array(0 => "product", 1 => "stock", 2 => "stock_available", 3 => "product_attribute", 4 => "product_lang", 5=> 'orders');
            
            $product = array("reference", "id_product", "active", "reference", "date_upd", "is_virtual", "price");
            $stock = array("reference", "id_product", "physical_quantity", "usable_quantity", "id_product_attribute");
            $stock_available = array("id_product","quantity");
            $product_attribute = array("reference","id_product");
			$product_lang = array("id_product","name");
			$orders = array("id_order","current_state");
			
            $this->camposC = array($product, $stock, $stock_available, $product_attribute, $product_lang, $orders);
            
            //Actualizaciones
            
            $this->tablasU = array(0 => "stock", 1 => "stock_available", 2 => "product");
            $product = array("reference", "ean13", "color", "material");
            $stockU = array("physical_quantity", "usable_quantity", "date_upd");
            $stockAvaible = array("quantity");
            
            $this->camposU = array($stockU, $stockAvaible, $product);
                        
		}   
        
        /**
		 * Gestiona los errores
		 *
		 * @param	message				Mensaje a mostrar			
		 *
		**/
		protected function messageError($message) {
            
            $message = utf8_encode($message);
            $array = array("error"=>$message);
            echo(json_encode($array));
            
		}   
        
        /**
		 * Función que filtra los datos recibidos
		 *
		 * @param	data				Texto a filtrar
		 *
		 * @return 	data				Datos ya filtrados
		 *
		**/
		public function filter($data) {
		
			$data = trim(htmlentities(strip_tags($data), ENT_QUOTES,'UTF-8' ));
			
			if (get_magic_quotes_gpc())
				$data = stripslashes($data);
				
			$data = $this->link->real_escape_string($data);
			
			return $data;
			
		}
        
        /**
		 * Función de identificación de acceso a la API
		 *
		**/
		public function identificacionAPI()
		{
			
			try{
			     
                if($this->comprobarGET("l") AND $_GET['l'] == $this::APIKEY){
                    //echo "Identificación correcta";
                    $this->procesarAPI();
                }else{
                    $this->throwException('Conexión incorrecta');
                    //echo "Identificación incorrecta";
                }
        
            }catch(Exception $e){
    
				//Capturo la excepción
                if($e->getCode() == false){
                    $this->messageError($e->getMessage());
                }else{
                    $this->messageError("Error 001. Error con la identificación.");
                }
			}	
		}
        
        
        /**
		 * Función principal que se encarga de todo el procesado de la API
         * Solo llamada después de la identificación correcta
		 *
		**/
		public function procesarAPI()
		{
			
			try{
			     
                switch($this->tipoOrdenAPI()){
                    
                    case 1:
                    
                    $sql = "SELECT ";
                    $cont = 0;
                    
                    foreach ($this->verificarTablasC($_GET['t']) as $valor){
                        if($cont != 0){
                            $sql .= ", ";
                        }
                        $sql .= $valor;
                        $cont++;
              		}
                    $sql = trim($sql, ',');
                    $sql .= " FROM " . $this::PS . $this->filter($_GET['t']) . " LIMIT 0, 300";
                    
                    $json = array();
                    $query = $this->link->query($sql) or $this->throwException('Problema con la sentencia 001');
                    $num = $query->num_rows;
                    if($num > 0){
                        while ($resEmp = $query->fetch_row()) {
                           $resultado = array();
                           for($i = 0; $i < (count($resEmp) ); $i++ ){
                                $resultado[] = $resEmp[$i];
                           }
                           $json[] = $resultado;
                        }
                    }
                    echo json_encode($json);
                    break;
                    
                    case 2:
                    
                    $sql = "SELECT ";
                    $cont = 0;
                    
                    foreach ($this->verificarTablasC($_GET['t']) as $valor){
                        if($cont != 0){
                            $sql .= ", ";
                        }
                        $sql .= $valor;
                        $cont++;
              		}
                    $sql = trim($sql, ',');
                    $pagination = $_GET['p'] * 300;
                    $sql .= " FROM " . $this::PS . $this->filter($_GET['t']) . " LIMIT " . $this->filter($pagination) . ", 300";
                    $json = array();
                    $query = $this->link->query($sql) or $this->throwException('Problema con la sentencia 002');
                    $num = $query->num_rows;
                    if($num > 0){
                        while ($resEmp = $query->fetch_row()) {
                           $resultado = array();
                           for($i = 0; $i < (count($resEmp) ); $i++ ){
                                $resultado[] = $resEmp[$i];
                           }
                           $json[] = $resultado;
                        }
                    }
                    echo json_encode($json);
                    break;
                    
                    case 3:
                    
                    $sql = "SELECT ";
                    $cont = 0;
                    
                    foreach ($this->verificarTablasC($_GET['t']) as $valor){
                        if($cont != 0){
                            $sql .= ", ";
                        }
                        $sql .= $valor;
                        $cont++;
              		}
                    $sql = trim($sql, ',');
                    $arrayT = $this->verificarTablasC($_GET['t']);
                    $sql .= " FROM " . $this::PS . $this->filter($_GET['t']) . " WHERE " . $arrayT[0] . " = '" . $this->filter($_GET['id']) . "'";
                    $json = array();
                    $query = $this->link->query($sql) or $this->throwException('Problema con la sentencia 003');
                    $num = $query->num_rows;
                    if($num > 0){
                        while ($resEmp = $query->fetch_row()) {
                           $resultado = array();
                           for($i = 0; $i < (count($resEmp) ); $i++ ){
                                $resultado[] = $resEmp[$i];
                           }
                           $json[] = $resultado;
                        }
                    }
                    
                    echo json_encode($json);
                    break;
                    
                    //UPDATE
                    case 4:
                    
                    $sql = "UPDATE " . $this::PS . $this->filter($_GET['t']) . " SET " . $this->filter($_GET['c']) . 
                    " = '" . $this->filter($_GET['u']) . 
                    "' WHERE ";
                    
                    $filtrado = $this->filter($_GET['cu']);
                    
                    $consultaCondiciones = explode("$", $filtrado);
                    if(count($consultaCondiciones) > 0){
                       for($i = 0; $i < (count($consultaCondiciones)); $i++){
                            if($i != 0){
                                $sql .= " AND ";
                            }
                            $consultaParametros = explode("=", $consultaCondiciones[$i]);
                            $sql .= $consultaParametros[0] . " = '" . $consultaParametros[1] . "' ";
                        } 
                    }else{
                        $consultaParametros = explode("=", $consultaCondiciones[$i]);
                        $sql .= $consultaParametros[0] . " = '" . $consultaParametros[1] . "' ";
                    }         
                                                        
                    $query = $this->link->query($sql) or $this->throwException('Error con los datos enviados');
                    
                    $json = array("ok"=>utf8_encode("Actualización realizada"));

                    echo json_encode($json);
                    break;
                    
                    case 5:
                    
                    if($_GET['ty'] == "c"){
                        $sql = "SELECT * FROM " .  $this::PS . $this->filter($_GET['t']);
                    }else if($_GET['ty'] == "cty"){
                        $sql = "SELECT ps_product.id_product, ps_product_lang.name, ps_product.active, ps_product.reference, 'ES', ps_stock_available.quantity, ps_product.date_upd, ps_stock.physical_quantity, ps_product.is_virtual, ps_product.price, '0' 
                        FROM ps_product, ps_stock, ps_product_lang, ps_stock_available 
                        WHERE ps_product.id_product = ps_stock.id_product 
                            AND ps_product.id_product = ps_product_lang.id_product 
                            AND ps_product.id_product = ps_stock_available.id_product 
                            AND ps_stock.id_product_attribute = 0 
                        GROUP BY ps_product.id_product";
                    }else if($_GET['ty'] == "cty2"){
                        $sql = "SELECT ps_product.id_product, ps_product_lang.name, ps_product.active, ps_product.reference, 'ES', ps_stock_available.quantity, ps_product.date_upd, ps_stock.physical_quantity, ps_product.is_virtual, ps_product.price, '1', ps_stock.id_product_attribute, ps_attribute_group_lang.public_name, ps_attribute_lang.name, ps_attribute.id_attribute 
                        FROM ps_product, ps_stock, ps_product_lang, ps_stock_available, ps_product_attribute_combination, ps_attribute_lang, ps_attribute, ps_attribute_group_lang
                        WHERE ps_product.id_product = ps_stock.id_product 
                            AND ps_product.id_product = ps_product_lang.id_product 
                            AND ps_product.id_product = ps_stock_available.id_product 
                            AND ps_stock.id_product_attribute != 0 
                            AND ps_stock.id_product_attribute = ps_product_attribute_combination.id_product_attribute 
                            AND ps_product_attribute_combination.id_attribute = ps_attribute_lang.id_attribute 
                            AND ps_product_attribute_combination.id_attribute = ps_attribute.id_attribute 
                            AND ps_attribute.id_attribute_group = ps_attribute_group_lang.id_attribute_group 
                            AND ps_attribute_group_lang.id_lang = 3 
                            AND ps_attribute_lang.id_lang = 3 
                        GROUP BY id_product_attribute, ps_attribute_group_lang.public_name, ps_attribute_lang.name";
                    }
                    
                    
                    
                    $query = $this->link->query($sql) or $this->throwException('Problema con la sentencia 005');
                    $numero = $query->num_rows;
                    $json = array("ok"=>utf8_encode($numero));
                   
                    echo json_encode($json);
                    break;
                    
                    //ESPECIAL 
                    case 6:
                    
                    if($_GET['at'] == 0){
                        
                        $pagination = $_GET['p'] * 300;
                        $sql = "SELECT ps_product.id_product, ps_product_lang.name, ps_product.active, ps_product.reference, '" . $this::COUNTRY . "', ps_stock_available.quantity, ps_product.date_upd, ps_stock.physical_quantity, ps_product.is_virtual, ps_product.price, '0' 
                        FROM ps_product, ps_stock, ps_product_lang, ps_stock_available 
                        WHERE ps_product.id_product = ps_stock.id_product 
                            AND ps_product.id_product = ps_product_lang.id_product 
                            AND ps_product.id_product = ps_stock_available.id_product 
                            AND ps_stock.id_product_attribute = 0";
                            
                        if($this::COUNTRY == "FR")
                            $sql .= " AND ps_product_lang.id_lang = 4";
						if($this::COUNTRY == "IT")
                            $sql .= " AND ps_product_lang.id_lang = 1";
						if($this::COUNTRY == "DE")
                            $sql .= " AND ps_product_lang.id_lang = 1";
						
						if(isset($_GET['id']))
                            $sql .= " AND ps_product.reference = '" . $_GET['id'] ."'";
                        
                        $sql .= " GROUP BY ps_product.id_product LIMIT " . $pagination . ", 300";

                        $json = array();
                        $query = $this->link->query($sql) or $this->throwException('Problema con la sentencia 006');
                        $num = $query->num_rows;
                        
                        if($num > 0){
                            while ($resEmp = $query->fetch_row()) {
                               $resultado = array();
                               for($i = 0; $i < (count($resEmp) ); $i++ ){
                                    $resultado[] = $resEmp[$i];
                               }
                               $json[] = $resultado;
                            }
                        }
                        echo json_encode($json);
                        
                    }else if($_GET['at'] == 1){
                        
                        $pagination = $_GET['p'] * 300;
                        $sql = "SELECT ps_product.id_product, ps_product_lang.name, ps_product.active, ps_product_attribute.reference, '" . $this::COUNTRY . "', ps_stock_available.quantity, ps_product.date_upd, ps_stock.physical_quantity, ps_product.is_virtual, ps_product.price, '1', ps_stock.id_product_attribute, ps_attribute_group_lang.public_name, ps_attribute_lang.name 
                        FROM ps_product, ps_stock, ps_product_lang, ps_stock_available, ps_product_attribute_combination, ps_attribute_lang, ps_attribute, ps_attribute_group_lang, ps_product_attribute
                        WHERE ps_product.id_product = ps_stock.id_product 
                            AND ps_product.id_product = ps_product_lang.id_product 
                            AND ps_product.id_product = ps_stock_available.id_product 
                            AND ps_stock.id_product_attribute != 0 
                            AND ps_stock.id_product_attribute = ps_product_attribute_combination.id_product_attribute 
                            AND ps_product_attribute_combination.id_attribute = ps_attribute_lang.id_attribute 
                            AND ps_product_attribute_combination.id_attribute = ps_attribute.id_attribute 
                            AND ps_attribute.id_attribute_group = ps_attribute_group_lang.id_attribute_group
                            AND ps_product_attribute.id_product = ps_product.id_product
                            AND ps_product_attribute.id_product_attribute = ps_stock.id_product_attribute 
							AND public_name != 'Pack content'";
                            
                         if($this::COUNTRY == "FR"){
                            $sql .= " AND ps_attribute_group_lang.id_lang = 4 
                             AND ps_attribute_lang.id_lang = 4 
                             AND ps_product_lang.id_lang = 4";
                         }else if($this::COUNTRY == "ES"){
                            $sql .= " AND ps_attribute_group_lang.id_lang = 3 
                             AND ps_attribute_lang.id_lang = 3";
                         }else if($this::COUNTRY == "IT"){
                            $sql .= " AND ps_attribute_group_lang.id_lang = 1
                             AND ps_attribute_lang.id_lang = 1 
                             AND ps_product_lang.id_lang = 1";
                         }else if($this::COUNTRY == "DE"){
                            $sql .= " AND ps_attribute_group_lang.id_lang = 1
                             AND ps_attribute_lang.id_lang = 1 
                             AND ps_product_lang.id_lang = 1";
                         }
						 
                            
						
						if(isset($_GET['id'])){
                            $sql .= " AND ps_product_attribute.reference = '" . $_GET['id'] . "'";
                        }
                        
                        $sql .= " GROUP BY id_product_attribute, ps_attribute_group_lang.public_name, ps_attribute_lang.name LIMIT " . $pagination . ", 300";
                        						
                        $json = array();
                        $query = $this->link->query($sql) or die(mysqli_error($this->link));
                        $num = $query->num_rows;
                        
                        if($num > 0){
                            while ($resEmp = $query->fetch_row()) {
                               $resultado = array();
                               for($i = 0; $i < (count($resEmp) ); $i++ ){
                                    $resultado[] = $resEmp[$i];
                               }
                               $json[] = $resultado;
                            }
                        }
                        echo json_encode($json);
                    
                    }else if($_GET['at'] == 2){
                        
                        $sql = "SELECT id_product_pack, id_product_item FROM ps_pack";
                        
						if(isset($_GET['id'])){
                            $sql .= " WHERE id_product_pack = " . $_GET['id'];
                        }
                        
						
                        $json = array();
                        $query = $this->link->query($sql) or die(mysqli_error($this->link));
                        $num = $query->num_rows;
                        
                        if($num > 0){
                            while ($resEmp = $query->fetch_row()) {
                               $resultado = array();
                               for($i = 0; $i < (count($resEmp) ); $i++ ){
                                    $resultado[] = $resEmp[$i];
                               }
                               $json[] = $resultado;
                            }
                        }
                        echo json_encode($json);
                    
                    }else if($_GET['at'] == 3){
                        
                        $sql = "select id_order from ps_order_history where id_order NOT IN
                        (SELECT id_order from ps_order_history where id_order_state = 4 or id_order_state = 6 or id_order_state = 5 or id_order_state =
                        (SELECT id_order_state from ps_order_state_lang where name = 'Reposición' OR name = 'Reposicion' GROUP BY id_order_state ) ) GROUP BY id_order";
                        $query = $this->link->query($sql) or die(mysqli_error($this->link));
                        $json = array();
                        
                        while($row = mysqli_fetch_array($query)) {
                            $sql2 = 'SELECT product_quantity, product_reference, product_attribute_id FROM ps_order_detail WHERE id_order = '.$row['id_order'];		
                            $query2 = $this->link->query($sql2) or die(mysqli_error($this->link));
                            
                            while($row2 = mysqli_fetch_array($query2)) {
                                $product = array(
                                    'product_quantity' => $row2[0],
                                    'reference' => $row2[1],
                                    'product_attribute_id' => $row2[2],
                                    'id_order' => $row[0]
                                );
                                $json[] = $product;                                
                            }
                        }
                        echo json_encode($json);
                    
                    }
                    
                    
                    
                    break;
                    
                    //ESPECIALFR2
                    case 7:
                    
                    $id2 = 0;
                    if($this->comprobarGET("id2")){
                        $id2 = $_GET['id2'];
                    }
                    $sql = "SELECT quantity FROM ps_stock_available, ps_stock WHERE ps_stock_available.id_product = ps_stock.id_product AND reference = '" . $_GET['id'] . "' AND ps_stock_available.id_product_attribute = " . $id2;
                    
                        $json = array();
                        $query = $this->link->query($sql) or die(mysqli_error($this->link));
                        $num = $query->num_rows;
                        
                        if($num > 0){
                            while ($resEmp = $query->fetch_row()) {
                               $resultado = array();
                               for($i = 0; $i < (count($resEmp) ); $i++ ){
                                    $resultado[] = $resEmp[$i];
                               }
                               $json[] = $resultado;
                            }
                        }
                        echo json_encode($json);
                    
                    break;
                    
                    //ESPECIAL ORDERS
                    case 8:
                    
                    $id = 0;
                    if($this->comprobarGET("id")){
                        $id = $_GET['id'];
                    }
                    $sql = "SELECT id_order FROM ps_order_history WHERE id_order NOT IN (select id_order from ps_order_history where id_order_state = 4) AND id_order = " . $id ." GROUP BY id_order";
                    $query = $this->link->query($sql) or die(mysqli_error($this->link));
                    $num = $query->num_rows;
                        
                    if($num > 0){
                        echo 0;
                    }else{
                        echo 1;
                    }
                    
                    break;
					
					//Devuelve los pedidos que todavía están en proceso
                    case 10:
					
					$json = array();
                    $sql = "SELECT id_order, date_add FROM ps_orders WHERE current_state != 4 AND current_state != 6 AND current_state != 24 AND current_state != 35";
                    $query = $this->link->query($sql) or die(mysqli_error($this->link));
					$num = $query->num_rows;
					if($num > 0){
						while ($resEmp = $query->fetch_row()) {
						   $resultado = array();
						   $resultado[] = $resEmp[0];
							$resultado[] = preg_replace('/(.*) .*/', '$1', $resEmp[1]);
							
							$productos = array();
							
							$sql2 = "SELECT product_reference, product_quantity FROM ps_order_detail WHERE id_order = " . $resEmp[0];
							$query2 = $this->link->query($sql2) or die(mysqli_error($this->link));
							while ($resEmp2 = $query2->fetch_row()) {
								$producto = array();
								$producto[] = $resEmp2[0];
								$producto[] = $resEmp2[1];
								$productos[] = $producto;
							}
							$resultado[] = $productos;		
						   $json[] = $resultado;
						}
					}
					echo json_encode($json);
				
                    break;
                    
                    //ESPECIAL STOCK VIRTUAL
                    case 9:
                    
                    $id = 0;
					if($this->comprobarGET("id")){
						$id = $_GET['id'];
					}
					
					$sql = "SELECT o.id_order, o.total_paid, current_state, product_quantity, o.date_add FROM ps_orders o, ps_order_detail od WHERE o.id_order = od.id_order AND current_state != 4 and current_state != 5 and current_state != 6 AND od.product_reference = '$id'";
					
					$query = $this->link->query($sql) or die(mysqli_error($this->link));
					$num = $query->num_rows;
					
					if($this->comprobarGET("show") AND $_GET['show'] == 1){
						$show = 1;
					}
					
					if($show){
						$orders = array();
						while($row = mysqli_fetch_array($query)) {
							for($i = 0; $i < $row['product_quantity']; $i++){
								$aTemporal=array($row['id_order'],$row['total_paid'],$row['current_state'],$row['date_add']);
								array_push($orders,$aTemporal);   
							}
						}
						
						echo json_encode($orders);
					}else{
					   echo $num;
					}
                    
                    break;
					
					//AZR
					//para actualizar en la base de datos las dimensiones y el peso de los articulos desde el ERP
					case 11:
					
					$modificacion_individual=false;
					$posicion_modificacion=$_GET['t'];
					$valor = $_GET['data'];
					
					switch($posicion_modificacion){
						case "peso":
							$cSQL = "UPDATE ps_product set weight='".(float)$valor."' WHERE reference='".$_GET['id']."'";
						break;
						case "anchura":
							$cSQL = "UPDATE ps_product set width='".(float)$valor."' WHERE reference='".$_GET['id']."'";
						break;
						case "alto":
							$cSQL = "UPDATE ps_product set height='".(float)$valor."' WHERE reference='".$_GET['id']."'";
						break;
						case "profundidad":
							$cSQL = "UPDATE ps_product set depth='".(float)$valor."' WHERE reference='".$_GET['id']."'";
						break;
					}
					
					if(!empty($cSQL)){
						$this->link->query($cSQL) or die(mysqli_error($this->link));
                    }
                    break;
					
					//Nuevo metodo de actualizacion de cantidades de productos por referencia
					case 12:
					
					$reference = $_GET['ref'];
					$quantity = $_GET['q'];
					
					//Detectamos si el producto tiene o no un atributo
					$pos = strpos($reference, "-");
					$pk = strpos($reference, "PK");
					$dp = strpos($reference, "DP");
					if($pos === false || ($pk !== false || $dp !== false)){
						//No tiene atributo
						$sql = "SELECT id_product FROM ps_product WHERE reference = '$reference'";
					}else{
						//Tiene atributo
						$sql = "SELECT id_product, id_product_attribute FROM ps_product_attribute WHERE reference = '$reference'";
					}
					$query = $this->link->query($sql) or die(mysqli_error($this->link));
					
					if($pos === false || ($pk !== false || $dp !== false)){
						list($id_product) = $query->fetch_row();   //Listamos los datos
						$id_product_attribute = 0;
					}else{
						list($id_product, $id_product_attribute) = $query->fetch_row();   //Listamos los datos
					}
					
					$sql = "UPDATE ps_stock_available SET quantity = $quantity WHERE id_product = $id_product AND id_product_attribute = $id_product_attribute";
					                                    
                    $query = $this->link->query($sql) or $this->throwException('Error con los datos enviados');
                
                    break;
					
					//Gestión de dependencias
					case 13:
					
					if($this->comprobarGET("act") AND $this->comprobarGET("refP") AND $this->comprobarGET("refC") AND $this->comprobarGET("q")){
						
						if($_GET['act'] == "add"){
							//Añadimos la nueva dependencia
							$sql = "INSERT INTO ps_product_dependency (reference_parent, reference_child, quantity) 
								VALUES('" . $_GET['refP'] . "', '" . $_GET['refC'] . "', '" . $_GET['q'] . "')";
					        $query = $this->link->query($sql) or $this->throwException('Error con los datos enviados');
							
						}else if($_GET['act'] == "del"){
							//Borramos la dependencia
							$sql = "DELETE FROM ps_product_dependency WHERE reference_parent = '" . $_GET['refP'] . "' AND reference_child = '" . $_GET['refC'] . "'";
					        $query = $this->link->query($sql) or $this->throwException('Error con los datos enviados');
						}
						
					}else{
						echo "Faltan parámetros";
					}
					break;
					
					//Añade localizaciones
                    case 14:
						
					$json = array();
					$sql = "INSERT INTO ps_product_location (reference, location) VALUES ('" . $_GET["ref"] . "', '" . $_GET["location"] . "')";
					$query = $this->link->query($sql) or die(mysqli_error($this->link));
					
					echo "Localizacion agregada";
					
                    break;
                    
                }
            
            }catch(Exception $e){
    
				//Capturo la excepción
                if($e->getCode() == false){
                    $this->messageError($e->getMessage());
                }else{
                    $this->messageError("Error 003. Error a la hora de procesar la orden");
                }
			}	
		}
        
        
        /**
		 * Función que determina qué tipo de orden se le ha mandado a la API y la devuelve
		 *
         * 0 = No muestra nada
         * 1 = Muestra los 300 primeros resultados de una tabla
         * 2 = Misma que anterior, pero paginándolo
         * 3 = Muestra los datos de un ID concreto
         * 4 = Actualiza un campo
         * 5 = Cuenta la cantidad de registros en una tabla
         * 6 = Consulta especial que nos devuelve los campos necesarios para trabajar
         * 
         * 
         * @return  data    Numero de tipo de orden
		**/
		public function tipoOrdenAPI()
		{
			
			try{
                
                //SELECT
                if($this->comprobarGET("ty") AND $_GET['ty'] == "s"){
                    if($this->comprobarGET("t") AND $this->verificarTablasC($_GET['t'])){
                        if($this->comprobarGET("p") AND intval($_GET['p'])){
                            return 2;
                        }else if($this->comprobarGET("id") AND $_GET['id']){
                            return 3;
                        }else{
                            return 1;
                        }
                    }else{
                        $this->throwException("No tiene permisos para consultar la tabla");
                        return 0;
                    }
                }
				//DEPENDENCIAS
				else if($this->comprobarGET("ty") AND $_GET['ty'] == "dep"){
						return 13;
                }
				//LOCALIZACIONES
                else if($this->comprobarGET("ref") AND $this->comprobarGET("location")){
						return 14;
                }
				//AZR
				//UPDATE ESPECIAL DIMENSIONES Y PESO
                else if($this->comprobarGET("ty") AND $_GET['ty'] == "espDim"){
						if($this->comprobarGET("id")){
							if($this->comprobarGet("data")){
								return 11;
							}
						}
                }
				//UPDATE ESPECIAL PARA CAMBIAR STOCK AVAILABLE
                else if($this->comprobarGET("ty") AND $_GET['ty'] == "usa"){
						if($this->comprobarGET("ref") AND $this->comprobarGET("q")){
							return 12;
						}
                }
				
                //SELECT ESPECIAL
                else if($this->comprobarGET("ty") AND $_GET['ty'] == "esp"){
                    if( ($this->comprobarGET("p") AND intval($_GET['p'])) OR $_GET['p'] == 0){
                        return 6;
                    }
                }
                //SELECT ESPECIAL 2
                else if($this->comprobarGET("ty") AND $_GET['ty'] == "espfr"){
                    if( $this->comprobarGET("id")){
                        return 7;
                    }else{
                        $this->throwException("Faltan campos para la consulta");
                        return 0;
                    }
                }
                //ESPECIAL ORDERS
                else if($this->comprobarGET("ty") AND $_GET['ty'] == "espor"){
                    if( $this->comprobarGET("id")){
                        return 8;
                    }else{
                        $this->throwException("Faltan campos para la consulta");
                        return 0;
                    }
                }
                //STOCK VIRTUAL
                else if($this->comprobarGET("ty") AND $_GET['ty'] == "vir"){
                    if( $this->comprobarGET("id")){
                        return 9;
                    }else{
                        $this->throwException("Faltan campos para la consulta");
                        return 0;
                    }
                }
				//PEDIDOS PROCESANDOSE
                else if($this->comprobarGET("orders") AND $_GET['orders'] == "process"){
                    return 10;
                }
                //UPDATE
                else if($this->comprobarGET("ty") AND $_GET['ty'] == "u"){
                    if($this->comprobarGET("t") AND $this->verificarTablasU($_GET['t'])){
                        if($this->comprobarGET("c") AND $this->verificarCamposU($this->verificarTablasU($_GET['t']), $_GET['c'])){
                            if(isset($_GET['u']) && $this->comprobarGET("cu")){
                                return 4;
                            }else{
                                $this->throwException("Faltan campos para realizar la actualización");
                                return 0;
                            }
                        }else{
                            $this->throwException("Campo inexistente o no autorizado");
                            return 0;
                        }
                    }else{
                        $this->throwException("Tabla inexistente o no autorizada");
                        return 0;
                    }
                }
                //COUNT
                else if($this->comprobarGET("ty") AND ($_GET['ty'] == "c" OR  $_GET['ty'] == "cty" OR  $_GET['ty'] == "cty2" )){
                   
                    if($this->comprobarGET("t") AND $this->verificarTablasC($_GET['t'])){
                        return 5;
                    }else{
                        $this->throwException("Tabla inexistente o no autorizada");
                        return 0;
                    }
                }
                //No conocido
                else if(!$this->comprobarGET("ty")){
                    
                    $message = utf8_encode("Conexión correcta");
                    $array = array("ok"=>$message);
                    echo(json_encode($array));
                    return 0;
                }else{
                    $this->throwException("Acción desconocida");
                    return 0;
                }
        
            }catch(Exception $e){
    
				//Capturo la excepción
                if($e->getMessage() != null){
                    $this->messageError($e->getMessage());
                }else{
                    $this->messageError("Error 004. Error.");
                }
			}	
		}
        
        /**
		 * Función que guarda las posibles tablas a las que la API tiene permisos para realizar las diferentes consultas
         * En caso de que la tabla esté autorizada, devuelve un array multidimensional con los campos que devolverá
         * En caso de que esta tabla no esté autorizada, devolverá false
         * 
         * @param   data         Nombre de la tabla
         * 
         * @return  data         Array multidimensional
		 *
		**/
		public function verificarTablasC($data)
		{
			
			try{
			     
                $posicion = array_search($data, $this->tablasC);
                if($posicion !== false)
                    $posicion = $this->camposC[$posicion];
                
                return $posicion;
            
            }catch(Exception $e){
    
				//Capturo la excepción
                if($e->getCode() == false){
                    $this->messageError($e->getMessage());
                }else{
                    $this->messageError("Error 002. Error con la verificación de la tabla");
                }
			}	
		}
        
        
        /**
		 * Función que guarda las posibles tablas a las que la API tiene permisos para realizar actualizaciones
         * En caso de que la tabla esté autorizada, devuelve un array multidimensional con los campos que tiene disponibles
         * En caso de que esta tabla no esté autorizada, devolverá false
         * 
         * @param   data         Nombre de la tabla
         * 
         * @return  data         Booleano
		 *
		**/
		public function verificarTablasU($data)
		{
			
			try{
			     
                $posicion = array_search($data, $this->tablasU);
                if($posicion !== false)
                    $posicion = $this->camposU[$posicion];
                    
                return $posicion;
            
            }catch(Exception $e){
    
				//Capturo la excepción
                if($e->getCode() == false){
                    $this->messageError($e->getMessage());
                }else{
                    $this->messageError("Error 006. Error con la verificación de la tabla");
                }
			}	
		}
        
        /**
		 * Función que guarda los posibles campos a las que la API tiene permisos para realizar actualizaciones
         * En caso de que la tabla esté autorizada, devuelve true
         * En caso de que esta tabla no esté autorizada, devolverá false
         * 
         * @param   data         Nombre de la tabla
         * @param   arrayCampos  Todos los campos de la tabla
         * 
         * @return  data         Booleano
		 *
		**/
		public function verificarCamposU($arrayCampos, $data)
		{
			
			try{
			     
                $posicion = array_search($data, $arrayCampos);
                if($posicion !== false)
                    return true;
                return false;
            
            }catch(Exception $e){
    
				//Capturo la excepción
                if($e->getCode() == false){
                    $this->messageError($e->getMessage());
                }else{
                    $this->messageError("Error 005. Error con la verificación de la tabla");
                }
			}	
		}
        
        
        /**
		 * Función que comprueba si está definido un parámetro GET y si tiene un valor
		 *
		 * @param	data				Nombre de la variable GET
		 *
		 * @return 	data				Datos ya filtrados
		 *
		**/
		public function comprobarGET($data) {
            
			if(isset($_GET[$data]) AND $_GET[$data] != "" )
                return true;
			else
                return false;
			
		}
        
        
        
        
       
    }

?>