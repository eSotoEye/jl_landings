   
Hola {firstname} {lastname},

Su pedido con la referencia: #{order_name} está en camino.

Para seguir la ubicación actual de su paquiete utilice este enlace {followup}

Puede revisar su pedido y descargar la factura en: "Historia y detalles de mis pedidos" dentro de su cuenta.	
                                        
¡Gracias por comprar en {shop_name} !