 
Hola {firstname} {lastname},
 
Te confirmamos que ya hemos recibido la orden del pedido {order_name}, realizado el {date} 
con los productos que te mostramos a continuación, ha sido recibido satisfactoriamente. 
Procedemos al envío de su pedido con la mayor brevedad posible.	
                                              
DETALLES DE TU PEDIDO

Ref.
Descripción

Precio
Cant.
Total

{products}
                                                                

Subtotal {total_products}

Descuento {total_discounts}

Envío {total_shipping}

TOTAL {total_paid}

Hasta que reciba su pedido puede revisar su estado y descargar la factura en el menú 
"Historial de pedidos"</a> entrando en "Mi cuenta" dentro de nuestra pagina web	
                
DETALLES DE TU PEDIDO

{date}

{payment}

{carrier}

DIRECCIÓN DE ENTREGA

Nombre y apellidos {delivery_firstname} {delivery_lastname}

Dirección {delivery_address1} {delivery_address2}

Código postal {delivery_postal_code}

Ciudad {delivery_city},{delivery_state}

País {delivery_country}

Teléfono {delivery_phone}

DIRECCIÓN DE FACTURACIÓN

Nombre y apellidos {invoice_firstname} {invoice_lastname}

Dirección {invoice_address1} {invoice_address2}

Código postal {invoice_postal_code}

Ciudad {invoice_city}, {invoice_state}

País {invoice_country}

Teléfono {invoice_phone}