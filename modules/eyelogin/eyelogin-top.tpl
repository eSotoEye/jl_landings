
<style>
		div.imagenCabecera{
			width: 100%;
			min-height: 73px;
			background-image: url('{$modules_dir}eyeLanding/images/foto_cabecera.png');
			background-position-x: -139px;
			background-size: 100%;
			background-position-y: center;
			background-repeat: no-repeat;
			    border-bottom: 10px solid #ff5000;
		}

		@media (max-width:1420px)
		{
			div.imagenCabecera{
				background-position-x: center;
			}
		}
		@media (max-width: 800px){
			div.imagenCabecera{
			
				background-size: 125%;
			}
		}
		
		@media (max-width:659px)
		{
			div.imagenCabecera{
			
				background-size: 125%;
				background-size: 162%;
				background-position-x: right;
			}
			
			
		}
		
		@media (max-width:360px)
		{
			#logo img{
				width:90% !important;
			}
			#logo { text-align: center; }
		}
		@media (max-width:491px)
		{
			div.imagenCabecera{
				min-height:0px;
				background-image:none;
				
			}
			#logo{
				position:initial !important;
				margin:0px auto;
			}
		}
		@media (max-width:1050px) {
			#menu_wrapper{
		   /* min-height:150px;*/
		  }
			  #menu span {
			padding: 10px 8px;
			margin-left: 0px;
		  }
		}
		@media (max-width: 530px){

			#menu{
				text-align:center !important;
			}
		}
		@media (max-width:500px) {
			#menu span{ padding:0px;}
			
				 #menu span {
			padding: 10px 10px;
			margin-left: 0px;
			font-size: 14px;
			}	
		}
		
		#menu span {
			padding: 10px 10px;
			margin-left: 0px;
			font-size: 16px;
		 
		}
		#menu {
			background-color: black;
			color: white;
			text-align: right;
			padding:2px;
		}

#index #logo{ top: 42px !important;}
#logo{
	position: absolute;
    top: 44px;
    width: 350px;
    background: white;
    height: 63px;
}

#logo img { width: 100%;     margin-top: 5px; }

#cms #menu{
	    padding: 2px;
}
#menu span a{
	color:white;
	font-style:normal;
}

#menu span a:hover{
	color: #ff5000;
}
#menu span a:after{
	content: "";
}
#menuContactanos{
	display:inline-block;
}

#menuContactanosTLF{
	 
   display:inline-block;
 
}
@media (max-width:490px)
{
	#menuCarro
	{
		
	}
	
	#contactanos{
		
	}
}
 

</style><!-- MENU -->
<div id="menu_wrapper">
	
	<div id="menu">
	   <span id=""><a href="http://crunch.teleachatdirect.com/pedido-rapido"><i style="font-size:18px;" class="fa fa-shopping-cart" aria-hidden="true"></i><span id="menuCarro">MI CESTA</span></a></span>
	  <span id="menuContactanos"><a href="http://crunch.teleachatdirect.com/contactanos"><i class="fa fa-envelope" aria-hidden="true"></i> <span id="contactanos">CONT&Aacute;CTANOS</span></a></span>
	  {*<span id="menuContactanosTLF"><i class="fa fa-phone" aria-hidden="true"></i> 951 204 106</span>*}
	 
	</div>
	<div id="logo"><a href="http://crunch.teleachatdirect.com/"><img src="{$modules_dir}eyeLanding/images/logo.png"></a></div><div class="imagenCabecera"></div>
</div>