<?php
/*
*  @author Fran Valladares <soporte1@eyeinversionesonline.com>
*  @copyright  2016
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*
*	NOTA: Basado en blocktopsearch
*/

if (!defined('_PS_VERSION_'))
	exit;

class eyelogin extends Module
{
	public function __construct()
	{
		$this->name = 'eyelogin';
		$this->tab = 'search_filter';
		$this->version = 1.0;
		$this->author = 'Fran Valladares';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Eye Login');
		$this->description = $this->l('Añade el box de inicio de sesión estilo Amazon en la cabecera de la página.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		if (!parent::install() || !$this->registerHook('top') || !$this->registerHook('header') || !$this->registerHook('displayMobileTopSiteMap'))
			return false;
		return true;
	}
	
	public function hookdisplayMobileTopSiteMap($params)
	{
		$this->smarty->assign(array('hook_mobile' => true, 'instantsearch' => false));
		$params['hook_mobile'] = true;
		return $this->hookTop($params);
	}
		
	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'eyelogin.css', 'all');
		$this->context->controller->addJS(($this->_path).'eyelogin.js');
		// Ejemplo de como importar un CSS
		//$this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
		$key = $this->getCacheId('eyelogin-top');
		if (Tools::getValue('search_query') || !$this->isCached('eyelogin-top.tpl', $key))
		{
			$this->smarty->assign(array(
				'self' => dirname(__FILE__),
				'eyelogin_type' => 'top',
				'search_query' => (string)Tools::getValue('search_query')
				)
			);
		}
		Media::addJsDef(array('eyelogin_type' => 'top'));
		return $this->display(__FILE__, 'eyelogin-top.tpl', Tools::getValue('search_query') ? null : $key);
	}

	public function hookTop($params)
	{
		
	}
	
	public function hookDisplayNav($params)
	{
		return $this->hookTop($params);
	}

}

