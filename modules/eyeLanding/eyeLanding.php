<?php

class eyeLanding extends Module
{

    public function __construct() {
        $this->name          = 'eyeLanding';
        $this->tab           = 'front_office_features';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Muestra el contenido de la landing');
        $this->description = $this->l('Muestra el contenido de la landing');

        $this->version = '1.0';
        $this->author  = 'Eduardo Soto';
        $this->error   = false;
        $this->valid   = false;
    }

    public function install() {
        if (parent::install() == false || $this->registerHook('displayHome') == false)
            return false;
        return true;



        //Valoraciones

        $valoraciones = array();
    }

    public function hookDisplayHome() {

        require_once(_PS_TOOL_DIR_ . 'mobile_Detect/Mobile_Detect.php');
        $this->mobile_detect = new Mobile_Detect();
        $mobile_class        = 'desktop';
        if ($this->mobile_detect->isMobile() && !$this->mobile_detect->isTablet())
        {
            $mobile_class = 'mobile';
        }
// Call Different file for desktop and mobile
        if ($mobile_class == 'desktop')
        {
            $template_name = 'eyeLanding.tpl';
        }
        else
        {
            $template_name = 'eyeLanding_m.tpl';
        }
// And finally setTemplate HTML.
//        $this->setTemplate($template_name);
        $this->smarty->assign(array(
            'tomorrow' => date("d/m/Y", strtotime(date("d-m-Y")."+ 1 days"))
        ));
        return $this->display(__FILE__, $template_name);
    }

}
