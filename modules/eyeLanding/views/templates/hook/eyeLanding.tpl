<link rel="stylesheet" href="{$module_dir}/slick/slick-theme.css">
<link rel="stylesheet" href="{$module_dir}/slick/slick.css">
<link rel="stylesheet" href="{$module_dir}style.css">
<script type="text/javascript" src="{$module_dir}/slick/slick.js"></script>

{literal}
	<style>
	

		
	</style>
{/literal}
<div style="clear:both;"></div>



<!-- SECCIÓN PRINCIPAL QUE CONTENDRÁ TODA LA INFORMACIÓN DEL PRODUCTO RESUMIDA -->
<div class="mainSection">
	
		<!-- Indice de imagenes -->
		<div class="leftImgIndex">
			<span id="miniaturavideo" class="miniaturavideo"><img src="modules/eyeLanding/images/video.png" style="width:100%;" /></span>
			<span id="miniatura1" class="miniaturaImg"><img src="modules/eyeLanding/images/1.png" style="width:100%;" /></span>
			<span id="miniatura2" class="miniaturaImg"><img src="modules/eyeLanding/images/2.png" style="width:100%;" /></span>
			<span id="miniatura3" class="miniaturaImg"><img src="modules/eyeLanding/images/3.png" style="width:100%;" /></span>
			 
		</div>
		<!-- Imágenes del producto-->
		<div class="leftImages">
			<img src="modules/eyeLanding/images/1.png" id="img1" />
		</div>
	
		<!-- Descipción del producto-->
		<div class="description">
		<div class="wrapper">
			<div class="productName">
				<span><h1 style="font-style:italic; font-weight:bold">Total Fit Crunch</h1></span>
				<div class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			</div> 
			<div class="productSubName"><p>EL EJERCICIO CARDIOVASCULAR QUE TONIFICA</p></div>
			<div class="productPrice"><span>329</span>,90€</div> <span id="enviogratis">ENV&Iacute;O GRATIS</span>
			
			
			
				<ul style="">
					<li><i class="fa fa-check"></i>&nbsp;Con Fit Crunch y sólo unos minutos al día conseguirás un cuerpo perfecto. Resultados visibles.</li>
					<li><i class="fa fa-check"></i>&nbsp;Realiza ejercicios de baja o alta resistencia con el propio peso de tu cuerpo, además puedes trabajar brazos, espalda, abdominales, glúteos y piernas con un mismo ejercicio.</li>
					<li><i class="fa fa-check"></i>&nbsp;Sillín adaptable en altura y distancia, fácil de plegar, contador digital.<li>
				</ul>

			
		
			<p style="margin-top:20px;"><strong>Puedes pagar con:</strong> tarjeta o Paypal. <a id="masinfoTarjeta" href="#">Más info</a></p>
			<div id="popupTarjetas"><img src="{$module_dir}/images/modalidades-de-pago.png" /> <span id="popupTarjetas_close">x</span></div>
			<p class="aviso" style="padding:10px;padding-left:0px; padding-top:0px;">
				<span>Env&iacute;o 3-5 d&iacute;as <span style="color:#64A954; font-size:14px;font-weight:bold ">GRATIS</span></span>
			</p>
			  
			
			<div style="clear:both;"></div>
	 
		<!-- Botones para la compra-->
		<div id="primerBoton"class="rightBuy col-lg-12">
			{*<p>Información adicional: este producto está sujeto a disponibilidad de stock etc etc....</p>*}
			<p id="add_to_cart" class="">
				<button  style="margin-left: 0px;"  onclick="validacionPedido(4,false);" name="Submit" >
					<span>Comprar</span>
				</button>
			</p>
		</div>
		</div>
		</div>


	
	<div style="clear:both;"></div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo">
</div>
</div>

<!-- Descripción y modo de empleo -->
<div id="sectiondos" class="container">
		<span class="titulo" style="border: 1px solid black;padding: 10px; font-style:italic;">Te puede interesar</span>
		<div class="wrapper">
			<div id="producto2">
					<div style=""  class="producto-img"><img src="{$module_dir}images/faja_slimcore.png"/></div>
					<div style=""  class="producto-desc">
							<p style="font-size:20px; border-bottom:1px solid black">Slim Core Belt <span style="font-style:italic"><b>Bonplus</b></span></p>
							<div style="font-size: 24px; font-weight:bold;" class="productPrice2"><span>81</span>,90€</div>
							<p class="descripcionProducto">Resultados más visibles en menos tiempo. Compuesta por 3 capas: 1- La capa interior favorece la sudoración; mediante la cual eliminará toxinas y reducirá el volumen. La capa intermedia está compuesta por millones de micro capsulas que absorben el sudor. Y la capa exterior impide que la humedad traspase a tu ropa, evitando manchas en la ropa.</p>
	
								<div style="" class="combinaciones">
									<ul>
										<li><i class="fa fa-circle-thin combinacion2 combinacionActivada" data-pr="3" aria-hidden="true"></i>S</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="4" aria-hidden="true"></i>M</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="5" aria-hidden="true"></i>L</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="6" aria-hidden="true"></i>XL</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="7"aria-hidden="true"></i>XXL</li>
									</ul>
							</div>
							<div  style="" class="rightBuy col-lg-12">
								<p id="add_to_cart" class="">
									<button style="margin-left:0px;" onclick="validacionPedido(10,$('.combinacion2.combinacionActivada').attr('data-pr'));" name="Submit" >
										<span>Comprar</span>
									</button>
								</p>
							</div>
					</div>
					
			</div>
			 
			<div id="producto3">
			
					<div style="" class="producto-img"><img src="{$module_dir}images/faja_xtreme.png"/></div>
					<div style=""  class="producto-desc">
							<p style="font-size:20px; border-bottom:1px solid black">Xtreme <span style="font-style:italic"><b>Bonplus</b></span> Belt</p>
							<div style="font-size: 24px; font-weight:bold;"class="productPrice2"><span>172</span>,90€</div>
							<p class="descripcionProducto">Moldea tu figura de forma inmediata. Ideal para llevar puesta en cualquier ocasión durante todo el día. Además ejerce de corrector postural, evitando lesiones mientras haces ejercicios y dolores lumbares. Efecto sauna y apariencia de varias tallas menos. Aumenta la sudoración e intensifica los efectos de las cremas reductoras.</p>
						<div style="" class="combinaciones">
									<ul>
										<li><i class="fa fa-circle-thin combinacion1 combinacionActivada" data-pr="1" aria-hidden="true"></i>S-M</li>
										<li><i class="fa fa-circle-thin combinacion1" data-pr="2" aria-hidden="true"></i>L-XL</li>
									</ul>
							</div>
							<div  style="" class="rightBuy col-lg-12">
								<p id="add_to_cart" class="">
									<button style="margin-left:0px;" onclick="validacionPedido(9,$('.combinacion1.combinacionActivada').attr('data-pr'));" name="Submit" >
										<span>Comprar</span>
									</button>
								</p>
							</div>
					</div>
			</div>
		</div>
		
		<div style="clear:both"></div>
		
</div>
{*<p style="text-align: center;
    padding-top: 18px;
    padding-bottom: 0px;
    font-size: 14px;">*Gastos de env&iacute;o gratis para pedidos superiores a 25€</p>*}
<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>

<!-- Descripción y modo de empleo -->
<div id="sectiontres" class="container">

		<span class="titulo">Descripción y modo de empleo</span>
		<div style="" class="wrapper">
			<p style="font-size:18px; color:#ff5000; font-weight:bold; font-style:italic; text-align:center">&iexcl;Trabaja tus m&uacute;sculos tonific&aacute;ndolos con ejercicios de alta y baja resistencia!</p>
			<p style="display:inline-block; width:100%; text-align:left;">
				<strong>Bonplus</strong>, marca conocida por sus productos de alta calidad, pone a nuestro alcance <strong>el aparato de gimnasia más completo del mercado en el mínimo espacio.</strong>
				<strong>Fit Crunch</strong> está diseñado para que puedas tonificar todo el cuerpo, además de ejercicio cardiovascular con un mismo movimiento.
				
			</p>
			<p style="display:inline-block; width:100%; text-align:left;">
				<strong>Trabaja y tonifica cada parte del cuerpo:</strong> brazos, pecho, espalda, abdominales, glúteos, muslos, gemelos…  Cambia los brazos de sitio, o las piernas de lugar e intensificarás el ejercicio.
			</p>
			<p style="display:inline-block; width:100%; text-align:left; padding-bottom:10px;">
				Su sillón es adaptable tanto en altura como en distancia. Y su contador digital te contará además de las repeticiones, el tiempo y las calorías consumidas.
			</p>
			<p id="especificacionesImagenes">
				<img class="izquierda" style="" src="{$module_dir}/images/descripcion_1.png" />
				<img class="derecha" style="" src="{$module_dir}/images/descripcion_2.png" />
				<img class="izquierda" style="" src="{$module_dir}/images/descripcion_3.png" />
				<img class="derecha" style="" src="{$module_dir}/images/descripcion_4.png" />
			</p>
			<div style="clear:both"></div>
			
				<div style="" class="especificaciones">
					<div style="    ">
						<ul>
							<li><strong>Incluye:</strong>
								<li> &nbsp;</li>
								<li>- Banco de ejercicios Fit Crunch</li>
								<li>- Contador digital </li>
								<li>- Plan de dietas <br>&nbsp;(Idiomas: Inglés, Francés, Español, Italiano y Alemán)</li>
								<li>- Manual de instrucciones y montaje con guía de entrenamiento <br>&nbsp;(Idiomas: Inglés, Francés, Español, Italiano y Alemán)</li>
								
							</ul>
						
					</div>	
					<!--<div style=" ">
						<ul>
							
						</ul>
					</div>-->

				</div>
				<div style="" class="especificaciones">

					<div style="  ">
							<ul>
								<li><strong>Datos técnicos:</strong>
								<li>  &nbsp;</li>
								<li>- Intensidad de alta y baja resistencia</li>
								<li>- Sillín ajustable para diferentes alturas y distancia</li>
								<li>- Soporte hasta un máximo de 100 kilos</li>
								<li>- Plegable, muy fácil de guardar</li>
								<li>- Medidas: * Abierto: 124cm x 88cm x 41cm * Plegado: 154cm x 26 cm</li>
								<li>- Material: acero</li>
							</ul>	
					</div>
					<!--<div style="">
						<ul>
							
						</ul>
					</div>-->
				</div>			
			
		<div style="clear:both"></div>
		<div class="rightBuy col-lg-12">
			{*<p>Información adicional: este producto está sujeto a disponibilidad de stock etc etc....</p>*}
			<p id="add_to_cart" class="">
				<button  onclick="validacionPedido(4,false);" name="Submit" >
					<span>Comprar</span>
				</button>
			</p>
		</div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>

<!-- Videos -->
<div id="sectioncuatro" class="container">
	
		<span style=" " class="titulo">V&iacute;deos</span>
		<div class="wrapper">
		{*<div class="slider">*}
		
			<div class="video"><span class="enlace" style="display:none">https://www.youtube.com/embed/s-AokyD_q3Q</span><img src="{$module_dir}/images/video2.png" /></div>
			
			
		{*</div>*}
		</div>
		<div class="rightBuy col-lg-12">
			<p onclick="validacionPedido(4,false);" id="add_to_cart" class="">
				<button name="Submit" >
					<span>Comprar</span>
				</button>
			</p>
		</div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>
 
  
<!-- Valoraciones -->

<div id="sectioncinco" class="container">
	
		<span class="titulo" style=" border: 1px solid black; padding: 10px;">Valoraciones</span>
	<div class="wrapper valoraciones">
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Aida 28/01/2016 </span>
			<div style="float:right; color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p>Estéticamente no me convencía mucho, pero me animó mi sobrina y la verdad es que la niña tenía razón. El segundo día estaba con agujetas hasta en las orejas.</p>
		</div>
		 
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Cris 29/01/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p> El material en el que está hecho, aporta seguridad cuando se está trabajando sobre él. </p>
		</div>
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Alba. 16/01/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p>Compré el aparato un lunes y me llegó en sólo 3 días, fue muy fácil de montar. Estoy muy contento con mi adquisición, lo uso todos los días para entrenar diferentes partes del cuerpo. Después de un mes se notan los resultados. Recomendable 100%.</p>
		</div>	
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Carmen 14/02/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p>Se lo he comprado a mi padre porque le cuesta mucho hacer ejercicio y desde que lo recibimos lo usa todos los días. La entrega fue rápida y el producto muy bien.</p>
		</div>		
				
	</div>
		<div style="clear:both"></div>

</div>

<div style="margin-top: 10px;" id="volverArriba">
	<p>Volver arriba</p>
</div>

<div id="cortina"></div>
<div style="" id="popupVideo">

</div>
{* END-PAGINACION *}
	<form id="buy_block" class="hidden" action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
		<!-- hidden datas -->
		<p class="hidden">
			<input type="hidden" name="token" value="{$static_token}" />
			<input type="hidden" class="id_product" id="product_page_product_id" name="id_product" value="1" />
			<input type="hidden" name="add" value="1" />
			<input type="hidden" name="quantity_wanted" id="quantity_wanted" value="1" />
			<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
		</p>
	</form>
	
{literal}
<script type="text/javascript">
	function validacionPedido(id_del_producto,id_product_attribute){
		
		document.getElementById("product_page_product_id").value=id_del_producto;
		if (id_product_attribute!=false)
			document.getElementById("idCombination").value=id_product_attribute;
		else
			document.getElementById("idCombination").value="";
	}
	
	$(document).ready(function() {
		  var offset = -20; //Offset of 20px
		  $("#popupVideo").hide();

		 $(".combinacion1").click(function(){
			$(".combinacion1").removeClass("combinacionActivada");
			$(this).addClass("combinacionActivada");
		 });
		 
		 $(".combinacion2").click(function(){
			$(".combinacion2").removeClass("combinacionActivada");
			$(this).addClass("combinacionActivada");
		 });
		  $("#masinfoTarjeta").click(function(){
			$("#popupTarjetas").show();
		  });
		    
		  $("#popupTarjetas_close").click(function(){
			$("#popupTarjetas").hide();
		  });
		  
		  $(".miniaturavideo").click(function(){
				//Obtenemos el ancho del navegador.
			var width = $(window).width();
			
			//En funcion del ancho del navegador, calculamos la proporcion del video.
			if (width > 850) {
			  var anchovideo = "800px";
			  var altovideo = "600px";
			} else if (width > 350 && width < 850) {
			  var anchovideo = "350px";
			  var altovideo = "323px";
			} else {
			  var anchovideo = "300px";
			  var altovideo = "280px";
			}
			
			//Obtenemos la direcci&oacute;n
			var enlace = $(".video").find(".enlace").text();
			//Maquetamos el iframe
			var embed = "<iframe width='" + anchovideo + "' height='" + altovideo + "' src='" + enlace + "' frameborder='1' allowfullscreen></iframe><span class='close-popup'>X</span>";
			$("#cortina").show();
			$("#popupVideo").html(embed);
			$("#popupVideo").show();
			
			//Hacemos scroll hasta el video.
			$('html, body').animate({
			  scrollTop: $("#popupVideo").offset().top - 40
			}, 1000);
		  
		  });
		  
		  $(".video").click(function() {
			//Obtenemos el ancho del navegador.
			var width = $(window).width();
			
			//En funcion del ancho del navegador, calculamos la proporcion del video.
			if (width > 850) {
			  var anchovideo = "800px";
			  var altovideo = "600px";
			} else if (width > 350 && width < 850) {
			  var anchovideo = "350px";
			  var altovideo = "323px";
			} else {
			  var anchovideo = "300px";
			  var altovideo = "280px";
			}
			
			//Obtenemos la direcci&oacute;n
			var enlace = $(this).find(".enlace").text();
			//Maquetamos el iframe
			var embed = "<iframe width='" + anchovideo + "' height='" + altovideo + "' src='" + enlace + "' frameborder='1' allowfullscreen></iframe><span class='close-popup'>X</span>";
			$("#cortina").show();
			$("#popupVideo").html(embed);
			$("#popupVideo").show();
			
			//Hacemos scroll hasta el video.
			$('html, body').animate({
			  scrollTop: $("#popupVideo").offset().top - 40
			}, 1000);

		  });
		
		  //Al hacer click en la cortina eliminamos el contenido del iframe y lo ocultamos.
		  $("#cortina").click(function() {
		  
			$('body,html').animate({
			  scrollTop: 0
			}, 500);
			  
			$("#popupVideo").html("");
			$("#popupVideo").hide();
			$(this).hide();
			
		  });
		    $(document).on("click",".close-popup",function(){
				$('body,html').animate({
					scrollTop: 0
				}, 500);
			
				$("#popupVideo").html("");
				$("#popupVideo").hide();
				$("#cortina").hide();
				
			
		  });
		  
		  /*Eventos para la navegaci&oacute;n*/
		  $('#volverArriba').click(function() {

			$('body,html').animate({
			  scrollTop: 0
			}, 1000);
			return false;
		  });

		  $(".sectiondos").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectiondos").offset().top + offset
			}, 1000);
		  });

		  $(".sectiontres").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectiontres").offset().top + offset
			}, 1000);
		  });

		  $(".sectioncuatro").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectioncuatro").offset().top + offset
			}, 1000);
		  });

		  $(".sectioncinco").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectioncinco").offset().top + offset
			}, 1000);
		  });

		  
		  /*Men&uacute; Izquierda del producto (imagenes en miniatura)*/
		  $(".miniaturaImg img").click(function() {

			var img = "<img src='" + $(this).attr("src") + "' />";

			$(".leftImages").html(img);

		  });

		  $(".miniaturaImg img").hover(function() {
			var img = "<img src='" + $(this).attr("src") + "' />";

			$(".leftImages").html(img);

		  });

		  
		  /*Sliders*/
		  /*Videos*/
		  $('.slider').slick({
			slidesToShow: 3,
			slidesToScroll: 3,
			dots: true,
			adaptiveHeight: true,
			autoplay: true,
			autoplaySpeed: 6000,

			responsive: [{
				breakpoint: 1000,
				settings: {
				  slidesToShow: 2,
				  slidesToScroll: 2,
				  adaptiveHeight: true,
				  dots: true,
				  centerMode: false,

				  autoplay: true,
				  autoplaySpeed: 6000
				}
			  }, {
				breakpoint: 800,
				settings: {
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  autoplay: true,
				  autoplaySpeed: 6000
				}
			  }


			]
		  });
 
		  /*Valoraciones*/
		  $('.valoraciones').slick({
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			slidesToShow: 2,
			cssEase: 'linear',
			responsive: [{
			  breakpoint: 1000,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 6000
			  }
			}]
		  });

	});
document.addEventListener("DOMContentLoaded", function(event) { 
	//do work
	$(".miniaturaImg").each(function(index){
		$(this).hover(function(){
			//alert(index);
		});
	});
});

</script>
{/literal}