<?php

if (!defined('_PS_VERSION_'))
	exit;

class tagsremarketing extends Module
{
	public function __construct()
	{
		$this->name = 'tagsremarketing';
		$this->tab = 'front_office_features';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Tags Remarketing');
		$this->description = $this->l('Añade etiquetas personalizadas para remarketing');

		$this->version = '1.0';
		$this->author = 'Joshua Leiva';
		$this->error = false;
		$this->valid = false;
	}
	
	public function install()
	{
		if (parent::install() == false || $this->registerHook('displayHeader') == false || $this->registerHook('displayOrderConfirmation') == false)
			return false;
		return true;
	}
	
	public function hookDisplayOrderConfirmation()
	{	
		
		/*JL JL*/
			$cSQLEAN = "SELECT ean13 FROM ps_product WHERE id_product=".$id_product;
			$ean13 = "";
			$ean13 = Db::getInstance()->getValue($cSQLEAN);
			if($ean13 != ""){
				$content .= '<g:gtin>'.$ean13.'</g:gtin>'."\n";	
			}
		
		//Id_order
		$id_order = "";
		if(Tools::getValue('id_order')){
			$cSQLEAN = "SELECT id_order FROM ps_orders WHERE id_order = '" . Tools::getValue('id_order') . "'";
			$id_order = Db::getInstance()->getValue($cSQLEAN);
		}
		else{
			$cSQLEAN = "SELECT id_order FROM ps_orders ORDER BY id_order DESC";
			$id_order = Db::getInstance()->getValue($cSQLEAN);
		}
		
		//Total order
		$cSQLEAN = "SELECT total_paid FROM ps_orders WHERE id_order = '$id_order'";
		$total_order = Db::getInstance()->getValue($cSQLEAN);
		
		//Productos del pedido
		$productsSQL = "SELECT product_reference, product_name, product_quantity, ROUND((od.product_price*1.21)-IFNULL(reduction, 0), 2) AS price_iva FROM ps_order_detail od LEFT JOIN ps_specific_price ps ON ps.id_product = od.product_id WHERE id_order = '$id_order'";
		$products = Db::getInstance()->ExecuteS($productsSQL);
		$products2 = Db::getInstance()->ExecuteS($productsSQL);
		
		$this->context->smarty->assign(array(
            'id_order' => $id_order,
			'total_order' => $total_order,
			'products' => $products,
			'products2' => $products2
        ));
		return $this->display(__FILE__, 'order_confirmation.tpl');
	}

	public function hookHeader()
	{	
		$total_cart = "";
		$products = array();
		if(isset($this->context->cart->id)){
			$total_cart = str_replace(",", ".", str_replace(" €", "", $this->context->cart->getTotalCart($this->context->cart->id)));	
			$products = $this->context->cart->getProducts();
		}
		$references_cart = array();
		$typepag = "other";
		
		$references_page = "";
		$prices_page = "";
		
		//Tipo de página
		if(Tools::getValue('id_product')){
			$typepag = "product";
		}else if(Tools::getValue('id_category')){
			$typepag = "category";
		}else if(Tools::getValue('search_query')){
			$typepag = "searchresults";
		}else if($_SERVER['REQUEST_URI'] == "/"){
			$typepag = "home";
		}else if(Tools::getValue('id_cart') OR Tools::getValue('id_order') OR strpos($_SERVER['REQUEST_URI'], "confirmacion") 
			OR strpos($_SERVER['REQUEST_URI'], "confirmation") OR strpos($_SERVER['REQUEST_URI'], "payment")){
			$typepag = "purchase";
		}else if(strpos($_SERVER['REQUEST_URI'], "carrito") OR strpos($_SERVER['REQUEST_URI'], "pedido")){
			$typepag = "cart";
		}
		
		
		//Guardamos las referencias del carrito
		foreach ($products as $num => $product) {
			$references_cart[] = $product['reference'];
		}
		
		//Obtenemos el id del producto, y en base a eso sacamos la referencia
		if(Tools::getValue('id_product')){
			
			$id_product = Tools::getValue('id_product');
			$cSQL2 = "SELECT reference FROM ps_product_attribute where id_product = '$id_product'";
			$ref = Db::getInstance()->getValue($cSQL2);
			
			if(!isset($ref) OR empty($ref)){
				$cSQL2 = "SELECT reference FROM ps_product where id_product = '$id_product'";
				$ref = Db::getInstance()->getValue($cSQL2);
			}
			
			$cSQL2 = "SELECT ROUND((p.price*1.21)-IFNULL(reduction, 0), 2) AS price_iva FROM ps_product p LEFT JOIN ps_specific_price ps ON ps.id_product = p.id_product WHERE p.id_product = '" . Tools::getValue('id_product') . "'";
			$prices_page = Db::getInstance()->getValue($cSQL2);
			
		}
		
		$this->context->smarty->assign(array(
            'references' => $ref,
			'prices_page' => $prices_page,
			'type_pag' => $typepag,
			'total_cart' => $total_cart,
			'products' => $references_cart
        ));
		return $this->display(__FILE__, 'tagsremarketing.tpl');
	}

}
