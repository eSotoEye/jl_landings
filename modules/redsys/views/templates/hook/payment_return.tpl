{*
* NOTA SOBRE LA LICENCIA DE USO DEL SOFTWARE
* 
* El uso de este software está sujeto a las Condiciones de uso de software que
* se incluyen en el paquete en el documento "Aviso Legal.pdf". También puede
* obtener una copia en la siguiente url:
* http://www.redsys.es/wps/portal/redsys/publica/areadeserviciosweb/descargaDeDocumentacionYEjecutables
* 
* Redsys es titular de todos los derechos de propiedad intelectual e industrial
* del software.
* 
* Quedan expresamente prohibidas la reproducción, la distribución y la
* comunicación pública, incluida su modalidad de puesta a disposición con fines
* distintos a los descritos en las Condiciones de uso.
* 
* Redsys se reserva la posibilidad de ejercer las acciones legales que le
* correspondan para hacer valer sus derechos frente a cualquier infracción de
* los derechos de propiedad intelectual y/o industrial.
* 
* Redsys Servicios de Procesamiento, S.L., CIF B85955367
*}
{if $status == 'ok'}
	<p>Il vostro ordine è andato a buon fine
		<br /><br />- Importo: <span class="price"><strong>{$total_to_pay|escape:'htmlall'}</strong></span>
		<br /><br />- N# <span class="price"><strong>{$id_order|escape:'htmlall'}</strong></span>
		<br /><br />Le abbiamo inviato una e­mail con le informazioni.
		<br /><br />Per eventuali domande sul suo ordine, non esiti a contattarci.</a>.
		<br /><br />Il vostro riferimento ordine è {$reference_order}. Il suo ordine di riferimento è stato inviato al suo indirizzo di posta elettronica</a>.
	</p>
{else}
	<p class="warning">
		Se ha producido un problema con su pedido, por favor, contacte con nosotros a través de nuestro  
		<a href="{$link->getPageLink('contact', true)|escape:'html'}">servicio de atención al cliente.</a>.
	</p>
{/if}
