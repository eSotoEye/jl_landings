<script>
{literal}
    function setcook() {
        var nazwa = 'cookie_ue';
        var wartosc = '1';
        var expire = new Date();
        expire.setMonth(expire.getMonth()+12);
        document.cookie = nazwa + "=" + escape(wartosc) + ((expire==null)?"" : ("; expires=" + expire.toGMTString()))
    }
{/literal}
</script>
<style>
{literal}
#btnCerrarCookies{
	background-color:#000000;
	color:#FFFFFF;
	border:0;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	padding: 5px 14px;
}
#enlace_a_cookies{
	color: black !important;
}
#enlace_a_cookies:hover{
	color: black !important;
	font-weight: bold;
	text-decoration: underline;
}
#cookieNotice p {margin:0px; padding:0px;}
{/literal}
</style>

{* Método para detectar la versión de IE *}
<!--[if lt IE 7 ]> <div id="IEdetector" class="ie6" style="display:none;"> <![endif]-->
<!--[if IE 7 ]>    <div id="IEdetector" class="ie7" style="display:none;"> <![endif]-->
<!--[if IE 8 ]>    <div id="IEdetector" class="ie8" style="display:none;"> <![endif]-->
<!--[if IE 9 ]>    <div id="IEdetector" class="ie9" style="display:none;"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <div id="IEdetector" class="" style="display:none;"> <!--<![endif]--> </div>
<script>
var clase = document.getElementById('IEdetector').className;
if(clase == 'ie9' || clase == 'ie8' || clase == 'ie7' || clase == 'ie6'){
	console.log(clase);setcook();
}
</script>

<div id="cookieNotice" style="
width: 100%; 
position: fixed; 
{if $vareu->uecookie_position==2}
bottom:0px;
box-shadow: 0px 0 10px 0 #{$vareu->uecookie_shadow};
{else}
top:0px;
box-shadow: 0 0 10px 0 #{$vareu->uecookie_shadow};
{/if}
background: #{$vareu->uecookie_bg};
z-index: 9999;
font-size: 14px;
line-height: 1.3em;
font-family: arial;
left: 0px;
text-align:center;
color:#FFF;
opacity: {$vareu->uecookie_opacity}
">
<div id="cookieNoticeContent" style="position:relative; margin:auto; padding:3px; width:100%; display:block; font-size:12px; color: #333; font-family: century gothic; letter-spacing:-0.3px">
	{$uecookie}
	<![if !IE]>
	<span id="cookiesClose" style="visibility:hidden; position:absolute; right:0px; top:1px; " onclick="$('#cookieNoticeContent').hide(); setcook();"><img src="{$module_template_dir}/close.png" alt="close" style="width:16px; height:16px; cursor:pointer;"/></span>
	<![else]>
	<span id="cookiesClose" style="visibility:hidden; position:absolute; right:0px; top:1px; " onclick="document.getElementById('cookieNoticeContent').style.display='none'; setcook();"><img src="{$module_template_dir}/close.png" alt="close" style="width:16px; height:16px; cursor:pointer;"/></span>
	<![endif]>
	</div>
</div>

<script type="text/javascript">
	
	$('html').click(function() {
		$("span#cookiesClose").click();
	});
	$("#enlace_a_cookies").click(function(){
		window.location.href='http://renumax-car.com/content/7-politica-de-cookies';
	});
	
</script>