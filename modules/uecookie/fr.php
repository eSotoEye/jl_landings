<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{uecookie}prestashop>uecookie_77dd54e4f719c6fb0c2e23452eb830da'] = 'L\'Union européenne \"Cookie\" la loi';
$_MODULE['<{uecookie}prestashop>uecookie_fba6a93b46d375a50cb1c58b84899053'] = 'Ce module affiche une information bien avec les cookies dans votre PrestaShop';
$_MODULE['<{uecookie}prestashop>uecookie_1061fda795260b08e769c493105557f7'] = 'Cette boutique utilise des cookies et d\'autres technologies afin que nous puissions améliorer votre expérience sur notre site.';
$_MODULE['<{uecookie}prestashop>uecookie_d12652129be13b431f0e491afac0d4fe'] = 'L\'Union européenne \"Cookie\" la loi';
$_MODULE['<{uecookie}prestashop>uecookie_4d1d06e0e2f0a31f2806796bf0513c1a'] = 'Le texte:';
$_MODULE['<{uecookie}prestashop>uecookie_43781db5c40ecc39fd718685594f0956'] = 'Enregistrer les paramètres';
