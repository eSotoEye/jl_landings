<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{uecookie}prestashop>uecookie_77dd54e4f719c6fb0c2e23452eb830da'] = 'Unijne Prawo dotyczące \"COOKIES\"';
$_MODULE['<{uecookie}prestashop>uecookie_fba6a93b46d375a50cb1c58b84899053'] = 'Ten moduł wyświetla informację dotyczącą ciasteczek w Twoim sklepie';
$_MODULE['<{uecookie}prestashop>uecookie_1061fda795260b08e769c493105557f7'] = 'Ten sklep wykorzystuje technologię \"Ciasteczek\", która rozszerza możliwości działania sklepu';
$_MODULE['<{uecookie}prestashop>uecookie_d12652129be13b431f0e491afac0d4fe'] = 'Unijne prawo dotyczące \"Cookies\"';
$_MODULE['<{uecookie}prestashop>uecookie_4d1d06e0e2f0a31f2806796bf0513c1a'] = 'Tekst komunikatu';
$_MODULE['<{uecookie}prestashop>uecookie_43781db5c40ecc39fd718685594f0956'] = 'zapisz';
