{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Contact' mod='recaptcha'}{/capture}
<style>
    @media screen and (max-width: 240px){
        .g-recaptcha {
            transform:scale(0.58);-webkit-transform:scale(0.58);
            transform-origin:0 0;-webkit-transform-origin:0 0;
        }}



    @media screen and (min-width: 319px) and (max-width: 320px){
        .g-recaptcha {
            transform:scale(0.79);-webkit-transform:scale(0.79);
            transform-origin:0 0;-webkit-transform-origin:0 0;
        }}

    @media (min-width: 768px){
        .container {
            max-width: 980px !important;
        }
    }
    </style>
    <div class="row" style="margin-top: 18px ;margin-bottom: 18px ;">
    <div class="col-md-5 col-sm-4 col-xs-3" style="border-bottom: solid 1.5px #d6d4d4; height: 15px" >

    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 text-center" style="padding: 0px;">
        <h3  style="margin-top: 0px ; margin-bottom: 0px;">Contáctenos</h3>

    </div>
    <div class="col-md-5 col-sm-4 col-xs-3" style="border-bottom: solid 1.5px #d6d4d4; height: 15px">

    </div>

</div>
{*<h1 class="page-heading bottom-indent">
{l s='Customer service' mod='recaptcha'} - {if isset($customerThread) && $customerThread}{l s='Your reply' mod='recaptcha'}{else}{l s='Contact us' mod='recaptcha'}{/if}
</h1>*}

<script>
    $(document).ready(function () {
        $("#showContactForm").click(function () {
            $(".contact-form-box").slideDown();
        });
    });
</script>
<div id="newContactContainer" class="row">

    {hook h='displayContacto'}

</div>

{if isset($confirmation)}
    <p class="alert alert-success">{l s='Your message has been successfully sent to our team.' mod='recaptcha'}</p>
    <div class="row" style=" margin-bottom: 50px;margin-top: 30px;">

        <div class="col-md-3 col-xs-12">
            <a name="continue_easypay" id="continue_easypay" title="Continuar comprando" href="{$base_dir_ssl}">
                <div style="
                     width: 100%;

                     background: white;
                     color: #b2b2b2;
                     border: 1px solid;
                     font-size: 15px;
                     border-color: #b2b2b2;
                     border-radius: 1.1rem;
                     padding: 10px 15px;
                     text-align: center;
                     font-weight:  bold;
                     ">
                    <div class="row">
                        <div class="col-md-2 col-xs-2"><i class="fas fa-angle-double-left"></i></div>

                        <div class="col-md-8 col-xs-8">Continuar comprando</div>
                        <div class="col-md-2 col-xs-2"><i class="fas fa-angle-double-left"></i></div>

                    </div>

                </div>
            </a>
        </div>
        <div class="col-md-9">

        </div>
    </div>
{elseif isset($alreadySent)}
    <p class="alert alert-warning">{l s='Your message has already been sent.' mod='recaptcha'}</p>
    <div class="row" style=" margin-bottom: 50px;margin-top: 30px;">

        <div class="col-md-3 col-xs-12">
            <a name="continue_easypay" id="continue_easypay" title="Continuar comprando" href="{$base_dir_ssl}">
                <div style="
                     width: 100%;

                     background: white;
                     color: #b2b2b2;
                     border: 1px solid;
                     font-size: 15px;
                     border-color: #b2b2b2;
                     border-radius: 1.1rem;
                     padding: 10px 15px;
                     text-align: center;
                     font-weight:  bold;
                     ">
                    <div class="row">
                        <div class="col-md-2 col-xs-2"><i class="fas fa-angle-double-left"></i></div>

                        <div class="col-md-8 col-xs-8">Continuar comprando</div>
                        <div class="col-md-2 col-xs-2"><i class="fas fa-angle-double-left"></i></div>

                    </div>

                </div>
            </a>
        </div>
        <div class="col-md-9">

        </div>
    </div>
{else}
    {include file="$tpl_dir./errors.tpl"}
    {*IPORRAS*}
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <form action="{$request_uri}" method="post" class="contact-form-box" style="background: white" enctype="multipart/form-data">
                <div  class="col-md-12 col-xs-12" style="background: white">
                    {*    <h3 class="page-subheading">{l s='send a message' mod='recaptcha'}</h3>*}
                    <div class="clearfix">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group selector1">
                                <label for="id_contact">{l s='Subject Heading' mod='recaptcha'}</label>
                                {if isset($customerThread.id_contact) && $customerThread.id_contact && $contacts|count}
                                    {assign var=flag value=true}
                                    {foreach from=$contacts item=contact}
                                        {if $contact.id_contact == $customerThread.id_contact}
                                            <input type="text" class="form-control" id="contact_name" name="contact_name" value="{$contact.name|escape:'html':'UTF-8'}" readonly="readonly" />
                                            <input type="hidden" name="id_contact" value="{$contact.id_contact|intval}" />
                                            {$flag=false}
                                        {/if}
                                    {/foreach}
                                    {if $flag && isset($contacts.0.id_contact)}
                                        <input type="text" class="form-control" id="contact_name" name="contact_name" value="{$contacts.0.name|escape:'html':'UTF-8'}" readonly="readonly" />
                                        <input type="hidden" name="id_contact" value="{$contacts.0.id_contact|intval}" />
                                    {/if}
                                </div>
                            {else}
                                <select id="id_contact" class="form-control" name="id_contact">
                                    <option value="0">{l s='-- Choose --' mod='recaptcha'}</option>
                                    {foreach from=$contacts item=contact}
                                        <option value="{$contact.id_contact|intval}"{if isset($smarty.request.id_contact) && $smarty.request.id_contact == $contact.id_contact} selected="selected"{/if}>{$contact.name|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <p id="desc_contact0" class="desc_contact{if isset($smarty.request.id_contact)} unvisible{/if}">&nbsp;</p>
                            {foreach from=$contacts item=contact}
                                <p id="desc_contact{$contact.id_contact|intval}" class="desc_contact contact-title{if !isset($smarty.request.id_contact) || $smarty.request.id_contact|intval != $contact.id_contact|intval} unvisible{/if}">
                                    <i class="icon-comment-alt"></i>{$contact.description|escape:'html':'UTF-8'}
                                </p>
                            {/foreach}
                        {/if}
                        <p class="form-group">
                            <label for="email">{l s='Email address' mod='recaptcha'}</label>
                            {if isset($customerThread.email)}
                                <input class="form-control grey" type="text" id="email" name="from" value="{$customerThread.email|escape:'html':'UTF-8'}" readonly="readonly" />
                            {else}
                                <input class="form-control grey validate" type="text" id="email" name="from" data-validate="isEmail" value="{$email|escape:'html':'UTF-8'}" />
                            {/if}
                        </p>
                        {if !$PS_CATALOG_MODE}
                            {if (!isset($customerThread.id_order) || $customerThread.id_order > 0)}
                                <div class="form-group selector1">
                                    <label>{l s='Order reference' mod='recaptcha'}</label>
                                    {if !isset($customerThread.id_order) && isset($is_logged) && $is_logged}
                                        <select name="id_order" class="form-control">
                                            <option value="0">{l s='-- Choose --' mod='recaptcha'}</option>
                                            {foreach from=$orderList item=order}
                                                <option value="{$order.value|intval}"{if $order.selected|intval} selected="selected"{/if}>{$order.label|escape:'html':'UTF-8'}</option>
                                            {/foreach}
                                        </select>
                                    {elseif !isset($customerThread.id_order) && empty($is_logged)}
                                        <input class="form-control grey" type="text" name="id_order" id="id_order" value="{if isset($customerThread.id_order) && $customerThread.id_order|intval > 0}{$customerThread.id_order|intval}{else}{if isset($smarty.post.id_order) && !empty($smarty.post.id_order)}{$smarty.post.id_order|escape:'html':'UTF-8'}{/if}{/if}" />
                                    {elseif $customerThread.id_order|intval > 0}
                                        <input class="form-control grey" type="text" name="id_order" id="id_order" value="{if isset($customerThread.reference) && $customerThread.reference}{$customerThread.reference|escape:'html':'UTF-8'}{else}{$customerThread.id_order|intval}{/if}" readonly="readonly" />
                                    {/if}
                                </div>
                            {/if}
                            {if isset($is_logged) && $is_logged}
                                <div class="form-group selector1">
                                    <label class="unvisible">{l s='Product' mod='recaptcha'}</label>
                                    {if !isset($customerThread.id_product)}
                                        {foreach from=$orderedProductList key=id_order item=products name=products}
                                            <select name="id_product" id="{$id_order}_order_products" class="unvisible product_select form-control"{if !$smarty.foreach.products.first} style="display:none;"{/if}{if !$smarty.foreach.products.first} disabled="disabled"{/if}>
                                                <option value="0">{l s='-- Choose --' mod='recaptcha'}</option>
                                                {foreach from=$products item=product}
                                                    <option value="{$product.value|intval}">{$product.label|escape:'html':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        {/foreach}
                                    {elseif $customerThread.id_product > 0}
                                        <input  type="hidden" name="id_product" id="id_product" value="{$customerThread.id_product|intval}" readonly="readonly" />
                                    {/if}
                                </div>
                            {/if}
                        {/if}
                        {if $fileupload == 1}
                            <p class="form-group">
                                <label for="fileUpload">{l s='Attach File' mod='recaptcha'}</label>
                                <input type="hidden" name="MAX_FILE_SIZE" value="{if isset($max_upload_size) && $max_upload_size}{$max_upload_size|intval}{else}2000000{/if}" />
                                <input type="file" name="fileUpload" id="fileUpload" class="form-control" />
                            </p>
                        {/if}


                        {if isset($HOOK_CONTACT_FORM_BOTTOM)}
                            {$HOOK_CONTACT_FORM_BOTTOM} 
                        {/if}


                    </div>
                    <div class="col-xs-12 col-md-8" >
                        <div class="form-group">
                            <label for="message">{l s='Message' mod='recaptcha'}</label>
                            <textarea class="form-control" id="message" name="message">{if isset($message)}{$message|escape:'html':'UTF-8'|stripslashes}{/if}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row submit" style="justify-content: center;     padding-left: 20px;">

                    {*IPORRAS*}

                    <input type="text" name="url" value="" class="hidden" />
                    <input type="hidden" name="contactKey" value="{$contactKey}" />
                    <button type="submit" name="submitMessage" id="submitMessage" 
                            class="button btn btn-default button-medium col-md-4 col-xs-12" style="
                            margin-bottom: 15px;
                            " ><span>{l s='Send' mod='recaptcha'}<i style="margin-left: 15px;" class="far fa-envelope"></i></span></button>
                </div>

            </form>
        </div>

        


        <div class="imagen_banner_gris" style=" margin-top: 20px; display: none;"><img style="width: 100%" src="{$smarty.const._MEDIA_LINK_JL_}img/tel16/iconos.jpg"> </div>
    </div>
{/if}
{addJsDefL name='contact_fileDefaultHtml'}{l s='No file selected' mod='recaptcha' js=1}{/addJsDefL}
{addJsDefL name='contact_fileButtonHtml'}{l s='Choose File' mod='recaptcha' js=1}{/addJsDefL}
