{literal}
<link href="http://belletica.com/ERPresta/libraries/chosen.min.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="http://belletica.com/ERPresta/libraries/chosen.jquery.min.js"></script>

<script>
$(document).ready(function(){
	$("#delp").chosen({no_results_text: "No se han encontrado referencias"}); 
	$("#addp").chosen({no_results_text: "No se han encontrado referencias"}); 
	
	if({/literal}{$completed}{literal} == 1){
		window.location.href =document.URL;
		console.log("ok");
	}
	
	$("#delp_container").hide();
	$("#addp_container").hide();
	
	$( "#typech0" ).click(function() {
		$("#delp_container").hide();
		$("#addp_container").hide();
	});
	
	$( "#typech1" ).click(function() {
		$("#delp_container").show();
		$("#addp_container").hide();
	});
	
	$( "#typech2" ).click(function() {
		$("#addp_container").show();
		$("#delp_container").hide();
	});
});
</script>

{/literal}

<fieldset style="margin-top: 15px; margin-bottom: 15px; padding: 10px; border: solid 1px #e6e6e6;
    background-color: #fff;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: rgba(0,0,0,0.1) 0 2px 0,#fff 0 0 0 3px inset;
    box-shadow: rgba(0,0,0,0.1) 0 2px 0,#fff 0 0 0 3px inset;">
	
<legend style="bordeR: 0px; width: 160px; color: #555; margin-bottom: 0px; font-size: 14px;">Modificación de pedidos</legend>
Desde este módulo podrá modificar los pedidos, independientemente del estado en el que se encuentre. Estos cambios se sincronizarán automáticamente con el ERP.
<br/> Cambio a realizar:
<form name="erpresta" method="post">
<br />
<input type="radio" name="typech" value="0" id="typech0" checked> Ninguno
<br />
<input type="radio" name="typech" value="1" id="typech1"> Borrar producto
<br />
<input type="radio" name="typech" value="2" id="typech2"> Añadir producto
<br /><br />
<div id="delp_container">
<select id="delp" class="form2" name="delp" style="width: 800px;">
	{foreach from=$order_products item=product}
		<option value="{$product[1]}">{$product[0]} - {$product[2]}</option>
	{/foreach}
 </select>
</div>
<div id="addp_container">
<select id="addp" class="form2" name="addp" style="width: 800px;  display: inline;">
	{foreach from=$shop_products item=product}
		<option value="{$product[1]}">{$product[0]} - {$product[2]}</option>
	{/foreach}
 </select>
 <br />
 Cantidad:
 <input style="width: 50px; display: inline;" type="text" name="addpquantity" value="1" />
</div>
<br />
<button style="width: 150px;" class="btn btn-primary btn-block" type="submit" name="submit">
											Procesar
										</button>
</form>


</fieldset>

