<?php
/**
*
*	Joshua Leiva - soporte@teletiendadirecto.com
*	
*	Módulo necesario en Prestashop para la gestión de pedidos
*
*   Todos los derechos reservados
*   Copyright 2014
*  
*/

if (!defined('_PS_VERSION_'))
	exit;
	
class ERPresta2 extends Module
{
	
	const ERPresta_URL = 'http://belletica.com/ERPresta';
	const Country = 'ES';
	const Cancelado = 6;
	const Enviado = 4;
	const DeVuelta = 18;

	public function __construct()
	{
		$this->name		= 'ERPresta2';
		$this->tab		= 'shipping_logistics';
		$this->version	= '1.0';
		$this->installed_version = '';
		$this->author = 'Edu Soto';
        $this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('ERPresta 2');
		$this->description = $this->l('Módulo encargado de la gestión de pedidos a través de ERPresta');
	}

	public function install()
	{
		if (!parent::install())
			return false;
			
		if (!$this->registerHooks())
			return false;
		
		return true;
	}

	/*
	** Función encargada de registrar los hooks
	*/
	private function registerHooks()
	{      
	   
	    if(!$this->registerHook('postUpdateOrderStatus') || !$this->registerHook('adminOrder') || !$this->registerHook('orderProductModified'))
            return false;
            
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
            
		return true;
	}

	public function hookpostUpdateOrderStatus($params)
	{
	   
		
		
	}
	
	public function hookAdminOrder($params)
	{	
	
		$idOrder=Tools::getValue('id_order');
		
		$sql="select product_id, product_name, product_reference from ps_order_detail where id_order=".$idOrder;
		$result=Db::getInstance()->ExecuteS($sql);
		
		$html="";
		foreach ($result as $r)
		{
				$html.="<option value='".$r['product_reference']."'>".$r['product_reference']."-".$r['product_name']."</option>\n";
		}
		//var_dump ($result);
		$this->smarty->assign('productListOrder', $html);
		
		$html="";
		
		$sql="SELECT pp.id_product, IFNULL(ppa.reference,pp.reference) as referencia, ppl.name from ps_product pp
				LEFT JOIN ps_product_attribute ppa on (ppa.id_product=pp.id_product)
				INNER JOIN ps_product_lang ppl on (pp.id_product=ppl.id_product)
				where ppl.id_lang=1";
			  
		$result=Db::getInstance()->ExecuteS($sql);
		
		foreach ($result as $r)
		{
			$html.="<option value='".$r['referencia']."'>".$r['referencia']."-".$r['name']."</option>\n";
		}
		$this->smarty->assign('productListAll', $html);
		$this->smarty->assign("id_order",$idOrder);
		
		return $this->display(__FILE__, 'admin.tpl');
	}
	
	public function hookOrderProductModified($params)
	{
	 
		
	}
	
}
