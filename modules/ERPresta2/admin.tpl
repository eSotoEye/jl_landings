{literal}
<link href="http://teleachatdirect.com/modules/ERPresta2/js/chosen.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="http://teleachatdirect.com/modules/ERPresta2/js/chosen.jquery.min.js"></script>

<script>
$(document).ready(function(){
	$("#delp").chosen({no_results_text: "No se han encontrado referencias"}); 
	$("#addp").chosen({no_results_text: "No se han encontrado referencias"}); 
	
	$("#delp_container").hide();
	$("#addp_container").hide();
	
	$( "#typech0" ).click(function() {
		$("#formAction").val("false");
		$("#delp_container").hide();
		$("#addp_container").hide();
	});
	
	$( "#typech1" ).click(function() {
		$("#formAction").val("remove");
		$("#delp_container").show();
		$("#addp_container").hide();
	});
	
	$( "#typech2" ).click(function() {
		$("#formAction").val("add");
		$("#addp_container").show();
		$("#delp_container").hide();
	});
	
	$("#erPrestaForm").submit(function( event ) {
		event.preventDefault();
		
		var action=$("#formAction").val();
		
		switch (action)
		{
			case "false": break;
			case "remove": removeProduct(); break;
			case "add": addProduct(); break;
			default: break;
		}
		
		
		//$("#addp_container").
	});

	function addProduct()
	{

			$.ajax({
				type: "POST",
                data: $( "#erPrestaForm" ).serialize(),
                url: "http://teleachatdirect.com/modules/ERPresta2/scripts/addProduct.php",
                success: function (result) {
					//console.log(result);
					location.reload();
                }
            });
	}
	
	
	function removeProduct()
	{
			var r = confirm("¿Estas seguro de que quieres borrar este producto del pedido?");
			if (r == true) {
				$.ajax({
				type: "POST",
                data: $( "#erPrestaForm" ).serialize(),
                url: "http://teleachatdirect.com/modules/ERPresta2/scripts/delProduct.php",
                success: function (result) {
					 //console.log(result);
                     location.reload();
                }
            });
			} else {
				
			}

			
	}
	
	
});
</script>
{/literal}

	
<fieldset style="margin-top: 15px; margin-bottom: 15px; padding: 10px; border: solid 1px #e6e6e6;
    background-color: #fff;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: rgba(0,0,0,0.1) 0 2px 0,#fff 0 0 0 3px inset;
    box-shadow: rgba(0,0,0,0.1) 0 2px 0,#fff 0 0 0 3px inset;">
	
<legend style="bordeR: 0px; width: 160px; color: #555; margin-bottom: 0px; font-size: 14px;">Modificación de pedidos (<b>EN PRUEBAS</b>)</legend>
Desde este módulo podrá modificar los pedidos, independientemente del estado en el que se encuentre. Estos cambios se sincronizarán automáticamente con el ERP.
<br/> Cambio a realizar:
<form id="erPrestaForm" name="erpresta2" method="post">
	{$key = $id_order|cat:"belletica.com"}
	
	<input type="hidden" name="key" value="{$key|md5}">
	<input type="hidden" name="id_order" value="{$id_order}">
	<input type="hidden" id="formAction" name="action" value="false">
	<br />
	<input type="radio" name="typech" value="0" id="typech0" checked> Ninguno
	<br />
	<input type="radio" name="typech" value="1" id="typech1"> Borrar producto
	<br />
	<input type="radio" name="typech" value="2" id="typech2"> Añadir producto
	<br /><br />
	<div id="delp_container">
		<select data-placeholder="Elige la referencia..." id="delp" style="width:600px;" class="chosen-select" name="delp">
			<option value=""></option>
			{$productListOrder}
		 </select>
	</div>
	<div id="addp_container">
		<select data-placeholder="Elige la referencia..." id="addp" style="width:600px;" class="chosen-select" name="addp">
			<option value=""></option>
			{$productListAll}
		 </select>
	 <br />
	 Cantidad:
	 <input style="width: 50px; display: inline;" type="text" name="addpquantity" value="1" />
	 <br>
	 Precio:
	 <input style="width: 50px; display: inline;" type="text" name="addprice" value="" />
	</div>
	<br />
	<button style="width: 150px;" class="btn btn-primary btn-block" type="submit" name="submit">
												Procesar
										</button>
</form>


</fieldset>

