<?php

class AdminMassiveStates extends ModuleAdminController
{
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function initContent()
	{
        parent::initContent(); 
		
		//Obtenemos el id de la tienda, ya que segun la tienda los envios se gestionarán de una manera u otra.
		
		$id_tienda=(int)$this->context->shop->id;
		 
	
		
		 if ($id_tienda==1)
		{
			  if( Tools::getValue('orders') ){
            $orders = unserialize(stripslashes($_POST['orders']));      
            $fp = fopen('../export.csv', 'w');
			$transportista = Tools::getValue('sh');
			
			 
			//VABRMA;VABRSD;VABLOD;VABCAD;VABCBO;VABCAS;VABNOT;VABTRC;VABEMD;VABIND;VABNCL;VABPKB;VABPRD;VABLNP;VABCCM;VABCTR;VABTSP
			$fp2 = fopen('../ordersproducts.csv', 'w+');
				if($transportista == 1){
            fputcsv($fp, array('ID_ORDER', 'REFERENCIA', 'NOMBRE COMPLETO', 'CIUDAD', 'CP', 'TIPO DE PAGO', 'REEMBOLSO', "OBSERVACIONES", "KEYTSV", "CONTACTO", 'TELEFONO', 'EMAIL', 'DIRECCION COMPLETA', 'DIRECCION 2', 'COD PAIS', 'REEMBOLSO', 'T.PAGO', '1', '1'), ";");
				}else if($transportista == 2){
            fputcsv($fp, array('VABRMA', 'VABRSD','VABLOD','VABCAD','VABCBO','VABCAS','VABNOT','VABTRC','VABEMD','VABIND','VABNCL','VABPKB','VABPRD','VABLNP','VABCCM','VABCTR','VABTSP','VABTIC'),";");
				}
			fputcsv($fp2, array('CANTIDAD', 'REFERENCIA', 'LOCALIZACION', 'EAN13', 'NOMBRE'), ";");
			$products = array();
			
			foreach ($orders as $order) {
                $sql = "SELECT o.id_order, o.reference, o.payment, o.total_paid_tax_incl, a.firstname, a.lastname, a.address1, a.address2, a.city, a.postcode, a.phone_mobile, c.email, a.other, co.iso_code, ps.iso_code as iniciales, ps.name FROM ps_orders o, ps_address a, ps_state ps, ps_customer c, ps_country co WHERE a.id_country = co.id_country AND a.id_address = o.id_address_delivery AND ps.id_state=a.id_state AND o.id_customer = c.id_customer AND o.id_order =". $order;
                if ($results = Db::getInstance()->ExecuteS($sql)){
                    foreach ($results as $row){
					
						$sqlproducts = "SELECT product_quantity, product_reference FROM ps_order_detail WHERE id_order = " . $row['id_order'];
						if ($resultsproducts = Db::getInstance()->ExecuteS($sqlproducts)){
							foreach ($resultsproducts as $rowp){
								if(isset($products[$rowp['product_reference']])){
									$products[$rowp['product_reference']] = $products[$rowp['product_reference']] + $rowp['product_quantity'];
								}else{
									$products[$rowp['product_reference']] = $rowp['product_quantity'];
								}
							}
						}
                        
                        if($row['payment'] == 'Cash on delivery (COD)'){
							$tPago=4;
                            $reembolso = str_replace(",",".",number_format($row['total_paid_tax_incl'], 2, ',', ' '));
                        }else{
							$tPago=1;
                            $reembolso = "";
                        }
                        preg_match('/^07.*/', $row['postcode'], $coincidencias, PREG_OFFSET_CAPTURE);
                        if(count($coincidencias) > 0){
                            $keytsv = 34;
                        }else{
                            $keytsv = 13;
                        }
                        
                        $dir1 = mb_substr($row['address1'], 0, 40, 'UTF-8');
                        $dir2 = mb_substr($row['address1'], 40, 40000, 'UTF-8');
                        $observaciones="";
						   if($transportista == 1){
                        fputcsv($fp, array($row['id_order'], $row['reference'], $row['firstname'] . " " . $row['lastname'], $row['city'], $row['postcode'], 
                        $row['payment'], $reembolso, $row['other'], $keytsv, $row['address2'], $row['phone_mobile'], $row['email'], $dir1, $dir2, $row['iso_code'],$reembolso, "CONTANTI", "1", "1"), ";");
						 	}else if($transportista == 2){
						fputcsv($fp, array($row['id_order'],$row['firstname'] . " " . $row['lastname'],$row['city'], $row['postcode'],$tPago,$reembolso,$observaciones,$row['phone_mobile'],$row['email'],$dir1,'1','1',$row['iniciales'],'50','506875','0','C'," "),";");
							}
                    }
                }
                
            }
            fclose($fp);

			foreach ($products as $k => $v) {
				$csql = "SELECT product_ean13, product_name FROM ps_order_detail WHERE product_reference = '" . $k . "' ORDER BY id_order_detail DESC LIMIT 1";
				if ($cres = Db::getInstance()->ExecuteS($csql)){
                    foreach ($cres as $crow){
						fputcsv($fp2, array($v, $k, "", $crow['product_ean13'], $crow['product_name']), ";");
					}
				}
				
			}
            fclose($fp2);
             
        }else{
        
        
            if( Tools::getValue('orderprocess') AND Tools::getValue('stateprocess') ){
                
                sleep(1);
                $sql = "SELECT id_employee FROM ps_employee WHERE firstname LIKE '%ERP%'";
                global $cookie;
                $employee = 8;
                
                $id_order = Tools::getValue('orderprocess');
                $newState = Tools::getValue('stateprocess');
                $history = new OrderHistory();
    			$history->id_order = $id_order;
    			$history->id_employee = $employee;
    			$history->changeIdOrderState($newState, $id_order);
    			$hoy = date('Y-m-d H:i:s');
    			Db::getInstance()->insert('order_history', array(
    				'id_employee' => $employee,
    				'id_order'      => $id_order,
    				'id_order_state'      => $newState,
    				'date_add'      => $hoy
    			));
				
				if($newState != 4 AND $newState != 6){
					$newState = 0;
				}
				
				$sql = "SELECT product_quantity, p.reference, product_attribute_id, p.id_product as id_product FROM ps_order_detail, ps_product p, ps_stock s WHERE product_id = p.id_product AND id_product_attribute = product_attribute_id AND id_order = $id_order AND p.id_product = s.id_product";		
				if ($results = Db::getInstance()->ExecuteS($sql)){
					foreach ($results as $row){
						
						$id_product = $row['id_product'];
						$id_product_attribute = $row['product_attribute_id'];
						
						$reference = $row['reference'];
					
						$sql2 = "SELECT reference FROM ps_product_attribute WHERE id_product = $id_product AND id_product_attribute = $id_product_attribute";	
						if ($row2 = Db::getInstance()->getRow($sql2))
							$reference = $row2['reference'];
						
						
						$isAttribute = 1;
						if($row['product_attribute_id'] == 0){
							$isAttribute = 0;
						}
						$product_quantity = $row['product_quantity'];
						
						$ci = curl_init();
						curl_setopt($ci, CURLOPT_URL, "http://teleshopdiretto.com/ERPresta/motor/ERPresta.orders.php?r=$reference&o=$id_order&q=$product_quantity&s=$newState&a=$isAttribute&c=FIT");
						curl_setopt($ci, CURLOPT_TIMEOUT, 30);
						curl_setopt($ci, CURLOPT_RETURNTRANSFER, TRUE);
						$result = curl_exec ($ci);
						
						/*eSoto de forma temporal y mientras este la oferta, descontamos aparte del renumax el limpia llantas*/
							/*if ($reference=="IT947")
							{
								$reference="IT948";
								$ci = curl_init();
								curl_setopt($ci, CURLOPT_URL, "http://teleshopdiretto.com/ERPresta/motor/ERPresta.orders.php?r=$reference&o=$id_order&q=$product_quantity&s=$newState&a=$isAttribute&c=REN");
								curl_setopt($ci, CURLOPT_TIMEOUT, 5);
								curl_setopt($ci, CURLOPT_RETURNTRANSFER, FALSE);
								$result = curl_exec ($ci);
								
							}*/
							/*eSoto*/
					}	
				}
                
            }else{
            
                $query = 'SELECT id_order_state, name FROM ps_order_state_lang WHERE id_lang = 3 AND id_order_state != 4 AND id_order_state != 6 ORDER BY name';
        		$history = Db::getInstance()->executeS($query);
                
                $query = 'SELECT id_order_state, name FROM ps_order_state_lang WHERE id_lang = 3 ORDER BY name';
        		$history2 = Db::getInstance()->executeS($query);
                
                if( Tools::getValue('id_order_state') ){
                    
                    if( Tools::getValue('datepicker') ){
                        if(Tools::getValue('datepicker') != ""){
                            
                            if(Tools::getValue('dateR') == "="){
                                $query = 'SELECT id_address, o.id_order, c.firstname, c.lastname, o.date_add, address1, address2, postcode, payment, a.other, total_paid FROM ps_orders o, ps_customer c, ps_address a, ps_carrier ca WHERE o.id_shop=1 and  c.id_customer = o.id_customer AND a.id_address = o.id_address_delivery AND current_state = ' . Tools::getValue('id_order_state') . ' AND o.id_carrier = ca.id_carrier AND external_module_name != "mondialrelay" AND o.date_add LIKE "%' . Tools::getValue('datepicker') .'%"';
                            }else if(Tools::getValue('dateR') == ">="){
                                $query = 'SELECT id_address, o.id_order, c.firstname, c.lastname, o.date_add, address1, address2, postcode, payment, a.other, total_paid FROM ps_orders o, ps_customer c, ps_address a, ps_carrier ca WHERE o.id_shop=1 and  c.id_customer = o.id_customer AND a.id_address = o.id_address_delivery AND current_state = ' . Tools::getValue('id_order_state') . ' AND o.id_carrier = ca.id_carrier AND external_module_name != "mondialrelay" AND DATE(o.date_add) >=  "' . Tools::getValue('datepicker') .'"';
                            }else if(Tools::getValue('dateR') == "<="){
                                $query = 'SELECT id_address, o.id_order, c.firstname, c.lastname, o.date_add, address1, address2, postcode, payment, a.other, total_paid FROM ps_orders o, ps_customer c, ps_address a, ps_carrier ca WHERE o.id_shop=1 and  c.id_customer = o.id_customer AND a.id_address = o.id_address_delivery AND current_state = ' . Tools::getValue('id_order_state') . ' AND o.id_carrier = ca.id_carrier AND external_module_name != "mondialrelay" AND DATE(o.date_add) <=  "' . Tools::getValue('datepicker') .'"';
                            }
                        }
                    }else{
                       // $query = 'SELECT id_address,o.id_customer, o.id_order, c.firstname, c.lastname, o.date_add, address1, address2, postcode, payment, a.other, total_paid FROM ps_orders o, ps_customer c, ps_address a, ps_carrier ca WHERE c.id_customer = o.id_customer AND o.id_carrier = ca.id_carrier AND external_module_name != "mondialrelay" AND a.id_address = o.id_address_delivery AND current_state = ' . Tools::getValue('id_order_state');
					   $query='SELECT id_address,o.id_customer, o.id_order, c.firstname, c.lastname, o.date_add, address1, address2, postcode, payment, a.other, total_paid, SUM(IFNULL(p.weight,0)) as peso
									FROM ps_orders o
									INNER JOIN ps_customer c on (c.id_customer=o.id_customer)
									INNER JOIN ps_address a on (a.id_address=o.id_address_delivery)
									INNER JOIN ps_carrier ca on (ca.id_carrier=o.id_carrier)
									INNER JOIN ps_order_detail pod on (pod.id_order=o.id_order)
									LEFT JOIN ps_product p on (pod.product_id=p.id_product)
									WHERE o.id_shop=1 and external_module_name != "mondialrelay" AND current_state ='. Tools::getValue('id_order_state').
									' group by o.id_order';
                    }
                    
                }
                else{
                   // $query = 'SELECT id_address,o.id_customer, o.id_order, c.firstname, c.lastname, o.date_add, address1, address2, postcode, payment, a.other, total_paid FROM ps_orders o, ps_customer c, ps_address a, ps_carrier ca WHERE c.id_customer = o.id_customer AND o.id_carrier = ca.id_carrier AND external_module_name != "mondialrelay" AND a.id_address = o.id_address_delivery AND current_state = 3';     
				     $query='SELECT id_address,o.id_customer, o.id_order, c.firstname, c.lastname, o.date_add, address1, address2, postcode, payment, a.other, total_paid, SUM(IFNULL(p.weight,0)) as peso
									FROM ps_orders o
									INNER JOIN ps_customer c on (c.id_customer=o.id_customer)
									INNER JOIN ps_address a on (a.id_address=o.id_address_delivery)
									INNER JOIN ps_carrier ca on (ca.id_carrier=o.id_carrier)
									INNER JOIN ps_order_detail pod on (pod.id_order=o.id_order)
									LEFT JOIN ps_product p on (pod.product_id=p.id_product)
									WHERE o.id_shop=1 and external_module_name != "mondialrelay" AND current_state =3 group by o.id_order';
                }
                $orders = array();
				/**** AARIAS **/
				$contador_pedidos=array();				
				/******** END AARIAS**/
                global $cookie;
                if ($results = Db::getInstance()->ExecuteS($query)){
					
					/********AARIAS primero cuento el número de pedidos de cada cliente **/
					foreach ($results as $row){
						$contador_pedidos[$row['id_customer']]=0;
					}
					foreach ($results as $row){
						$contador_pedidos[$row['id_customer']]++;
					}
					/******** END AARIAS**/
					
                    foreach ($results as $row){
                        $order = array(
                            'id_order' => $row['id_order'],
                            'firstname' => $row['firstname'],
                            'lastname' => $row['lastname'],
							'num_pedidos' =>$contador_pedidos[$row['id_customer']], //AAV:añado el contador de pedido de cada usuario
                            'date_add' => $row['date_add'],
                            'address1' => $row['address1'],
                            'address2' => $row['address2'],
                            'postcode' => $row['postcode'],
                            'payment' => $row['payment'],
                            'other' => $row['other'],
                            'id_address' => $row['id_address'],
							'peso' => $row['peso'],
                            'token' => Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)$this->context->employee->id)
                        );
                        
                        
                        $query2 = 'SELECT message FROM ps_customer_message cm, ps_customer_thread ct, ps_orders o WHERE ct.id_customer_thread = cm.id_customer_thread AND o.id_order = ct.id_order AND o.id_order = ' . $row['id_order'];
                        $messages = array();
                        if ($results2 = Db::getInstance()->ExecuteS($query2)){
                            foreach ($results2 as $row2){
                                if(strlen($row2['message']) >= 120){
                                    
                                    if(strlen($row2['message']) >= 360){
                                        $messages[] = "PARTE 1: " . substr($row2['message'], 0, 120);
                                        $messages[] = "PARTE 2: " . substr($row2['message'], 120, 120);
                                        $messages[] = "PARTE 3: " . substr($row2['message'], 240, 120);
                                        $messages[] = "PARTE 4: " . substr($row2['message'], 360, 20000000);
                                        
                                    }else if(strlen($row2['message']) >= 240){
                                        $messages[] = "PARTE 1: " . substr($row2['message'], 0, 120);
                                        $messages[] = "PARTE 2: " . substr($row2['message'], 120, 120);
                                        $messages[] = "PARTE 3: " . substr($row2['message'], 240, 20000000);
                                    }else{
                                        $messages[] = "PARTE 1: " . substr($row2['message'], 0, 120);
                                        $messages[] = "PARTE 2: " . substr($row2['message'], 120, 20000000);
                                    }
                                }else{
                                    $messages[] = $row2['message'];
                                }
                            }
                        }
                        
                        $query3 = 'SELECT product_name, product_quantity FROM ps_order_detail WHERE id_order = ' . $row['id_order'];
                        $products = array();
                        if ($results3 = Db::getInstance()->ExecuteS($query3)){
                            foreach ($results3 as $row3){
                                 $products[] = $row3['product_name'] . ": <b>" . $row3['product_quantity'] ."</b>";
                            }
                        }
                                                        
                        $order['products'] = $products;
                        $order['messages'] = $messages;
                        
                        $orders[] = $order;
                    }
                }
                    
                    
                $this->context->smarty->assign(array(
                    'orders' => $orders,
                    'states2' => $history2,         
                    'states' => $history));
                    
                    
               	$this->setTemplate('../../../../themes/fit-crunch-it/modules/massivestates/index.tpl');
            }
	   }
			
		}
		
	}
}

?>