<?php
require('fpdf.php');

$productos = array();
$i = 0;
if (($fichero = fopen("../../ordersproducts.csv", "r")) !== FALSE) {
    while (($datos = fgetcsv($fichero, 1000, ";")) !== FALSE) {
		if($i == 0){
			$i = 1;
		}else{
			$producto = array('Cantidad' => $datos[0], 'Localizacion' => $datos[1], 'Referencia' => $datos[2],
			'EAN13' => $datos[3], 'Nombre' => $datos[4]);
			$productos[] = $producto;
		}
    }
}


$pdf=new FPDF("L", 'mm', 'A5');                         
$pdf->AliasNbPages();                 
$pdf->AddPage();                      
 
$pdf->SetFont('Arial','B',6);           
$pdf->SetXY(5,10);                      
$pdf->SetFillColor(236,235,236);        
$pdf->Cell(10,4,'Cantidad',1,0,'C',1);
$pdf->Cell(20,4,'Localización',1,0,'C',1);  
$pdf->Cell(25,4,'Referencia',1,0,'C',1);    
$pdf->Cell(40,4,'EAN13',1,0,'C',1);        
$pdf->Cell(105,4,'Nombre',1,0,'C',1);      

$pos_y  =   14;
$i = 0;
 
foreach ($productos as $producto)
{	
	if($i >= 25){
		$pdf->AliasNbPages();                 
		$pdf->AddPage();                      
		 
		$pdf->SetFont('Arial','B',6);           
		$pdf->SetXY(5,10);                      
		$pdf->SetFillColor(236,235,236);        
		$pdf->Cell(10,4,'Cantidad',1,0,'C',1);
		$pdf->Cell(20,4,'Localización',1,0,'C',1);  
		$pdf->Cell(25,4,'Referencia',1,0,'C',1);    
		$pdf->Cell(40,4,'EAN13',1,0,'C',1);        
		$pdf->Cell(105,4,'Nombre',1,0,'C',1);      

		$pos_y  =   14;
		$i = 0;
	}
	
    $pdf->SetFont('Arial','B',6);
    $pdf->SetXY(5,$pos_y);
    $pdf->SetFillColor(999,999,999);
    $pdf->Cell(10,4,$producto['Cantidad'],1,0,'C',1);
    $pdf->Cell(20,4, $producto['Localizacion'],1,0,'C',1);
    $pdf->Cell(25,4, $producto['Referencia'],1,0,'C',1);
	$pdf->Cell(40,4, $producto['EAN13'],1,0,'C',1);
    $pdf->Cell(105,4, $producto['Nombre'],1,0,'L',1);
    $pos_y+=4;
	$i++;
}

$pdf->Output(); 

?>