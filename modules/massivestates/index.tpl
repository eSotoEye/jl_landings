
<script type="text/javascript" src="http://teletiendadirecto.com/ERPresta/libraries/jquery.dataTables.min.js"></script>
<link href="http://teletiendadirecto.com/ERPresta/global.tables2.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="http://teletiendadirecto.com/ERPresta/libraries/zebra_datepicker.js"></script>  
<link type="text/css" rel="stylesheet" href="http://teletiendadirecto.com/ERPresta/libraries/default.css" />
<script type="text/javascript">
$(document).ready(function(){
    
        var d = new Date();
        
        function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
        
        $('.datepicker').Zebra_DatePicker({
             format: 'Y-m-d',
             days_abbr: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sab"],
             months: ['Enero',"Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
             lang_clear_date: "Limpiar"
        });
        
    
        $('#dtProduct').DataTable({
            "aLengthMenu": [[-1], ["Todas"]],
            "aaSorting": [[0,'desc']],
            "language": {
                "search": "Buscar:",
                "lengthMenu":     "Mostrar _MENU_ entradas",
                "zeroRecords":    "No se han encontrado resultados",
    			"paginate": {
    				"first":      "Primero",
    				"last":       "Último",
    				"next":       "Siguiente",
    				"previous":   "Anterior"
    			},
                "aaSorting": [[2,'desc']],
    			"info":           "Mostrando registros del _START_ al _END_ de _TOTAL_ entradas",
                "infoEmpty":      "Mostrando registros del 0 al 0 de 0 entradas",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        
    $('input[type="checkbox"]').on('click', function(){
        idFila = this.id.replace("check","");
        id=$("#" + idFila).html();
        if ( $(this).is(':checked') ) {
            if( $("#ordersChecked").html().length == 0){
                $("#ordersChecked").html($("#ordersChecked").html() + id);
            }else{
                $("#ordersChecked").html($("#ordersChecked").html() + ", "  + id);
            }
        }else{
            replaced = $("#ordersChecked").html().replace(", " + id, "");           
            $("#ordersChecked").html(replaced);
            replaced = $("#ordersChecked").html().replace(id, "");           
            $("#ordersChecked").html(replaced);
        }
        
    });
    
    $('#buttonMessages').click(function(){
        
            totalOrders = $(".orderwrapper").length;
            for(var i = 1; i <= totalOrders; i++){
                
            fila = $('#listOrders tr:nth-child('+i+')').attr('id');
            fila = fila.replace('order', '');
                
            if(!$('#check' + fila).is(':checked')){    
                if( $("#messages" + fila).html().length == 194 ){
                    $('#check' + fila).prop('checked', true);
                    id=$("#" + fila).html();
                    
                    if( $("#ordersChecked").html().length == 0){
                        $("#ordersChecked").html($("#ordersChecked").html() + id);
                    }else{
                        $("#ordersChecked").html($("#ordersChecked").html() + ", "  + id);
                    }
                }
            }
        }
        
    });
	
	$('#buttonCorreos').click(function(){
        
        totalOrders = $(".orderwrapper").length;
        for(var i = 1; i <= totalOrders; i++){
            
            fila = $('#listOrders tr:nth-child('+i+')').attr('id');
            fila = fila.replace('order', '');
			
            if( $("#trans" + fila).html().indexOf("Correos") > -1 ){
                if(!$('#check' + fila).is(':checked')){
                    $('#check' + fila).prop('checked', true);
                    id=$("#" + fila).html();
                    
                    if( $("#ordersChecked").html().length == 0){
                        $("#ordersChecked").html($("#ordersChecked").html() + id);
                    }else{
                        $("#ordersChecked").html($("#ordersChecked").html() + ", "  + id);
                    }
                }
            }
        }
        
    });
	
	$('#buttonGLS').click(function(){
        
        totalOrders = $(".orderwrapper").length;
        for(var i = 1; i <= totalOrders; i++){
            
            fila = $('#listOrders tr:nth-child('+i+')').attr('id');
            fila = fila.replace('order', '');
			
            if( $("#trans" + fila).html().indexOf("GLS") > -1 && $("#tPayment" + fila).html().indexOf("Pago contra reembolso") == -1){
                if(!$('#check' + fila).is(':checked')){
                    $('#check' + fila).prop('checked', true);
                    id=$("#" + fila).html();
                    
                    if( $("#ordersChecked").html().length == 0){
                        $("#ordersChecked").html($("#ordersChecked").html() + id);
                    }else{
                        $("#ordersChecked").html($("#ordersChecked").html() + ", "  + id);
                    }
                }
            }
        }
        
    });
    
    $('#buttonWMessages').click(function(){
        
        totalOrders = $(".orderwrapper").length;
        for(var i = 1; i <= totalOrders; i++){
            
             fila = $('#listOrders tr:nth-child('+i+')').attr('id');
            fila = fila.replace('order', '');
            
            if( $("#messages" + fila).html().length != 194 ){
                
                if(!$('#check' + fila).is(':checked')){
                    $('#check' + fila).prop('checked', true);
                    id=$("#" + fila).html();
                    
                    if( $("#ordersChecked").html().length == 0){
                        $("#ordersChecked").html($("#ordersChecked").html() + id);
                    }else{
                        $("#ordersChecked").html($("#ordersChecked").html() + ", "  + id);
                    }
                }
            }
        }
        
    });
    
    $('#buttonToday').click(function(){
            totalOrders = $(".orderwrapper").length;
            for(var i = 1; i <= totalOrders; i++){
                
            fila = $('#listOrders tr:nth-child('+i+')').attr('id');
            fila = fila.replace('order', '');
                
            if( $("#date" + fila).html().indexOf(d.getFullYear() + "-" + parseInt(d.getMonth()+1) + "-" + d.getDate()) != -1 ){
                
                 if(!$('#check' + fila).is(':checked')){
                    $('#check' + fila).prop('checked', true);
                    id=$("#" + fila).html();
                    
                    if( $("#ordersChecked").html().length == 0){
                        $("#ordersChecked").html($("#ordersChecked").html() + id);
                    }else{
                        $("#ordersChecked").html($("#ordersChecked").html() + ", "  + id);
                    }
                }
            }
        }
        
    }); 
    
    $('#buttonS').click(function(){
        totalOrders = $(".orderwrapper").length;
        for(var i = 1; i <= totalOrders; i++){
            
            fila = $('#listOrders tr:nth-child('+i+')').attr('id');
            fila = fila.replace('order', '');
            
            $('#check' + fila).prop('checked', true);
            id=$("#" + fila).html();
            
            if( $("#ordersChecked").html().length == 0){
                $("#ordersChecked").html($("#ordersChecked").html() + id);
            }else{
                $("#ordersChecked").html($("#ordersChecked").html() + ", "  + id);
            }
        }
        
    });
    
    $('#buttonDS').click(function(){
        totalOrders = $(".orderwrapper").length;
        for(var i = 1; i <= totalOrders; i++){
            
            fila = $('#listOrders tr:nth-child('+i+')').attr('id');
            fila = fila.replace('order', '');
                                    
            $('#check' + fila).prop('checked', false);
        }
        $("#ordersChecked").html("")
    });  
    
    $('#listOrders tr').click(function(event) {
        if (event.target.type !== 'checkbox' && event.target.type !== 'select-one') {
                        
            if ( !$(':checkbox', this).is(':checked') ) {
                $(':checkbox', this).prop('checked', true);
                idFila = this.id.replace("order",""); 
                id=$("#" + idFila).html();
              
              
                if( $("#ordersChecked").html().length == 0){
                    $("#ordersChecked").html($("#ordersChecked").html() + id);
                }else{
                    $("#ordersChecked").html($("#ordersChecked").html() + ", "  + id);
                }
            }else{
                idFila = this.id.replace("order",""); 
                id=$("#" + idFila).html();
                $(':checkbox', this).prop('checked', false);
                replaced = $("#ordersChecked").html().replace(", " + id, "");           
                $("#ordersChecked").html(replaced);
                replaced = $("#ordersChecked").html().replace(id, "");           
                $("#ordersChecked").html(replaced);
            }
            
          
        }
      });
      
      $('#buttonClients').click(function(event) {
            
        totalOrders = $(".orderwrapper").length;
        
        
        for(var i = 1; i < totalOrders; i++){
            
            
            if(!$("#" + i).hasClass('checked')){
            
               name=$("#client" + i).html().replace(/\s/g,'');               
               color = getRandomColor();
               for(var o = i+1; o <= totalOrders; o++){
                    name2=$("#client" + o).html().replace(/\s/g,'');
                    if(name == name2){
                        $("#" + i).css("background-color", color);
                        $("#" + o).css("background-color", color);
                        $("#" + i).addClass("checked");
                        $("#" + o).addClass("checked");
                    }
               } 
           }
        }
            
      });
	  
      $('#buttonCan').click(function(event) {
            
        totalOrders = $(".orderwrapper").length;
        color = getRandomColor();
        
        for(var i = 1; i <= totalOrders; i++){
            
            if(/^35.*/.test($("#pc" + i).html()) == true){
                $("#pc" + i).css("background-color", color);
            }
            if(/^38.*/.test($("#pc" + i).html()) == true){
                $("#pc" + i).css("background-color", color);
            }
            if(/^51.*/.test($("#pc" + i).html()) == true){
                $("#pc" + i).css("background-color", color);
            }
            if(/^52.*/.test($("#pc" + i).html()) == true){
                $("#pc" + i).css("background-color", color);
            }
			{*
            if(/^0786.*/.test($("#pc" + i).html()) == true){
                $("#pc" + i).css("background-color", color);
            }
            if(/^0787.*/.test($("#pc" + i).html()) == true){
                $("#pc" + i).css("background-color", color);
            }
			*}
        }
            
      });
      
      setTimeout(function() {
        }, 5000);
        
        
      
      $('#buttonState').click(function(event) {
        
        if(confirm("¿Estás seguro que desea cambiar el estado de los pedidos?")){
            newstate = $('#id_state').val();
            arrayt = $("#ordersChecked").html().split(/\W+/);
            
            console.log(arrayt.length);
            for(var i = 0; i < arrayt.length; i++){
                
                changeState(arrayt[i], newstate, "Correos");
            }
            
        } 
        
            
      });
	  
	  $('#buttonState2').click(function(event) {
           
        if(confirm("¿Estás seguro que desea cambiar el estado de los pedidos?")){
            newstate = $('#id_state').val();
            arrayt = $("#ordersChecked").html().split(/\W+/);
            
            console.log(arrayt.length);
            for(var i = 0; i < arrayt.length; i++){
                
                changeState(arrayt[i], newstate, "GLS");
            }
            
        } 
        
            
      });
      
      
      function changeState(numOrder, newstate, trans) {
        if(numOrder.length > 1){
            console.log(numOrder + " " + newstate);
            $.ajax({
                url: "http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}",
                data: "orderprocess="+numOrder+"&stateprocess="+newstate+"&trans="+trans,
                type: "post",
                async:false, 
                success: function(response) { 
                    $("#loading2").hide();
                    console.log("sucess");
                    replaced = $("#ordersChecked").html().replace(numOrder, "<b style='color: green;'>" + numOrder + "</b>");           
                    $("#ordersChecked").html(replaced);
                    $("#buttonPrint").show();
                    $("#buttonPrint2").show();
					 $("#buttonPrint3").show();
                },
                beforeSend: function( xhr ) {
                    $("#loading2").css("display","inline-block");
                },
                error: function(xhr, ajaxOptions, thrownError){
                    console.log(xhr);
                    console.log(thrownError);
                    console.log(numOrder + newstate);
                    //alert("Ha ocurrido un error con la actualización.");
                }
            });   
            
        } 
        
      
    }
    
    $('#buttonPrint').click(function(event) {
                
        arrayt = replaced.split(/\W+/); 
        
        $.ajax({
            url: "http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}",
            data: "orders="+serialize(arrayt)+"&sh=1",
            type: "post",
            success: function(response) { 
                $("#loading2").hide();
                console.log(response);
                $("#exportcsv").show();
				$("#exportcsvproduct").show();
            },
            beforeSend: function( xhr ) {
                $("#loading2").css("display","inline-block");
            },
            error: function(xhr, ajaxOptions, thrownError){
                //console.log(xhr);
                //console.log(thrownError);
            }
        }); 
        
            
    });
	
	$('#buttonPrint2').click(function(event) {
        
        arrayt = replaced.split(/\W+/);
        
        $.ajax({
            url: "http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}",
            data: "orders="+serialize(arrayt)+"&sh=2",
            type: "post",
            success: function(response) { 
                $("#loading2").hide();
                console.log(response);
                $("#exportcsv").show();
				$("#exportcsvproduct").show();
            },
            beforeSend: function( xhr ) {
                $("#loading2").css("display","inline-block");
            },
            error: function(xhr, ajaxOptions, thrownError){
                //console.log(xhr);
                //console.log(thrownError);
            }
        }); 
        
            
    });
	
		$('#buttonPrint3').click(function(event) {
        
        arrayt = replaced.split(/\W+/);
        
        $.ajax({
            url: "http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}",
            data: "orders="+serialize(arrayt)+"&sh=3",
            type: "post",
            success: function(response) { 
                $("#loading2").hide();
                console.log(response);
                $("#exportcsv").show();
				$("#exportcsvproduct").show();
            },
            beforeSend: function( xhr ) {
                $("#loading2").css("display","inline-block");
            },
            error: function(xhr, ajaxOptions, thrownError){
                //console.log(xhr);
                //console.log(thrownError);
            }
        }); 
        
            
    });
    
    
    {literal}
    function serialize(arr){
        var res = 'a:'+arr.length+':{';
        for(i=0; i<arr.length; i++){
            res += 'i:'+i+';s:'+arr[i].length+':"'+arr[i]+'";';
        }
        res += '}';
         
        return res;
    }
    {/literal}
    
    
	
});
	
</script>
<style type="text/css">
    #dtProduct_wrapper{
        max-width: 98% !important;   
    }
    th{
        font-weight: normal;
        text-align: center;
    }
    
    .orderwrapper{
        cursor: pointer;
    }
    
    #loading2{
    background-color: #5ccdde; 
    width: 25px; 
    height: 25px; 
    -moz-animation: rotateplane .8s infinite ease-in-out; 
    -webkit-animation: rotateplane .8s infinite ease-in-out;
    animation: rotateplane .8s infinite ease-in-out;
    margin-bottom: -9px;
    margin-left: 20px;
    display: inline-block;
    display: none;
}

@-webkit-keyframes rotateplane{
    0%{
        -webkit-transform:perspective(120px)
    }
    50%{
        -webkit-transform:perspective(120px) rotateY(180deg)
        }
    100%{
        -webkit-transform:perspective(120px) rotateY(180deg) rotateX(180deg)
    }
}

@keyframes rotateplane{
    0%{
        transform:perspective(120px) rotateX(0deg) rotateY(0deg);
        -webkit-transform:perspective(120px) rotateX(0deg) rotateY(0deg)
    }
    50%{
        transform:perspective(120px) rotateX(-180.1deg) rotateY(0deg);
        -webkit-transform:perspective(120px) rotateX(-180.1deg) rotateY(0deg)
    }
    100%{
        transform:perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
        -webkit-transform:perspective(120px) rotateX(-180deg) rotateY(-179.9deg)
    }
}
</style>
<div>
	<form action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}" method="get" class="form">
		<fieldset>
			<legend><img src="../modules/massivestates/logo.gif" />Filtrar por estado</legend>
			<label for="id_order_state">Estado del pedido</label>
			<div class="margin-form">
				<select id="id_order_state" name="id_order_state" style="width:250px" style="background-color:#DDFFAA;">
                    {foreach from=$states key=num_history item=history}
                    
                        <option value="{$history.id_order_state|intval}" {if (empty($smarty.get.id_order_state))} {if ($history.id_order_state|intval == 3)} selected="selected" style="background-color:#DDFFAA;" {/if}{else}{if ($history.id_order_state|intval == $smarty.get.id_order_state)} selected="selected"  style="background-color:#DDFFAA;" {/if}{/if} >{$history.name}</option>
                        
                    {/foreach}
				</select>
				<p>
				Elegir el estado del pedido por filtrar
				</p>
			</div>
            
            <label for="id_order_state">Fecha</label>
			<div class="margin-form">
                <select id="dateR" name="dateR">
                    <option>>=</option>
                    <option><=</option>
                    <option>=</option>
                </select>
				<input class="datepicker" id="datepicker" type="text" style="width: 80px;" name="datepicker" />
				<p>
				Elegir fecha para filtrar (Dejar en blanco para ignorar filtro de fecha)
				</p>
			</div>

			<div class="clear"></div>
			<div class="margin-form">
				<input type="submit" value="Filtrar" class="button" />
			</div>
            
		</fieldset>
        <input type="hidden" name="controller" value="AdminMassiveStates" />
        <input type="hidden" name="token" value="{$smarty.get.token}" />
	</form>
</div>
<br />
<br />

<div>
	<fieldset>
		<legend>Pedidos marcados</legend>
		<label for="id_order_state">Pedidos marcados: </label>
        <span id="ordersChecked"></span>
		<div class="margin-form">
			
		</div>

		<div class="clear"></div>
        
        <br />
        <br />
        
        
        <button class="button" id="buttonS">Seleccionar todos</button>
        <button class="button" id="buttonDS">Deseleccionar todos</button>
        <button class="button" id="buttonMessages">Marcar todos los pedidos SIN mensajes</button>
        <button class="button" id="buttonWMessages">Marcar todos los pedidos CON mensajes</button>
        <button class="button" id="buttonToday">Marcar pedidos de hoy</button>
        <br /><br />
        <button class="button" id="buttonClients">Mostrar pedidos del mismo cliente</button>
        <button class="button" id="buttonCan">Mostrar pedidos Canarias</button>
		<br /><br />
		<button class="button" id="buttonGLS">Marcar pedidos GLS</button>
		<button class="button" id="buttonCorreos">Marcar pedidos Correos</button>
		<br /><br />
        <button class="button" id="buttonPrint" style="display: none; background: none; background-color: rgb(249, 249, 42) !important; text-shadow: none;">Etiquetas Correo Express</button>
		<button class="button" id="buttonPrint2" style="display: none; background: none; background-color: rgb(174, 244, 126) !important; text-shadow: none;">Etiquetas GLS</button>
		<button class="button" id="buttonPrint3" style="display: none; background: none; background-color: rgb(60, 102, 255) !important; text-shadow: none;">Etiquetas Zeleris</button>
        <a style="display: none; font-weight: bold;" id="exportcsv" href="http://renumax-car.com/export.csv" >Descargar excel de etiquetas</a>
		<a style="display: none; font-weight: bold;" id="exportcsvproduct" href="http://renumax-car.com/modules/massivestates/genpdf.php" > - Descargar ficha de productos</a>
        
	</fieldset>
</div>
<br />
<br />
<div>
	<fieldset>
		<legend>Marcar pedidos</legend>
		<label for="id_order_state">Nuevo estado del pedido</label>
			<div class="margin-form">
				<select id="id_state" name="id_state" style="width:250px" style="background-color:#DDFFAA;">
                    {foreach from=$states2 key=num_history item=history2}
                    
                        <option value="{$history2.id_order_state|intval}" {if ($history2.id_order_state|intval == 4)} selected="selected" style="background-color:#DDFFAA;" {/if} >{$history2.name}</option>
                        
                    {/foreach}
				</select>
                <button class="button" id="buttonState" >Cambiar estados</button>
                <div id="loading2"></div>
			</div>
        
	</fieldset>
</div>
<br />
<br />
<div>
	<fieldset>
			<legend>Lista de pedidos filtrados</legend>
			
            <table id="dtProduct" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Ir</th>
						<th style="width: 30px;">Transportista</th>
                        <th style="width: 30px;">ID</th>
                        <th style="width: 111px;">Cliente</th>
                        <th style="width: 80px;">Fecha</th>
                        <th style="width: 180px;">Mensajes</th>
                        <th style="width: 180px;">Productos</th>
                        <th style="width: 130px;">Dirección</th>
                        <th style="width: 130px;">Mensajes en dirección</th>
                        <th style="width: 20px;">C.Postal</th>
                        <th style="width: 80px;">Tipo de pago</th>
                        <th style="width: 20px;">Editar dir.</th>
                        <th style="width: 20px;">Seleccionar</th>
                    </tr>
                </thead>
                <tbody id="listOrders">
                    {foreach from=$orders key=list_orders item=order}
                        {$count = $order@iteration}
                        <tr class="orderwrapper" id="order{$count}" {if ($order.payment == 'Pago contra reembolso' OR $order.payment == 'Pago con tarjeta') AND $order.total_paid >= 300}style="background-color: #F58C8C"{else if $order.payment == 'Paypal' AND $order.total_paid >= 500}style="background-color: #FBFCE8"{else if ($order.esGLS AND $order.payment != 'Pago contra reembolso')}style="background-color: #FBFCE8"{else}style="background-color: #FFF7F7"{/if}>
                            <th><a href="http://renumax-car.com/eye84/index.php?controller=AdminOrders&id_order={$order.id_order}&vieworder" ><img src="http://renumax-car.com/img/admin/edit.gif" /></a></th>
                            <th id="trans{$count}">{if ($order.esGLS AND $order.payment != 'Pago contra reembolso')}
								GLS
							{else}
								Correos
							{/if}</th>
							<th id="{$count}">{$order.id_order}</th>
                            <th id="client{$count}">{$order.firstname} {$order.lastname}</th>
                            <th id="date{$count}">{$order.date_add}</th>
                            <th id="messages{$count}">
                               
                                {if $order.messages|@count > 1}
                                    <select>
                                    {foreach from=$order.messages key=list_messages item=message}
                                            <option>{$message}</option>
                                    {/foreach}
                                     </select>
                                {else}
                                    {foreach from=$order.messages key=list_messages item=message}
                                        {$message}
                                    {/foreach}
                                {/if}
                                {if ($order.payment == 'Pago contra reembolso' OR $order.payment == 'Pago con tarjeta') AND $order.total_paid >= 300}- ¡Más de 300€!{/if}
								{if $order.payment == 'Paypal' AND $order.total_paid >= 500}- ¡Más de 500€!{/if}
						   </th>
                            <th id="products{$count}">
                               
                                {if $order.products|@count > 1}
                                    <select>
                                    {foreach from=$order.products key=list_products item=product}
                                            <option>{$product}</option>
                                    {/foreach}
                                     </select>
                                {else}
                                    {foreach from=$order.products key=list_products item=product}
                                        {$product}
                                    {/foreach}
                                {/if}
                                
                            </th>
                            <th>{$order.address1} {$order.address2}</th>
                            <th>{$order.other}</th>
                            <th id="pc{$count}">{$order.postcode}</th>
                            <th id="tPayment{$count}">{$order.payment}</th>
                            <th><a href="http://renumax-car.com/eye84/index.php?tab=AdminAddresses&token=48a6fa9cd4a7430491c58cb90e6a0e5d&realedit=1&id_order={$order.id_order}&id_address={$order.id_address}&addaddress" target="_blank"><img src="http://renumax-car.com/img/admin/edit.gif" /></a></th>
                            <th><input type="checkbox" class="checked" id="check{$count}" /></th>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
            
			<div class="clear"></div>
	</fieldset>
</div>

