<?php
/**
*
*	Joshua Leiva - soporte@teletiendadirecto.com
*	
*	Módulo capaz de gestionar multitud de pedidos para pasarlos a enviados
*
*   Todos los derechos reservados
*   Copyright 2014
*  
*/

if (!defined('_PS_VERSION_'))
	exit;
	
class massivestates extends Module
{
	public function __construct()
	{
		$this->name		= 'massivestates';
		$this->tab		= 'shipping_logistics';
		$this->version	= '1.0';
		$this->installed_version = '';
		$this->author = 'Joshua Leiva';
        $this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Massive States');
		$this->description = $this->l('Módulo capaz de cambiar masivamente el estado de multitud de pedidos');
	}

	public function install()
	{
		if (!parent::install())
			return false;
		
		$result = Db::getInstance()->getRow('
			SELECT id_tab
			FROM `'._DB_PREFIX_.'tab`
			WHERE class_name="AdminMassiveStates"');
		
		if (!$result)
		{	
			$tab = new Tab();
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
				$tab->name[$language['id_lang']] = 'Cambio de estados masivo';
			$tab->class_name = 'AdminMassiveStates';
			$tab->module = 'massivestates';
			$tab->id_parent = Tab::getIdFromClassName('AdminOrders');

			if (!$tab->add())
				return false;
		}
		
		return true;
	}

	/*
	** Función encargada de registrar los hooks
	*/
	private function registerHooks()
	{      
	   
	    if(!$this->registerHook('orderProcessStatus') || !$this->registerHook('adminOrder') || !$this->registerHook('orderProductModified'))
            return false;
            
		return true;
	}
		
	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
			
		if (($id_tab = Tab::getIdFromClassName('massivestatesadmin')))
		{
			$tab = new Tab($id_tab);
			$tab->delete();
		}
            
		return true;
	}

	
	
}
?>