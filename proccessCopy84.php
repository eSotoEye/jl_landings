<?php

set_time_limit (0);
ini_set('default_socket_timeout', 60000);
ini_set('memory_limit', '-1');

$inicio = microtime(true); 

/*
	Para realizar copias de seguridad de prestashop y de su BD
*/

//Establezco ver todos los errores
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'on');

/*FUNCIONES*/
function comprobarGET($parametro){
	if(isset($parametro) AND $parametro!=""){
		return true;
	}else{
		return false;
	}
}

if(comprobarGET($_GET['copia']) AND comprobarGET($_GET['accion'])){
	
	/*parametros por GET*/
	$tipo_copia = $_GET['copia'];
	$accion = $_GET['accion'];
	
	$copiarScript = false;
	
	//realizamos la copia de seguridad de las tablas que contienen datos (las tablas vacias las desechamos)
	$rutaActual = exec('pwd');
	
	//extraemos los directorios
	//obtenemos la fecha en la que se realiza la copia de seguridad
	date_default_timezone_set('UTC');
	$fecha = date("d-m-Y");
	
	switch($accion){
		case "copiar":
			$host="127.0.0.1";
			$user="p4941_4";
			$pass="6f0TLkiztbgy";
			$database="p4941_4";
			$oConni = new mysqli($host,$user,$pass,$database) or die("error al conectar...");
			
			switch($tipo_copia){
				case "diaria":
					$copiarScript = true;
					$directorios = 'classes controllers override themes';
					$termino = 'd';
				break;
				case "semanal":
					$copiarScript = true;
					$directorios = 'classes controllers override themes mails modules';
					$termino = 's';
				break;
				//caso especial de las imagenes
				case "imagenes":
					$copiarScript = false;
					$directorios = 'img';
					$termino = 'i';
				break;
				default:
					echo('<p>No ha pasado un tipo de copia v&aacute;lido.</p>');
				break;
			}
			
			///////////////REALIZACION DEL PROCESO DE COPIA DE SEGURIDAD //////////////////
			

			$arrayTablasCS=array();

			$stmt=$oConni->prepare("show tables");
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($tabla);
			while($stmt->fetch()){
				
				//compruebo si dicha tabla contiene registros
				$resultado = $oConni->query("select * from ".$tabla);
				
				if(count($fila = $resultado->fetch_row())>0 AND $tabla!='ps_cart' AND $tabla!='ps_cart_rule_product_rule_value' AND $tabla!='ps_connections' AND $tabla!='ps_connections_source' AND $tabla!='ps_connections_page' AND $tabla!='ps_guest' AND $tabla!='ps_log' AND $tabla!='ps_mrwcarrier_mrw' AND $tabla!='ps_pagenotfound' AND $tabla!='ps_search_index' AND $tabla!='ps_access' AND $tabla!='ps_search_word' AND $tabla!='ps_sekeyword' AND $tabla!='ps_statssearch'){
					array_push($arrayTablasCS,$tabla);
				}
				
			}

			$stmt->close();

			$cadTablas='';
			for($i=0;$i<count($arrayTablasCS);$i++){
				$cadTablas.=$arrayTablasCS[$i].' ';
			}
			if($copiarScript){
				$operacion = 'mysqldump --opt --host='.$host.' --user='.$user.' --password=\''.$pass.'\' '.$database.' '.$cadTablas.'> \''.$rutaActual.'/copia_seguridad.sql\'';

				exec($operacion);
			}
			

			//creamos una carpeta con la fecha del dia en el que se realiza la copia y copiamos dentro tanto los directorios principales como el script SQL
			exec('mkdir '.$rutaActual.'/copia_'.$fecha);
			if($copiarScript){
				exec('cp -R '.$directorios.' copia_seguridad.sql '.$rutaActual.'/copia_'.$fecha);
			}else{
				exec('cp -R '.$directorios.' '.$rutaActual.'/copia_'.$fecha);
			}
			

			//comprimimos dicha carpeta en un archivo comprimido (que posteriormente va a ser llamado desde consola para ser descargado)
			exec('tar -zcvf '.$rutaActual.'/copia_'.$fecha.'_'.$termino.'.tar.gz copia_'.$fecha);
					
					
		break;
		case "eliminar":
		
			//borramos los residuos de la copia de seguridad para dejar limpio el servidor
			exec('rm -Rf '.$rutaActual.'/copia_seguridad.sql');
			exec('rm -Rf '.$rutaActual.'/copia_'.$fecha);
			exec('rm -Rf '.$rutaActual.'/copia_'.$fecha.'_*.tar.gz');
		
			//enviamos un correo de confirmacion de copia al administrador
			mail('soporte+copia@eyeinversionesonline.com', 'Copia de seguridad realizada el '.$fecha, 'Copia de seguridad realizada en Fit-Crunch: '.$fecha);
			mail('soporte.edu@eyeinversionesonline.com', 'Copia de seguridad realizada el '.$fecha, 'Copia de seguridad realizada en Fit-Crunch: '.$fecha);
		
		break;
		default:
			echo('<p>No ha pasado como par&aacute;metro una opci&oacute;n v&aacute;lida.</p>');
		break;
	}	
	
}else{
	echo '<p>error en los par&aacute;metros</p>';
}

$fin = microtime(true);
$tiempoTotal = $fin - $inicio;
echo 'tiempo total: '.$tiempoTotal;