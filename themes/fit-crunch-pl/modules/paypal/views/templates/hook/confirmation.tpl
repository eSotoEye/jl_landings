{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<p>Zamówienie <span class="bold">{$shop_name|escape:'htmlall':'UTF-8'}</span> zostało poprawnie zapisane
	<br /><br />
	Jako sposób zapłaty został wybrany PayPal
	<br /><br /><span class="bold">
Zamówienie zostanie wysłane tak szybko, jak to będzie to możliwe
</span>
	<br /><br />W razie jakichkolwiek wątpliwości lub zapytań prosimy o kontakt z naszym 
	<a href="{$link->getPageLink('contact', true)}" data-ajax="false" target="_blank">serwisem obsługi klienta .</a>.
</p>
