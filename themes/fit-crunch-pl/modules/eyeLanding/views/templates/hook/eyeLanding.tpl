<link rel="stylesheet" href="{$module_dir}/slick/slick-theme.css">
<link rel="stylesheet" href="{$module_dir}/slick/slick.css">
<link rel="stylesheet" href="{$module_dir}style.css">
<script type="text/javascript" src="{$module_dir}/slick/slick.js"></script>

{literal}
	<style>



	</style>
{/literal}
<div style="clear:both;"></div>



<!-- SECCIÓN PRINCIPAL QUE CONTENDRÁ TODA LA INFORMACIÓN DEL PRODUCTO RESUMIDA -->
<div class="mainSection">

		<!-- Indice de imagenes -->
		<div class="leftImgIndex">
			<span id="miniaturavideo" class="miniaturavideo"><img src="modules/eyeLanding/images/video.png" style="width:100%;" /></span>
			<span id="miniatura1" class="miniaturaImg"><img src="modules/eyeLanding/images/1.png" style="width:100%;" /></span>
			<span id="miniatura2" class="miniaturaImg"><img src="modules/eyeLanding/images/2.png" style="width:100%;" /></span>
			<span id="miniatura3" class="miniaturaImg"><img src="modules/eyeLanding/images/3.png" style="width:100%;" /></span>

		</div>
		<!-- Imágenes del producto-->
		<div class="leftImages">
			<img src="modules/eyeLanding/images/1.png" id="img1" />
		</div>

		<!-- Descipción del producto-->
		<div class="description">
		<div class="wrapper">
			<div class="productName">
				<span><h1 style="font-style:italic; font-weight:bold">Total Fit Crunch</h1></span>
				<div class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div>
			</div>
			<div class="productSubName"><p>TONIZUJ I I ĆWICZ SWÓJ UKŁAD KRĄŻENIA</p></div>
			<div class="productPrice"><span>329</span>,90zł</div> <span id="enviogratis">WYSYŁKA GRATIS</span>
				<ul style="">
					<li><i class="fa fa-check"></i>&nbsp;Dzięki Fit Crunch, ćwicząc tylko kilka minut dziennie, osiągniesz idealne ciało.</li>
					<li><i class="fa fa-check"></i>&nbsp;Odpowiedni do ćwiczeń o wysokim lub niskim obciążeniu z wykorzystaniem masy własnego ciała, ponadto pozwala na pracę nad mięśniami ramion, pleców, brzucha, pośladków i nóg w czasie tego samego ćwiczenia.</li>
					<li><i class="fa fa-check"></i>&nbsp;Siodełko o regulowanej wysokości i odległości, łatwy w składaniu i rozkładaniu, z licznikiem cyfrowym.<li>
				</ul>
			<p style="margin-top:20px;"><strong>Opcje płatności: </strong> karta lub Paypal.<a id="masinfoTarjeta" href="#">Więcej informacji</a></p>
			<div id="popupTarjetas"><img src="{$module_dir}/images/modalidades-de-pago.png" /> <span id="popupTarjetas_close">x</span></div>
			<p class="aviso" style="padding:10px;padding-left:0px; padding-top:0px;">
				<span>Wysyłka w ciągu 3-5 dni <span style="color:#64A954; font-size:14px;font-weight:bold ">GRATIS</span></span>
			</p>
			<div style="clear:both;"></div>

		<!-- Botones para la compra-->
		<div id="primerBoton"class="rightBuy col-lg-12">
			{*<p>Información adicional: este producto está sujeto a disponibilidad de stock etc etc....</p>*}
			<p id="add_to_cart" class="">
				<button  style="margin-left: 0px;"  onclick="validacionPedido(16,false);" name="Submit" >
					<span>Kup</span>
				</button>
			</p>
		</div>
		</div>
		</div>



	<div style="clear:both;"></div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo">
</div>
</div>

<!-- Descripción y modo de empleo -->
<div id="sectiondos" class="container">
		<span class="titulo" style="border: 1px solid black;padding: 10px; font-style:italic;">Może Cię zainteresować</span>
		<div class="wrapper">
			<div id="producto2">
					<div style=""  class="producto-img"><img src="{$module_dir}images/faja_slimcore.png"/></div>
					<div style=""  class="producto-desc">
							<p style="font-size:20px; border-bottom:1px solid black">Slim Core Belt <span style="font-style:italic"><b>Bonplus</b></span></p>
							<div style="font-size: 24px; font-weight:bold;" class="productPrice2"><span>81</span>,90zł</div>
							<p class="descripcionProducto">Bardziej widoczne rezultaty w krótszym czasie. Składa się z 3 warstw: 1- Warstwa wewnętrzna wspiera pocenie, dzięki któremu eliminowane są toksyny i ograniczana objętość. 2 - Warstwa środkowa składa się z milionów mikrokapsułek, które pochłaniają pot. 3 - Warstwa zewnętrzna zapobiega przedostawaniu się wilgoci na ubranie, zapobiegając jego zaplamieniu.</p>

								<div style="" class="combinaciones">
									<ul>
										<li><i class="fa fa-circle-thin combinacion2 combinacionActivada" data-pr="3" aria-hidden="true"></i>S</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="4" aria-hidden="true"></i>M</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="5" aria-hidden="true"></i>L</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="6" aria-hidden="true"></i>XL</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="7"aria-hidden="true"></i>XXL</li>
									</ul>
							</div>
							<div  style="" class="rightBuy col-lg-12">
								<p id="add_to_cart" class="">
									<button style="margin-left:0px;" onclick="validacionPedido(10,$('.combinacion2.combinacionActivada').attr('data-pr'));" name="Submit" >
										<span>Kup</span>
									</button>
								</p>
							</div>
					</div>

			</div>

			<div id="producto3">
					<div style="" class="producto-img"><img src="{$module_dir}images/faja_xtreme.png"/></div>
					<div style=""  class="producto-desc">
							<p style="font-size:20px; border-bottom:1px solid black">Xtreme <span style="font-style:italic"><b>Bonplus</b></span> Belt</p>
							<div style="font-size: 24px; font-weight:bold;"class="productPrice2"><span>172</span>,90zł</div>
							<p class="descripcionProducto">Natychmiastowo modeluje sylwetkę. Idealny do noszenia przez cały dzień w jakiejkolwiek sytuacji. Dodatkowo koryguje postawę, zapobiegając kontuzjom podczas ćwiczeń oraz bólowi odcinka lędźwiowego. Efekt sauny oraz wrażenie kilku rozmiarów mniej. Wzmacnia pocenie oraz działanie kremów redukujących.</p>
						<div style="" class="combinaciones">
									<ul>
										<li><i class="fa fa-circle-thin combinacion1 combinacionActivada" data-pr="1" aria-hidden="true"></i>S-M</li>
										<li><i class="fa fa-circle-thin combinacion1" data-pr="2" aria-hidden="true"></i>L-XL</li>
									</ul>
							</div>
							<div  style="" class="rightBuy col-lg-12">
								<p id="add_to_cart" class="">
									<button style="margin-left:0px;" onclick="validacionPedido(9,$('.combinacion1.combinacionActivada').attr('data-pr'));" name="Submit" >
										<span>Kup</span>
									</button>
								</p>
							</div>
					</div>
			</div>
		</div>

		<div style="clear:both"></div>

</div>
{*<p style="text-align: center;
    padding-top: 18px;
    padding-bottom: 0px;
    font-size: 14px;">*Gastos de env&iacute;o gratis para pedidos superiores a 25€</p>*}
<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>

<!-- Descripción y modo de empleo -->
<div id="sectiontres" class="container">

		<span style="line-height: 25px;" class="titulo">Trenuj swoje mięśnie i tonizuj je z niskim i wysokim obciążeniem!</span>
		<div style="" class="wrapper">
			<p style="font-size:18px; color:#ff5000; font-weight:bold; font-style:italic; text-align:center">Trenuj swoje mięśnie i tonizuj je z niskim i wysokim obciążeniem!</p>
			<p style="display:inline-block; width:100%; text-align:left;">
				<strong>Dzięki Bonplus</strong>, marce znanej z jakościowych produktów, w naszym zasięgu znajduje się najbardziej kompletny na rynku aparat do ćwiczeń, zajmujący minimalną przestrzeń.
				<strong>Fit Crunch</strong> został zaprojektowany tak, by tonizować całe ciało oraz uprawiać gimnastykę sercowo-naczyniową, wykonując tylko jeden ruch.

			</p>
			<p style="display:inline-block; width:100%; text-align:left;">
				<strong>Ćwicz i tonizuj każdą część ciała: </strong> ramiona, klatkę piersiową, plecy, brzuch, uda, łydki... Zmieniaj ustawienie rąk i nóg, aby zwiększyć intensywność ćwiczenia.
			</p>
			<p style="display:inline-block; width:100%; text-align:left; padding-bottom:10px;">
			Posiada siodełko, regulowane zarówno co do wysokości, jak i odległości. Natomiast licznik cyfrowy kontroluje liczbę powtórzeń, czas oraz spalone kalorie.
			</p>
			<p id="especificacionesImagenes">
				<img class="izquierda" style="" src="{$module_dir}/images/descripcion_1.png" />
				<img class="derecha" style="" src="{$module_dir}/images/descripcion_2.png" />
				<img class="izquierda" style="" src="{$module_dir}/images/descripcion_3.png" />
				<img class="derecha" style="" src="{$module_dir}/images/descripcion_pl.png" />
			</p>
			<div style="clear:both"></div>

				<div style="" class="especificaciones">
					<div style="    ">
						<ul>
							<li><strong>Zawiera:</strong>
								<li> &nbsp;</li>
								<li>- Ławkę do ćwiczeń Fit Crunch</li>
								<li>- Licznik cyfrowy </li>
								<li>- Plan diety <br>&nbsp;(języki: angielski, francuski, hiszpański, włoski i niemiecki)</li>
								<li>- Instrukcje obsługi i montażu wraz z przewodnikiem treningowym  <br>&nbsp;(języki: angielski, francuski, hiszpański, włoski i niemiecki)</li>

							</ul>

					</div>
					<!--<div style=" ">
						<ul>

						</ul>
					</div>-->

				</div>
				<div style="" class="especificaciones">

					<div style="  ">
							<ul>
								<li><strong>Dane techniczne:</strong>
								<li>  &nbsp;</li>
								<li>- Intensywność o niskim i wysokim obciążeniu</li>
								<li>- Siodełko regulowane do różnych wysokości i odległości</li>
								<li>- Wytrzymałość do maks. 100 kg</li>
								<li>- Składany, bardzo łatwy w przechowywaniu</li>
								<li>- Wymiary: * Po rozłożeniu: 124cm x 88cm x 41cm * Po złożeniu: 154cm x 26 cm</li>
								<li>- Materiał: stal</li>
							</ul>
					</div>
					<!--<div style="">
						<ul>

						</ul>
					</div>-->
				</div>

		<div style="clear:both"></div>
		<div class="rightBuy col-lg-12">
			{*<p>Información adicional: este producto está sujeto a disponibilidad de stock etc etc....</p>*}
			<p id="add_to_cart" class="">
				<button  onclick="validacionPedido(16,false);" name="Submit" >
					<span>Kup</span>
				</button>
			</p>
		</div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>

<!-- Videos -->
<div id="sectioncuatro" class="container">

		<span style=" " class="titulo">Nagrania wideo</span>
		<div class="wrapper">
		{*<div class="slider">*}

			<div class="video"><span class="enlace" style="display:none">https://www.youtube.com/embed/ai6em4hxXCY</span><img src="{$module_dir}/images/video2.png" /></div>


		{*</div>*}
		</div>
		<div class="rightBuy col-lg-12">
			<p onclick="validacionPedido(16,false);" id="add_to_cart" class="">
				<button name="Submit" >
					<span>Kup</span>
				</button>
			</p>
		</div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>


<!-- Valoraciones -->

<div id="sectioncinco" class="container">

		<span class="titulo" style=" border: 1px solid black; padding: 10px;">Komentarze</span>
	<div class="wrapper valoraciones">
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Aida 28/01/2016 </span>
			<div style="float:right; color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div>
			<p>Nie była do końca przekonana, ale zachęciła mnie do niego siostrzenica - i miała rację! Na drugi dzień miałam zakwasy nawet na uszach ;).</p>
		</div>

		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Cris 29/01/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div>
			<p> Trwały materiał wykonania sprawia, że można na nim bezpiecznie ćwiczyć. </p>
		</div>
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Alba. 16/01/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div>
			<p>Kupiłam urządzenie w poniedziałek i otrzymałam je już po 3 dniach; było bardzo łatwe w montażu. Jestem bardzo zadowolona z zakupu; używam go codziennie, aby ćwiczyć różne części ciała. Już po miesiącu widać rezultaty. Polecam w 100%.</p>
		</div>
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Carmen 14/02/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div>
			<p>Kupiłam go dla mojego ojca, żeby zmotywować go do ćwiczeń i rzeczywiście używa go codziennie. Dostawa była szybka, a produkt bardzo dobry.</p>
		</div>

	</div>
		<div style="clear:both"></div>

</div>

<div style="margin-top: 10px;" id="volverArriba">
	<p>Wróć do góry</p>
</div>

<div id="cortina"></div>
<div style="" id="popupVideo">

</div>
{* END-PAGINACION *}
	<form id="buy_block" class="hidden" action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
		<!-- hidden datas -->
		<p class="hidden">
			<input type="hidden" name="token" value="{$static_token}" />
			<input type="hidden" class="id_product" id="product_page_product_id" name="id_product" value="1" />
			<input type="hidden" name="add" value="1" />
			<input type="hidden" name="quantity_wanted" id="quantity_wanted" value="1" />
			<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
		</p>
	</form>

{literal}
<script type="text/javascript">
	function validacionPedido(id_del_producto,id_product_attribute){

		document.getElementById("product_page_product_id").value=id_del_producto;
		if (id_product_attribute!=false)
			document.getElementById("idCombination").value=id_product_attribute;
		else
			document.getElementById("idCombination").value="";
	}

	$(document).ready(function() {
		  var offset = -20; //Offset of 20px
		  $("#popupVideo").hide();

		 $(".combinacion1").click(function(){
			$(".combinacion1").removeClass("combinacionActivada");
			$(this).addClass("combinacionActivada");
		 });

		 $(".combinacion2").click(function(){
			$(".combinacion2").removeClass("combinacionActivada");
			$(this).addClass("combinacionActivada");
		 });
		  $("#masinfoTarjeta").click(function(){
			$("#popupTarjetas").show();
		  });

		  $("#popupTarjetas_close").click(function(){
			$("#popupTarjetas").hide();
		  });

		  $(".miniaturavideo").click(function(){
				//Obtenemos el ancho del navegador.
			var width = $(window).width();

			//En funcion del ancho del navegador, calculamos la proporcion del video.
			if (width > 850) {
			  var anchovideo = "800px";
			  var altovideo = "600px";
			} else if (width > 350 && width < 850) {
			  var anchovideo = "350px";
			  var altovideo = "323px";
			} else {
			  var anchovideo = "300px";
			  var altovideo = "280px";
			}

			//Obtenemos la direcci&oacute;n
			var enlace = $(".video").find(".enlace").text();
			//Maquetamos el iframe
			var embed = "<iframe width='" + anchovideo + "' height='" + altovideo + "' src='" + enlace + "' frameborder='1' allowfullscreen></iframe><span class='close-popup'>X</span>";
			$("#cortina").show();
			$("#popupVideo").html(embed);
			$("#popupVideo").show();

			//Hacemos scroll hasta el video.
			$('html, body').animate({
			  scrollTop: $("#popupVideo").offset().top - 40
			}, 1000);

		  });

		  $(".video").click(function() {
			//Obtenemos el ancho del navegador.
			var width = $(window).width();

			//En funcion del ancho del navegador, calculamos la proporcion del video.
			if (width > 850) {
			  var anchovideo = "800px";
			  var altovideo = "600px";
			} else if (width > 350 && width < 850) {
			  var anchovideo = "350px";
			  var altovideo = "323px";
			} else {
			  var anchovideo = "300px";
			  var altovideo = "280px";
			}

			//Obtenemos la direcci&oacute;n
			var enlace = $(this).find(".enlace").text();
			//Maquetamos el iframe
			var embed = "<iframe width='" + anchovideo + "' height='" + altovideo + "' src='" + enlace + "' frameborder='1' allowfullscreen></iframe><span class='close-popup'>X</span>";
			$("#cortina").show();
			$("#popupVideo").html(embed);
			$("#popupVideo").show();

			//Hacemos scroll hasta el video.
			$('html, body').animate({
			  scrollTop: $("#popupVideo").offset().top - 40
			}, 1000);

		  });

		  //Al hacer click en la cortina eliminamos el contenido del iframe y lo ocultamos.
		  $("#cortina").click(function() {

			$('body,html').animate({
			  scrollTop: 0
			}, 500);

			$("#popupVideo").html("");
			$("#popupVideo").hide();
			$(this).hide();

		  });
		    $(document).on("click",".close-popup",function(){
				$('body,html').animate({
					scrollTop: 0
				}, 500);

				$("#popupVideo").html("");
				$("#popupVideo").hide();
				$("#cortina").hide();


		  });

		  /*Eventos para la navegaci&oacute;n*/
		  $('#volverArriba').click(function() {

			$('body,html').animate({
			  scrollTop: 0
			}, 1000);
			return false;
		  });

		  $(".sectiondos").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectiondos").offset().top + offset
			}, 1000);
		  });

		  $(".sectiontres").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectiontres").offset().top + offset
			}, 1000);
		  });

		  $(".sectioncuatro").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectioncuatro").offset().top + offset
			}, 1000);
		  });

		  $(".sectioncinco").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectioncinco").offset().top + offset
			}, 1000);
		  });


		  /*Men&uacute; Izquierda del producto (imagenes en miniatura)*/
		  $(".miniaturaImg img").click(function() {

			var img = "<img src='" + $(this).attr("src") + "' />";

			$(".leftImages").html(img);

		  });

		  $(".miniaturaImg img").hover(function() {
			var img = "<img src='" + $(this).attr("src") + "' />";

			$(".leftImages").html(img);

		  });


		  /*Sliders*/
		  /*Videos*/
		  $('.slider').slick({
			slidesToShow: 3,
			slidesToScroll: 3,
			dots: true,
			adaptiveHeight: true,
			autoplay: true,
			autoplaySpeed: 6000,

			responsive: [{
				breakpoint: 1000,
				settings: {
				  slidesToShow: 2,
				  slidesToScroll: 2,
				  adaptiveHeight: true,
				  dots: true,
				  centerMode: false,

				  autoplay: true,
				  autoplaySpeed: 6000
				}
			  }, {
				breakpoint: 800,
				settings: {
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  autoplay: true,
				  autoplaySpeed: 6000
				}
			  }


			]
		  });

		  /*Valoraciones*/
		  $('.valoraciones').slick({
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			slidesToShow: 2,
			cssEase: 'linear',
			responsive: [{
			  breakpoint: 1000,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 6000
			  }
			}]
		  });

	});
document.addEventListener("DOMContentLoaded", function(event) {
	//do work
	$(".miniaturaImg").each(function(index){
		$(this).hover(function(){
			//alert(index);
		});
	});
});

</script>
{/literal}
