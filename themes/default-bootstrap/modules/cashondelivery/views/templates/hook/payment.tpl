{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
* 
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<p class="payment_module" style="margin-bottom:0px;cursor:not-allowed;">
    <a class="cash" href="{$link->getModuleLink('cashondelivery', 'validation', [], true)|escape:'html'}" title="{l s='Pay with cash on delivery (COD)' mod='cashondelivery'}" rel="nofollow">

    {*<a class="cash" href="" title="{l s='Pay with cash on delivery (COD)' mod='cashondelivery'}" rel="nofollow" style="pointer-events:none;">*}
        <i class="far fa-money-bill-alt"></i>
        CONTRAREEMBOLSO (+2.95€ adicional)
        {*<br>
        <span style="color: red; font-size: 12px;display: inline-block;padding-left: 20px;">(Opción no disponible por emergencia sanitaria)</span>*}
    </a>
</p>
