
<style>
    div.imagenCabecera{
        width: 100%;
        min-height: 73px;
        background-image: url('{$modules_dir}eyeLanding/images/foto_cabecera.png');
        background-position-x: -139px;
        background-size: 100%;
        background-position-y: center;
        background-repeat: no-repeat;
        border-bottom: 10px solid #ff5000;
    }

    @media (max-width:1420px)
    {
        div.imagenCabecera{
            background-position-x: center;
        }
    }
    @media (max-width: 800px){
        div.imagenCabecera{

            background-size: 125%;
        }
    }

    @media (max-width:659px)
    {
        div.imagenCabecera{

            background-size: 125%;
            background-size: 162%;
            background-position-x: right;
        }


    }

    @media (max-width:360px)
    {
        #logo img{
            width:90% !important;
        }
        #logo { text-align: center; }
    }
    @media (max-width:491px)
    {
        div.imagenCabecera{
            min-height:0px;
            background-image:none;

        }
        #logo{
            position:initial !important;
            margin:0px auto;
        }
    }
    @media (max-width:1050px) {
        #menu,#menu_m_wrapper{
            /* min-height:150px;*/
        }
        #menu,#menu_m span {
            padding: 10px 8px;
            margin-left: 0px;
        }
    }
    @media (max-width: 530px){

        #menu,#menu_m{
            text-align:center !important;
        }
    }
    @media (max-width:500px) {
        #menu,#menu_m span{ padding:0px;}

        #menu,#menu_m span {
            {*            padding: 10px 10px;*}
            margin-left: 0px;
            {*            font-size: 14px;*}
        }	
    }

    #menu,#menu_m span {
        {*        padding: 10px 10px;*}
        margin-left: 0px;
        {*        font-size: 16px;*}

    }
    #menu,#menu_m,#menu_m {
        background-color: #ef7b16;
        color: white;
        text-align: right;
        {*        padding:20px;*}
    }

    #index #logo{ top: 42px !important;}
    #logo{
        position: absolute;
        top: 44px;
        width: 350px;
        background: white;
        height: 63px;
    }

    #logo img { width: 100%;     margin-top: 5px; }

    #cms#menu,#menu_m{
        {*        padding: 2px;*}
    }
    .enlace{
        color:white;
        font-style:normal;
    }

    #menu,#menu_m span a:hover{
        {*        color: #ff5000;*}
    }
    #menu,#menu_m span a:after{
        {*        content: "";*}
    }
    #menu,#menu_mContactanos{
        display:inline-block;
    }

    #menu,#menu_mContactanosTLF{

        display:inline-block;

    }

    #menu{
        display: block !important;
    }

    #menu_m{
        display: none !important;
    }
    @media (max-width:768px)
    {
        #menu{
            display: none !important;
        }

        #menu_m{
            display: block !important;
        }
    }


</style><!-- MENU -->
<div id="menu_wrapper">

    <div id="menu" class="row" style="margin: 0px;">
        <div style=" display: flex;
             align-items: center;padding: 10px 0px">
            <div class="col-lg-3 col-md-3" style="  
                 float: left;padding:  0px; text-align: center">
                <a href="{$base_dir}"><img  style="max-width: 240px; width: 80%" src="{$modules_dir}eyeLanding/images/vibratrainer/vibratrainer-logo.png"></a>
            </div>
            <div class="col-lg-3 col-md-3" style="   
                 float: left;padding:  0px; text-align: center">
                <img  style="width: 80%; max-width:211px"src="{$modules_dir}eyeLanding/images/vibratrainer/imagen-cabecera.png">
            </div>
            <div class="col-lg-3 col-md-3 row" style="  
                 float: left;padding:  0px;">
                <div class="col-lg-3 col-md-1"></div>
                <div  class="col-lg-6  col-md-10"style="    text-align: center;
                      background: #56c3ea;
                      display: block;
                      {*                  width: 100%;*}
                      height: 59px;
                      padding: 0px;
                      border: 1px solid transparent;
                      border-radius: 0.7em;
                      {*                  margin-top: 7px;*}
                      font-family: 'Oswald', sans-serif;">
                    <div style="width: 100%;float: left;display: block;font-size: 15px;">¡Llámanos GRATIS!</div>
                    <div style="width: 100%;float: left;display: block;font-size: 25px;margin-top: 5px"> <i class="fa fa-phone" aria-hidden="true"></i> 951204539</div>
                </div>
                <div class="col-lg-3 col-md-1"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 row" style="  
                 display:inline-block; padding: 0px;">
                 <div class="" style="  
                 display:inline-block; padding: 0px;">
                        <a class="enlace" href="{$base_dir}pedido-rapido">
                            <div style="width: 100%;float: left;text-align: center;">
                                <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> 
                            </div>
                            <div style="width: 100%;float: left;text-align: center; font-family: 'Oswald', sans-serif;">
                                CESTA
                            </div>
                        </a>
                    </div>
                    <div class="" style="  
                 display:inline-block; padding: 0px;">

                        <a class="enlace" href="{$base_dir}contactanos">

                            <div style="width: 100%;float: left;text-align: center;">
                                <i class="fa fa-envelope  fa-2x" aria-hidden="true"></i> 
                            </div>
                            <div style="width: 100%;float: left;text-align: center;font-family: 'Oswald', sans-serif;">
                                CONTACTO
                            </div>
                        </a>
                    </div>
                
            </div>

        </div>
    </div>
    <div id="menu_m" class="row" style="display: block;margin: 0px;">
        <div style="display: flex;
             align-items: center;padding: 10px 0px">
            <div class="col-md-6" style="  
                 float: left;padding:  0px;width: 65%; text-align: left">
                  <a href="{$base_dir}"><img  style="width: 50%" src="{$modules_dir}eyeLanding/images/vibratrainer/vibratrainer-logo.png"></a>

            </div>
            <div class="col-md-6 row" style="  float: left;padding: 0px;width: 35%">
                <div class="col-md-6" style="width: 50%; float: left;padding-right: 0px;">
                    <a class="enlace" href="{$base_dir}pedido-rapido">
                        <div style="width: 100%;float: left;text-align: center;">
                            <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i> 
                        </div>
                    </a>
                </div>
                <div class="col-md-6" style="width: 50%; float: left; padding-right: 0px">
                    <a  class="enlace" href="{$base_dir}contactanos">
                        <div style="width: 100%;float: left;text-align: center;">
                            <i class="fa fa-envelope  fa-2x" aria-hidden="true"></i> 
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12 row" style="  width: 100%;
             float: left;
             background: #56c3ea;
             display: block;
             margin-left: 0px;
             border: 1px solid transparent;
             font-family: 'Oswald', sans-serif;
             text-align: center;margin-top:3px">
            ¡Llámanos GRATIS! <i class="fa fa-phone" aria-hidden="true"></i> 951204539

        </div>

    </div>        
</div>