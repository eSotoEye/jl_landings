
<link rel="stylesheet" href="{$module_dir}/slick_up/slick-theme.css">
<link rel="stylesheet" href="{$module_dir}/slick_up/slick.css">
<link rel="stylesheet" href="{$module_dir}style_m.css">

<script type="text/javascript" src="{$module_dir}/slick_up/slick.min.js"></script>

<div class="mainSection_m">
    <div class="col-xs-12">
        <div class="productName row" style="border-bottom: 1px solid;padding: 5px 0px;">
            <div class="col-xs-6 texto_Oswald" style="font-size: 24px;text-transform: uppercase;font-weight: bold;padding-left: 0px;">
                Vibratrainer
            </div>
            <div  class="col-xs-6" style="padding-right: 0px;">
                <div class="puntuacion" style="float: right;">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div> 
            </div>
        </div> 
        <div class="productSubName texto_Oswald"  style="font-size: 12px;font-weight: 100;text-transform: uppercase;padding-left: 0px;margin-left: -15px;">Entrena todo el cuerpo con un solo aparato</div>
        <div class="carrousel" style="margin-left: -15px;margin-right: -15px;">
            {*            En caso de que sea un video descargado y no de youtube*}
            <video style="width: 100%;    height: 320px;"  controls autoplay muted loop>
                <source src="/modules/eyeLanding/images/vibratrainer/video.mp4" type="video/mp4">

                Your browser does not support the video tag.
            </video>

            <img src="/modules/eyeLanding/images/vibratrainer/vibratrainer-1_.png" style="width:100%;      height: 320px;" />
            <img src="/modules/eyeLanding/images/vibratrainer/vibratrainer-2_.png" style="width:100%;     height: 320px;" />
            <img src="/modules/eyeLanding/images/vibratrainer/vibratrainer-4_.png" style="width:100%;      height: 320px;" />

        </div>
        {*            <iframe style="width: 100%;" height="290" src="https://www.youtube.com/embed/_NUlPRP4yOg&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>*}


        <div class="wrapper row" style="margin: 5px -15px">
            <div class="productPrice_ant col-xs-3 text-left texto_Oswald crossed" ><span>279</span>,00€</div>
            <div class="productPrice  col-xs-5 text-center texto_Oswald" style="padding: 10px 0px;"><span>149</span>,00€</div>
            <div class="col-xs-4 text-center texto_Oswald envio_gratis" >ENV&Iacute;O GRATIS 24/48h</div>
        </div>
        <div class="parpadea text texto_Oswald text-center ">
            Oferta Disponible hasta {$tomorrow}
        </div>
        <div id="primerBoton"class="rightBuy col-lg-12 texto_Oswald" style="margin: 0px -15px;">
            {*<p>Información adicional: este producto está sujeto a disponibilidad de stock etc etc....</p>*}

            <button id="add_to_cart"  style="margin: 0px -15px;width: 100%"  onclick="validacionPedido(4, false);" name="Submit" >
                Comprar
            </button>

        </div>
        {*<div id="amazonpaybp20" class="amazonLoginWr"></div>*}
    </div>

    <div  class="col-xs-12 row  texto_Oswald" style="
          margin: 20px 0px;
          ">
        <div  class="col-xs-12 row" style="margin: 15px 0px;padding: 0px;font-size: 18px;"> 
            <div class="col-xs-2" style="padding: 0px 0px;">
                <img style="width: 50%;" src="/modules/eyeLanding/images/tick.jpg"/>
            </div>
            <div class="col-xs-10" style="text-align: justify;padding: 0px 0px; font-weight: 100;">Entrena el 100% de tus músculos protegiendo huesos, articulaciones y músculos.</div> 
        </div>
        <div  class="col-xs-12 row" style="margin: 15px 0px;padding: 0px;font-size: 18px;"> 
            <div class="col-xs-2" style="padding: 0px 0px;">
                <img style="width: 50%;" src="/modules/eyeLanding/images/tick.jpg"/>
            </div>
            <div class="col-xs-10"  style="text-align: justify;padding: 0px 0px; font-weight: 100;">Entrena en tu lugar preferido: viendo series de TV, el móvil, mientras cuidas a tus hijos, etc.</div>
        </div>
        <div  class="col-xs-12 row" style="margin: 15px 0px;padding: 0px;font-size: 18px;">
            <div class="col-xs-2" style="padding: 0px 0px;">
                <img style="width: 50%;" src="/modules/eyeLanding/images/tick.jpg"/>
            </div>
            <div class="col-xs-10"  style="text-align: justify;padding: 0px 0px; font-weight: 100;">Apto para todas las edades y condiciones físicas: 9 programas y 99 niveles de intensidad.</div>
        </div>
{*        <div  class="col-xs-12 row" style="margin: 15px 0px;padding: 0px;font-size: 15px; "> *}
            {* <div class="col-xs-2" style="padding: 0.8em 0px;">
            <img style="width: 50%;" src="modules/eyeLanding/images/tick.jpg"/>
            </div>*}
            {*            <div class="col-xs-5" style="text-align: left;padding: 0.8em 0px;">Pago SEGURO</div>*}
            {*<div class="col-xs-12 campo_tarjeta row" style="margin: 0px;">
                <div class="col-xs-4" style="padding: 5px;
                     font-size: 20px;
                     color: #b9b9b9;">  Tarjeta</div>

                <div class="col-xs-8" style="padding: 0px;text-align: center;">
                    <img style="width: 100%;" src="/modules/eyeLanding/images/modalidades-de-pago.png"/>
                </div>


            </div>*}
{*        </div>*}
    </div>
    <div class="lineahorizontal">
        <div class="lineahorizontalTriangulo">
        </div>
    </div>
    <style> .texto_Corbel { font-size: 16px; } </style>
    <!-- Descipción del producto-->
    <div class="description row texto_Oswald" style="font-size: 16px;
         text-align: justify;
         margin: 0px 0px;
         display: block;
         float: left;">
        <div class="col-xs-12"style="text-align: center;
             font-size: 20px;
             margin: 15px 0px;font-weight: 300; padding: 0px">DESCRIPCIÓN</div>
        <div class="col-xs-12 texto_Corbel" style="margin: 15px 0px; text-align: justify; padding: 0px">
            Vibratrainer es un moderno método de entrenamiento que utiliza la vibración oscilante 
            multidimensional para entrenar el 100% de los músculos del cuerpo. 
            Este método está aconsejado por los de entrenadores deportivos y se encuentra presente en múltiples 
            centros deportivos. Ahora tienes la posibilidad de tenerla en tu casa con esta oferta especial.<br><br>
            Solo tendrás que colocar tus pies en el centro y tu cuerpo se activará como si estuvieras caminando, sepáralos hasta la posición de trotar indicada en el tapiz y aumentarás el ritmo circulatorio y actividad muscular como si estuvieras trotando, y si deseas un entrenamiento más intenso separa los pies hasta los extremos en la posición de correr y todo tu cuerpo se activará como si estuvieras corriendo.

        </div>
        <div class="col-xs-12 row"style="margin: 15px 0px; padding: 0px"> 

            <div class="col-xs-12 row" style="text-align: center; margin: 15px 0px; padding: 0px">
                <div class="col-xs-6">
                    <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-1-correr.jpg"/>
                </div>
                <div class="col-xs-6" >
                    <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-1-trotar.jpg"/>
                </div>
                <div class="col-xs-6" style="  font-weight: 300;  font-size: 18px;margin-top: 10px;">
                    CORRER
                </div>
                <div class="col-xs-6"style="   font-weight: 300; font-size: 18px;margin-top: 10px; ">
                    TROTAR
                </div>
            </div>
            <div lass="col-xs-12 row" style="text-align: center; margin: 15px 0px; padding: 0px">
                <div class="col-xs-12">
                    <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-1-caminar.jpg"/>
                </div>
                <div class="col-xs-12" style="text-align: center; font-weight: 300;   font-size: 18px;margin-top: 10px;">
                    CAMINAR
                </div>
            </div>
        </div>
        <div class="col-xs-12 row" style="margin: 15px 0px; padding: 0px">
            <div class="col-xs-8 texto_Corbel" style=" text-align: justify; padding: 0px"> 
                Vibratrainer también incluye 2 bandas tensoras para intensificar el trabajo de la parte superior del cuerpo. A la vez que caminas, trotas o corres podrás crear tensión con las bandas intensificando el trabajo de músculos de brazos, espalda y abdominales. 
            </div>
            <div class="col-xs-4">
                <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-2.jpg"/>
            </div>
        </div>
        <div class="col-xs-12 row"style="margin: 15px 0px; padding: 0px">
            <div class="col-xs-4">
                <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-3.jpg"/>
            </div>
            <div class="col-xs-8 texto_Corbel" style=" text-align: justify; padding: 0px"> 
                Sus 20 imanes estratégicamente situados te aportarán múltiples ventajas como la activación de la circulación y la mejora de los niveles generales de energía del cuerpo 
            </div>
        </div>
        <div class="col-xs-12 texto_Corbel" style="margin: 15px 0px; padding: 0px">
            Incluye un display con contador de tiempo y velocidad seleccionada. Podrás dejarlo programado desde el display antes del inicio o utilizar su mando a distancia para ir cambiando intensidad y programas mientras lo practicas. 
        </div>

        <div class="col-xs-12" style=" padding: 0px;margin: 15px 0px; ">
            <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-4.jpg"/>
        </div>
        <div class="col-xs-12 texto_Corbel" style=" text-align: justify; padding: 0px">
            Vibratrainer te permitirá hacer una amplia variedad de ejercicios con el objetivo de intensificar el entrenamiento en las diferentes zonas del cuerpo: sentadillas, plancha para entrenamiento abdominal o simplemente ponerlo en los pies mientras estás trabajando para activar la circulación
        </div>
        <div class="col-xs-12 row" style="margin: 15px 0px; padding: 0px">
            <div lass="col-xs-12 row" style="text-align: center; margin: 15px 0px;">
                <div class="col-xs-12">
                    <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-5-ejercicio1.jpg"/>
                </div>
                <div class="col-xs-12" style="text-align: center; font-weight: 300;   font-size: 18px;margin-top: 10px;">
                    SENTADILLAS
                </div>
            </div>
            <div lass="col-xs-12 row" style="text-align: center; margin: 15px 0px;margin-top: 10px;">
                <div class="col-xs-12">
                    <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-5-ejercicio2.jpg"/>
                </div>
                <div class="col-xs-12" style="text-align: center; font-weight: 300;   font-size: 18px;margin-top: 10px;">
                    ABDOMINALES
                </div>
            </div>
            <div lass="col-xs-12 row" style="text-align: center; margin: 15px 0px;">
                <div class="col-xs-12">
                    <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-5-ejercicio3.jpg"/>
                </div>
                <div class="col-xs-12" style="text-align: center; font-weight: 300;   font-size: 18px;margin-top: 10px;">
                    ACTIVA TU CIRCULACIÓN
                </div>
            </div>
        </div>
    </div>

    <div class="lineahorizontal" style=" display: block;
         float: left; width: 100%">
        <div class="lineahorizontalTriangulo">
        </div>
    </div>


    <!-- Descripción y modo de empleo -->
    <div id="seccionincluye" class="row texto_Oswald" style="font-size: 12px;
         text-align: justify;margin: 0px 0px;display: block;
         float: left;">
        <div class="col-xs-12"style="text-align: center;
             font-size: 20px;
             margin: 15px 0px;">INCLUYE
        </div>
        <div lass="col-xs-12 row" style="text-align: center; margin: 15px 0px;"> 
            <div class="col-xs-12">
                <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-6-incluye-2.jpg"/>
            </div>
            <div class="col-xs-12" style="text-align: center; font-weight: 300;   font-size: 18px;margin-top: 10px;">
                PLATAFORMA
            </div>
        </div>
        <div class="col-xs-12 row" style="text-align: center; margin: 15px 0px;">
            <div class="col-xs-6">
                <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-6-incluye-1.jpg"/>
            </div>
            <div class="col-xs-6">
                <img style="width: 100%;" src="/modules/eyeLanding/images/vibratrainer/vibratrainer-imagen-6-incluye-3.jpg"/>
            </div>
            <div class="col-xs-6" style="  font-weight: 300;  font-size: 18px;margin-top: 10px;">
                MANDO
            </div>
            <div class="col-xs-6"style="  font-weight: 300;  font-size: 18px;margin-top: 10px;">
                TENSORES
            </div>
        </div>
        <div id="primerBoton"class="rightBuy col-lg-12 texto_Oswald" style="margin: 0px -15px;padding: 0px 15px;">
            {*<p>Información adicional: este producto está sujeto a disponibilidad de stock etc etc....</p>*}

            <button id="add_to_cart"  style="margin: 0px -15px;width: 100%;padding: 15px 60px;"  
                    onclick="validacionPedido(4, false);" name="Submit" >
                Comprar
            </button>

        </div>
    </div>

    <div class="lineahorizontal" style=" display: block;
         float: left;
         width: 100%;">
        <div class="lineahorizontalTriangulo"></div>
    </div>


    <!-- Valoraciones -->

    <div id="seccionopiniones" class="container" style=" display: block;
         float: left;
         width: 100%;">

        <span class="titulo texto_Oswald" style="margin-bottom: 15px;font-size: 18px; font-weight: 300;">OPINIONES DE NUESTROS CLIENTES</span>
        <div class="wrapper valoraciones" style=" border: 1px solid #888886;">
            <div class="valoracion">
                <span class="autor"> Aida 28/01/2016 </span>
                <div  class="puntuacion">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div> 
                <p class=" texto_Corbel">Estéticamente no me convencía mucho, pero me animó mi sobrina y la verdad es que la niña tenía razón. El segundo día estaba con agujetas hasta en las orejas.</p>
            </div>

            <div class="valoracion ">
                <span class="autor"> Cris 29/01/2016 </span>
                <div  class="puntuacion">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div> 
                <p class=" texto_Corbel"> El material en el que está hecho, aporta seguridad cuando se está trabajando sobre él. </p>
            </div>
            <div class="valoracion">
                <span class="autor"> Alba. 16/01/2016 </span>
                <div  class="puntuacion">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div> 
                <p class=" texto_Corbel">Compré el aparato un lunes y me llegó en sólo 3 días, fue muy fácil de montar. Estoy muy contento con mi adquisición, lo uso todos los días para entrenar diferentes partes del cuerpo. Después de un mes se notan los resultados. Recomendable 100%.</p>
            </div>	
            <div class="valoracion ">
                <span class="autor"> Carmen 14/02/2016 </span>
                <div class="puntuacion">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div> 
                <p class=" texto_Corbel">Se lo he comprado a mi padre porque le cuesta mucho hacer ejercicio y desde que lo recibimos lo usa todos los días. La entrega fue rápida y el producto muy bien.</p>
            </div>		

        </div>


    </div>

    <div style="margin-top: 25px;
         display: block;
         float: left;
         width: 100%;" id="volverArriba" class="texto_Oswald">
        <p style="background: #9ec866;
           color: white;
           padding: 15px;
           font-size: 21px;">Volver arriba</p>
    </div>

    {*  <div id="cortina"></div>
    <div style="" id="popupVideo">*}


    {* END-PAGINACION *}
    <form id="buy_block" class="hidden" action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
        <!-- hidden datas -->
        <p class="hidden">
            <input type="hidden" name="token" value="{$static_token}" />
            <input type="hidden" class="id_product" id="product_page_product_id" name="id_product" value="1" />
            <input type="hidden" name="add" value="1" />
            <input type="hidden" name="quantity_wanted" id="quantity_wanted" value="1" />
            <input type="hidden" name="id_product_attribute" id="idCombination" value="" />
        </p>
    </form>
</div>
{literal}
    <script type="text/javascript">
 function validacionPedido(id_del_producto, id_product_attribute) {

            document.getElementById("product_page_product_id").value = id_del_producto;
            if (id_product_attribute != false)
                document.getElementById("idCombination").value = id_product_attribute;
            else
                document.getElementById("idCombination").value = "";

                $("#buy_block").trigger("submit");
				{literal}
				fbq('track', 'AddToCart', {
				  content_ids: "ES1022",
				  content_type: 'product',
				  value: "149",
				  currency: 'EUR'
				});
				{/literal}
        }
        $(document).ready(function () {
        /*    var offset = -20; //Offset of 20px
            $("#popupVideo").hide();

            $(".combinacion1").click(function () {
                $(".combinacion1").removeClass("combinacionActivada");
                $(this).addClass("combinacionActivada");
            });

            $(".combinacion2").click(function () {
                $(".combinacion2").removeClass("combinacionActivada");
                $(this).addClass("combinacionActivada");
            });
            $("#masinfoTarjeta").click(function () {
                $("#popupTarjetas").show();
            });

            $("#popupTarjetas_close").click(function () {
                $("#popupTarjetas").hide();
            });

            $(".miniaturavideo").click(function () {
                //Obtenemos el ancho del navegador.
                var width = $(window).width();

                //En funcion del ancho del navegador, calculamos la proporcion del video.
                if (width > 850) {
                    var anchovideo = "800px";
                    var altovideo = "600px";
                } else if (width > 350 && width < 850) {
                    var anchovideo = "350px";
                    var altovideo = "323px";
                } else {
                    var anchovideo = "300px";
                    var altovideo = "280px";
                }

                //Obtenemos la direcci&oacute;n
                var enlace = $(".video").find(".enlace").text();
                //Maquetamos el iframe
                var embed = "<iframe width='" + anchovideo + "' height='" + altovideo + "' src='" + enlace + "' frameborder='1' allowfullscreen></iframe><span class='close-popup'>X</span>";
                $("#cortina").show();
                $("#popupVideo").html(embed);
                $("#popupVideo").show();

                //Hacemos scroll hasta el video.
                $('html, body').animate({
                    scrollTop: $("#popupVideo").offset().top - 40
                }, 1000);

            });

            $(".video").click(function () {
                //Obtenemos el ancho del navegador.
                var width = $(window).width();

                //En funcion del ancho del navegador, calculamos la proporcion del video.
                if (width > 850) {
                    var anchovideo = "800px";
                    var altovideo = "600px";
                } else if (width > 350 && width < 850) {
                    var anchovideo = "350px";
                    var altovideo = "323px";
                } else {
                    var anchovideo = "300px";
                    var altovideo = "280px";
                }

                //Obtenemos la direcci&oacute;n
                var enlace = $(this).find(".enlace").text();
                //Maquetamos el iframe
                var embed = "<iframe width='" + anchovideo + "' height='" + altovideo + "' src='" + enlace + "' frameborder='1' allowfullscreen></iframe><span class='close-popup'>X</span>";
                $("#cortina").show();
                $("#popupVideo").html(embed);
                $("#popupVideo").show();

                //Hacemos scroll hasta el video.
                $('html, body').animate({
                    scrollTop: $("#popupVideo").offset().top - 40
                }, 1000);

            });

            //Al hacer click en la cortina eliminamos el contenido del iframe y lo ocultamos.
            $("#cortina").click(function () {

                $('body,html').animate({
                    scrollTop: 0
                }, 500);

                $("#popupVideo").html("");
                $("#popupVideo").hide();
                $(this).hide();

            });
            $(document).on("click", ".close-popup", function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 500);

                $("#popupVideo").html("");
                $("#popupVideo").hide();
                $("#cortina").hide();


            });

            /*Eventos para la navegaci&oacute;n*/
            $('#volverArriba').click(function () {

                $('body,html').animate({
                    scrollTop: 0
                }, 1000);
                return false;
            });
/*
            $(".sectiondos").click(function () {
                $('html, body').animate({
                    scrollTop: $("#sectiondos").offset().top + offset
                }, 1000);
            });

            $(".sectiontres").click(function () {
                $('html, body').animate({
                    scrollTop: $("#sectiontres").offset().top + offset
                }, 1000);
            });

            $(".sectioncuatro").click(function () {
                $('html, body').animate({
                    scrollTop: $("#sectioncuatro").offset().top + offset
                }, 1000);
            });

            $(".sectioncinco").click(function () {
                $('html, body').animate({
                    scrollTop: $("#sectioncinco").offset().top + offset
                }, 1000);
            });


            /*Men&uacute; Izquierda del producto (imagenes en miniatura)*/
          /*  $(".miniaturaImg img").click(function () {

                var img = "<img src='" + $(this).attr("src") + "' />";

                $(".leftImages").html(img);

            });

            $(".miniaturaImg img").hover(function () {
                var img = "<img src='" + $(this).attr("src") + "' />";

                $(".leftImages").html(img);

            });


            /*Sliders*/
            /*Videos*/
          /*  $('.slider').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: true,
                adaptiveHeight: true,
                autoplay: true,
                autoplaySpeed: 6000,

                responsive: [{
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            adaptiveHeight: true,
                            dots: true,
                            centerMode: false,

                            autoplay: true,
                            autoplaySpeed: 6000
                        }
                    }, {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            autoplay: true,
                            autoplaySpeed: 6000
                        }
                    }


                ]
            });*/

            /*Valoraciones*/
            $('.valoraciones').slick({
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 3000,
                slidesToShow: 1,
                speed: 400,
                arrows: false,
                cssEase: 'linear',
                dots: false
            });
            /*Valoraciones*/
            $('.carrousel').slick({
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 3000,
                speed: 400,
                slidesToShow: 1,
                arrows: false,
                cssEase: 'linear',
                dots: true
            });

        });
        document.addEventListener("DOMContentLoaded", function (event) {
            //do work
            $(".miniaturaImg").each(function (index) {
                $(this).hover(function () {
                    //alert(index);
                });
            });
        });

    </script>
{/literal}