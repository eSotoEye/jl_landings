{* TEST *}
{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
</div><!-- #center_column -->
{if isset($right_column_size) && !empty($right_column_size)}
    <div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
{/if}
</div><!-- .row -->
</div><!-- #columns -->
</div><!-- .columns-container -->
{if isset($HOOK_FOOTER)}
    <!-- Footer -->
    <div class="footer-container">
        <footer id="footer"  class="container">
            <div class="row">{$HOOK_FOOTER}
                <div class="cms row" style="width: 100%;margin: 15px 0px">
                    <div class="col-md-3 col-xs-6" style="margin: 5px 0px;"><a href="{$base_dir}content/6-politica-de-privacidad">Política de privacidad</a></div>
                    <div class="col-md-3 col-xs-6" style="margin: 5px 0px;"><a href="{$base_dir}content/1-devoluciones-y-garantia">Devoluciones y ganatía</a></div>
                    <div class="col-md-3 col-xs-6" style="margin: 5px 0px;"><a href="{$base_dir}content/3-condiciones-generales">Condiciones generales</a></div>
                    <div class="col-md-3 col-xs-6" style="margin: 5px 0px;"><a class="ultimoEnlace" href="{$base_dir}contactanos">Contactar con nosotros</a></div>
                </div>
            </div>
        </footer>
    </div><!-- #footer -->
{/if}
</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
<style>
    .footer-container {     background-color:white !important; }

    .ultimoEnlace:after{
        content:"" !important;
    }
    .cms{
        /*margin-top:20px;*/
        text-align:center;
    }
    {*  .cms div{
    padding:2px;
    }*}

    .cms div a{
        cursor: pointer;
        text-decoration:none;
        font-style:normal;
        color:black;
    }

    .cms div a:hover{
        color: #ff5000 !important;
    }


</style>

{literal}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149024594-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-149024594-1');
</script>
{/literal}

<link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
</body>
</html>