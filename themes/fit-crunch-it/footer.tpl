{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<footer id="footer"  class="container">
						<div class="row">{$HOOK_FOOTER}
							<div class="cms">
								<span><a href="http://fit-crunch.com/content/6-politica-de-privacidad">&nbsp;&nbsp;Privacy</a></span>
								<span><a href="http://fit-crunch.com/content/3-condiciones-generales">&nbsp;&nbsp;Condizioni generali</a></span>
								<span><a href="http://fit-crunch.com/content/7-politica-de-cookies">&nbsp;&nbsp;Cookies</a></span>
								<span><a href="http://fit-crunch.com/content/1-devoluciones-y-garantia">&nbsp;&nbsp;Invio e restituzione</a></span>
								<span><a class="ultimoEnlace" href="http://fit-crunch.com/contactanos">&nbsp;&nbsp;Servizio Clienti</a></span>
							</div>
						</div>
					</footer>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
<style>
	.footer-container {     margin-top: 50px; background-color:white !important; }
	.cms span a:after{
		content: "|";
		position: relative;
		left: 12px;
		
	}
	.ultimoEnlace:after{
		content:"" !important;
	}
	.cms{
		/*margin-top:20px;*/
		text-align:center;
	}
	.cms span{
		padding:2px;
	}

	.cms span a{
		text-decoration:none;
		font-style:normal;
		color:black;
	}
	
	.cms span a:hover{
		color: #ff5000 !important;
	}
	@media (max-width:540px)
	{
		.cms span{
			display:block;
			width:100%;
		}
	}

</style>
	</body>
</html>