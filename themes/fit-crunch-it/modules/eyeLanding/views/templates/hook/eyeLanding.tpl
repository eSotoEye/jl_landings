<link rel="stylesheet" href="{$module_dir}/slick/slick-theme.css">
<link rel="stylesheet" href="{$module_dir}/slick/slick.css">
<link rel="stylesheet" href="{$module_dir}style.css">
<script type="text/javascript" src="{$module_dir}/slick/slick.js"></script>

{literal}
	<style>
	

		
	</style>
{/literal}
<div style="clear:both;"></div>



<!-- SECCIÓN PRINCIPAL QUE CONTENDRÁ TODA LA INFORMACIÓN DEL PRODUCTO RESUMIDA -->
<div class="mainSection">
	
		<!-- Indice de imagenes -->
		<div class="leftImgIndex">
			<span id="miniaturavideo" class="miniaturavideo"><img src="modules/eyeLanding/images/video.png" style="width:100%;" /></span>
			<span id="miniatura1" class="miniaturaImg"><img src="modules/eyeLanding/images/1.png" style="width:100%;" /></span>
			<span id="miniatura2" class="miniaturaImg"><img src="modules/eyeLanding/images/2.png" style="width:100%;" /></span>
			<span id="miniatura3" class="miniaturaImg"><img src="modules/eyeLanding/images/3.png" style="width:100%;" /></span>
			 
		</div>
		<!-- Imágenes del producto-->
		<div class="leftImages">
			<img src="modules/eyeLanding/images/1.png" id="img1" />
		</div>
	
		<!-- Descipción del producto-->
		<div class="description">
		<div class="wrapper">
			<div class="productName">
				<span><h1 style="font-style:italic; font-weight:bold">TOTAL FIT CRUNCH</h1></span>
				<div class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			</div> 
			<div class="productSubName"><p>TONIFICA IL CORPO CON UN ESERCIZIO CARDIOVASCOLARE</p></div>
			<div class="productPrice"><span>79</span>,90&euro;</div> <span id="enviogratis">SPEDIZIONE GRATUITA</span>
			
			
			
				<ul style="">
					<li><i class="fa fa-check"></i>&nbsp;Utilizzando il Total Fit Crunch pochi minuti al giorno si otterrà un corpo perfetto con risultati visibili.</li>
					<li><i class="fa fa-check"></i>&nbsp;Si possono fare esercizi di bassa o alta resistenza con il peso del proprio corpo, si può anche lavorare sulle braccia, schiena, addominali, glutei e gambe con lo stesso esercizio.</li>
					<li><i class="fa fa-check"></i>&nbsp;Altezza e distanza adattabili con il sellino, facile da piegare, contatore digitale.<li>
					<li><i class="fa fa-check"></i>&nbsp;<h2>Attrezzatura domestica fitness al miglior prezzo.</h2><li>
				</ul>

			
		
			<p style="margin-top:20px;"><strong>Puoi pagare con:</strong> carta o Paypal. <a id="masinfoTarjeta" href="#">Maggiori informazioni</a></p>
			<div id="popupTarjetas"><img src="{$module_dir}/images/modalidades-de-pago.png" /> <span id="popupTarjetas_close">x</span></div>
			<p class="aviso" style="padding:10px;padding-left:0px; padding-top:0px;">
				<span><span style="color:#64A954; font-size:14px;font-weight:bold ">Invio Gratuito in 24-48H</span></span>
			</p>
			  
			
			<div style="clear:both;"></div>
	 
		<!-- Botones para la compra-->
		<div id="primerBoton"class="rightBuy col-lg-12">
			{*<p>Información adicional: este producto está sujeto a disponibilidad de stock etc etc....</p>*}
			<p id="add_to_cart" class="">
				<button  style="margin-left: 0px;"  onclick="validacionPedido(11,false);" name="Submit" >
					<span>Compra</span>
				</button>
			</p>
		</div>
		</div>
		</div>


	
	<div style="clear:both;"></div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo">
</div>
</div>

<!-- Descripción y modo de empleo -->
<div id="sectiondos" class="container">
		<span class="titulo" style="border: 1px solid black;padding: 10px; font-style:italic;">Pu&ograve; interessarti</span>
		<div class="wrapper">
			<div id="producto2">
					<div style=""  class="producto-img"><img src="{$module_dir}images/faja_slimcore.png"/></div>
					<div style=""  class="producto-desc">
							<p style="font-size:20px; border-bottom:1px solid black">ABS FASCIA <span style="font-style:italic"><b><!--Bonplus--></b></span></p>
							<div style="font-size: 24px; font-weight:bold;" class="productPrice2"><span>18</span>,90&euro;</div>
							<p class="descripcionProducto">Con questa fascia otterrai risultati visibili in meno tempo. Composta da 3 strati: 1­ Lo strato interno facilita la traspirazione che aiuterà a rimuovere le tossine e ridurre il volume. Lo strato intermedio è composto da milioni di microcapsule per assorbire il sudore. Lo strato esterno impedisce all'umidità di traspirare, evitando le macchie sui vestiti.</p>
	
								<div style="" class="combinaciones">
									<ul>
										<li><i class="fa fa-circle-thin combinacion2 combinacionActivada" data-pr="10" aria-hidden="true"></i>S</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="11" aria-hidden="true"></i>M</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="12" aria-hidden="true"></i>L</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="13" aria-hidden="true"></i>XL</li>
										<li><i class="fa fa-circle-thin combinacion2" data-pr="14"aria-hidden="true"></i>XXL</li>
									</ul>
							</div>
							<div  style="" class="rightBuy col-lg-12">
								<p id="add_to_cart" class="">
									<button style="margin-left:0px;" onclick="validacionPedido(13,$('.combinacion2.combinacionActivada').attr('data-pr'));" name="Submit" >
										<span>Compra</span>
									</button>
								</p>
							</div>
					</div>
					
			</div>
			 
			<div id="producto3">
					<div style="" class="producto-img"><img src="{$module_dir}images/faja_xtreme.png"/></div>
					<div style=""  class="producto-desc">
							<p style="font-size:20px; border-bottom:1px solid black">Xtreme Fascia <span style="font-style:italic"><b><!--Bonplus--></b></span> <!--Belt--></p>
							<div style="font-size: 24px; font-weight:bold;"class="productPrice2"><span>49</span>,90&euro;</div>
							<p class="descripcionProducto">Modella subito la tua figura. Ideale da indossare in ogni occasione per tutta la giornata. Agisce anche come correttore posturale, evitando lesioni mentre facciamo esercizi e il mal di schiena. Ha un effetto sauna e da un aspetto da un'apperenza di portare taglie in meno. Aumenta la sudorazione e intensifica gli effetti delle creme riducenti.</p>
						<div style="" class="combinaciones">
									<ul>
										<li><i class="fa fa-circle-thin combinacion1 combinacionActivada" data-pr="8" aria-hidden="true"></i>S-M</li>
										<li><i class="fa fa-circle-thin combinacion1" data-pr="9" aria-hidden="true"></i>L-XL</li>
									</ul>
							</div>
							<div  style="" class="rightBuy col-lg-12">
								<p id="add_to_cart" class="">
									<button style="margin-left:0px;" onclick="validacionPedido(12,$('.combinacion1.combinacionActivada').attr('data-pr'));" name="Submit" >
										<span>Compra</span>
									</button>
								</p>
							</div>
					</div>
			</div>
		</div>
		
		<div style="clear:both"></div>
		
</div>
{*<p style="text-align: center;
    padding-top: 18px;
    padding-bottom: 0px;
    font-size: 14px;">*Gastos de env&iacute;o gratis para pedidos superiores a 25€</p>*}
<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>

<!-- Descripción y modo de empleo -->
<div id="sectiontres" class="container">

		<span class="titulo"><h2 style="text-align: center;margin: 10px auto;font-size: 28px;display: block;">Descrizione e modo d'uso di TOTAL FIT CRUNCH</h2></span>
		<div style="" class="wrapper">
			<p style="font-size:18px; color:#ff5000; font-weight:bold; font-style:italic; text-align:center">Allena  i tuoi muscoli e tonificali con esercizi ad alta e bassa resistenza!</p>
			<p style="display:inline-block; width:100%; text-align:left;">
					Bonplus è una marca conosciuta per i suoi prodotti di alta qualità, possiede sul mercato la più completa gamma di attrezzi da ginnastica que occupano uno spazio minimo.
				
			</p>
			<p style="display:inline-block; width:100%; text-align:left;">
					Fit Crunch è il nuovo attrezzo ginnico che rassoda e tonifica tutto il corpo comodamente da casa, in sole 4 settimane vedrai gli incredibili risultati! Noterai un allenamento con minor sforzo che ti permetterà di sviluppare bicipidi, tricipidi, torace, addominali, dorsali, cosce, glutei e polpacci...e tutto comodamente in casa, mentre guardi la TV. Leggero da trasportare, si piega e si trasporta, in modo da occupare il minimo spazio una volta terminato l'esercizio.
				
			</p>
			<p style="display:inline-block; width:100%; text-align:left;">
				Incluso nella confezione del Fit Crunch si trova anche la guida dietetica,il manuale d'uso e una guida per poter seguire un'allenamento con esercizi specifici per ottimizzare l'allenamento.
			</p>
			<p style="display:inline-block; width:100%; text-align:left; padding-bottom:10px;">
			
			</p>
			<p id="especificacionesImagenes">
				<img class="izquierda" style="" src="{$module_dir}/images/descripcion_1.png" />
				<img class="derecha" style="" src="{$module_dir}/images/descripcion_2.png" />
				<img class="izquierda" style="" src="{$module_dir}/images/descripcion_3.png" />
				<img class="derecha" style="" src="{$module_dir}/images/descripcion_4IT.png" />
			</p>
			<div style="clear:both"></div>
			
				<div style="" class="especificaciones">
					<div style="    ">
						<ul>
							<li><strong>Esso include:</strong>
								<li> &nbsp;</li>
								<li>- Banco di esercizio Fit Crunch</li>
								<li>- Contatore digitale </li>
								<li>- Piano di dieta <br>&nbsp;(Lingue: inglese, francese, spagnolo, italiano e tedesco)</li>
								<li>- Manuale d'istruzioni d'uso e di montaggio con guida all'allenamento<br>&nbsp;(lingue: Inglese,Francese,Spagnolo,Italiano e Tedesco)</li>
								
							</ul>
						
					</div>	
					<!--<div style=" ">
						<ul>
							
						</ul>
					</div>-->

				</div>
				<div style="" class="especificaciones">

					<div style="  ">
							<ul>
								<li><strong>Dati tecnici:</strong>
								<li>  &nbsp;</li>
								<li>- Intensità alta e bassa resistenza</li>
								<li>- Sella regolabile per diverse altezze e varie distanze</li>
								<li>- Supporto fino a 100 chili</li>
								<li>- Pieghevole, facile da guardare</li>
								<li>- Dimensioni: * Aperto: 124 centimetri x 88 centimetri x 41 centimetri * Chiuso : 154cm x 26 cm</li>
								<li>- Materiale: Acciaio</li>
							</ul>	
					</div>
					<!--<div style="">
						<ul>
							
						</ul>
					</div>-->
				</div>			
			
		<div style="clear:both"></div>
		<h3> Approfitta della nostra offerta di Total Fit Crunch, oggi stesso</h3>
		<div class="rightBuy col-lg-12">
			{*<p>Información adicional: este producto está sujeto a disponibilidad de stock etc etc....</p>*}
			<p id="add_to_cart" class="">
				<button  onclick="validacionPedido(11,false);" name="Submit" >
					<span>Compra</span>
				</button>
			</p>
		</div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>

<!-- Videos -->
<div id="sectioncuatro" class="container">
	
		<span style=" " class="titulo">Video</span>
		<div class="wrapper">
		{*<div class="slider">*}
		
			<div class="video"><span class="enlace" style="display:none">https://www.youtube.com/embed/oM-0CkAO5CY</span><img src="{$module_dir}/images/video2.png" /></div>
			
			
		{*</div>*}
		</div>
		<div class="rightBuy col-lg-12">
			<p onclick="validacionPedido(11,false);" id="add_to_cart" class="">
				<button name="Submit" >
					<span>Compra</span>
				</button>
			</p>
		</div>
</div>

<div class="lineahorizontal">
	<div class="lineahorizontalTriangulo"></div>
</div>
 
  
<!-- Valoraciones -->

<div id="sectioncinco" class="container">
	
		<span class="titulo" style=" border: 1px solid black; padding: 10px;">Recensioni</span>
	<div class="wrapper valoraciones">
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Nicole 05/09/2016 </span>
			<div style="float:right; color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p>Esteticamente non mi piace molto, però mia nipote mi ha incoraggiato a usarlo e la verità è che aveva ragione. Il giorno seguente mi facevano male persino i lobi delle orecchie .</p>
		</div>
		 
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Mauro 07/09/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p> Eccellente servizio per qualità prodotto, prezzo e tempi di consegna</p>
		</div>
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Maria 12/09/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p>Ti permette di allenarti con sicurezza,il materiale è molto resistente.</p>
		</div>	
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Luciano 12/09/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p>Ho comprato il Fit Crunch ieri e mi è arrivato oggi, è stato molto facile da montare. MI sento molto soddisfatta del mio acquisto, lo uso tutti i giorni per tonificare le diverse parti del corpo. Dopo un mese i risultati sono evidenti. 100% raccomandato.</p>
		</div>				
		<div class="valoracion col-lg-5 col-sm-5 col-sm-12 col-xs-12">
			<span class="autor"> Aliona 14/09/2016 </span>
			<div style="float:right;  color:#ff5000;" class="puntuacion">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</div> 
			<p>L'avevo comprato a mio padre, perché non aveva voglia di fare molto esercizio, da quando l'ha ricevuto lo utilizza ogni giorno. La consegna è stata rapida e il prodotto molto è perfetto.</p>
		</div>		
				
	</div>
		<div style="clear:both"></div>

</div>

<div style="margin-top: 10px;" id="volverArriba">
	<p>Tornare su</p>
</div>

<div id="cortina"></div>
<div style="" id="popupVideo">

</div>
{* END-PAGINACION *}
	<form id="buy_block" class="hidden" action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
		<!-- hidden datas -->
		<p class="hidden">
			<input type="hidden" name="token" value="{$static_token}" />
			<input type="hidden" class="id_product" id="product_page_product_id" name="id_product" value="1" />
			<input type="hidden" name="add" value="1" />
			<input type="hidden" name="quantity_wanted" id="quantity_wanted" value="1" />
			<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
		</p>
	</form>
	
{literal}
<script type="text/javascript">
	function validacionPedido(id_del_producto,id_product_attribute){
		
		document.getElementById("product_page_product_id").value=id_del_producto;
		if (id_product_attribute!=false)
			document.getElementById("idCombination").value=id_product_attribute;
		else
			document.getElementById("idCombination").value="";
	}
	
	$(document).ready(function() {
		  var offset = -20; //Offset of 20px
		  $("#popupVideo").hide();

		 $(".combinacion1").click(function(){
			$(".combinacion1").removeClass("combinacionActivada");
			$(this).addClass("combinacionActivada");
		 });
		 
		 $(".combinacion2").click(function(){
			$(".combinacion2").removeClass("combinacionActivada");
			$(this).addClass("combinacionActivada");
		 });
		  $("#masinfoTarjeta").click(function(){
			$("#popupTarjetas").show();
		  });
		    
		  $("#popupTarjetas_close").click(function(){
			$("#popupTarjetas").hide();
		  });
		  
		  $(".miniaturavideo").click(function(){
				//Obtenemos el ancho del navegador.
			var width = $(window).width();
			
			//En funcion del ancho del navegador, calculamos la proporcion del video.
			if (width > 850) {
			  var anchovideo = "800px";
			  var altovideo = "600px";
			} else if (width > 350 && width < 850) {
			  var anchovideo = "350px";
			  var altovideo = "323px";
			} else {
			  var anchovideo = "300px";
			  var altovideo = "280px";
			}
			
			//Obtenemos la direcci&oacute;n
			var enlace = $(".video").find(".enlace").text();
			//Maquetamos el iframe
			var embed = "<iframe width='" + anchovideo + "' height='" + altovideo + "' src='" + enlace + "' frameborder='1' allowfullscreen></iframe><span class='close-popup'>X</span>";
			$("#cortina").show();
			$("#popupVideo").html(embed);
			$("#popupVideo").show();
			
			//Hacemos scroll hasta el video.
			$('html, body').animate({
			  scrollTop: $("#popupVideo").offset().top - 40
			}, 1000);
		  
		  });
		  
		  $(".video").click(function() {
			//Obtenemos el ancho del navegador.
			var width = $(window).width();
			
			//En funcion del ancho del navegador, calculamos la proporcion del video.
			if (width > 850) {
			  var anchovideo = "800px";
			  var altovideo = "600px";
			} else if (width > 350 && width < 850) {
			  var anchovideo = "350px";
			  var altovideo = "323px";
			} else {
			  var anchovideo = "300px";
			  var altovideo = "280px";
			}
			
			//Obtenemos la direcci&oacute;n
			var enlace = $(this).find(".enlace").text();
			//Maquetamos el iframe
			var embed = "<iframe width='" + anchovideo + "' height='" + altovideo + "' src='" + enlace + "' frameborder='1' allowfullscreen></iframe><span class='close-popup'>X</span>";
			$("#cortina").show();
			$("#popupVideo").html(embed);
			$("#popupVideo").show();
			
			//Hacemos scroll hasta el video.
			$('html, body').animate({
			  scrollTop: $("#popupVideo").offset().top - 40
			}, 1000);

		  });
		
		  //Al hacer click en la cortina eliminamos el contenido del iframe y lo ocultamos.
		  $("#cortina").click(function() {
		  
			$('body,html').animate({
			  scrollTop: 0
			}, 500);
			  
			$("#popupVideo").html("");
			$("#popupVideo").hide();
			$(this).hide();
			
		  });
		    $(document).on("click",".close-popup",function(){
				$('body,html').animate({
					scrollTop: 0
				}, 500);
			
				$("#popupVideo").html("");
				$("#popupVideo").hide();
				$("#cortina").hide();
				
			
		  });
		  
		  /*Eventos para la navegaci&oacute;n*/
		  $('#volverArriba').click(function() {

			$('body,html').animate({
			  scrollTop: 0
			}, 1000);
			return false;
		  });

		  $(".sectiondos").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectiondos").offset().top + offset
			}, 1000);
		  });

		  $(".sectiontres").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectiontres").offset().top + offset
			}, 1000);
		  });

		  $(".sectioncuatro").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectioncuatro").offset().top + offset
			}, 1000);
		  });

		  $(".sectioncinco").click(function() {
			$('html, body').animate({
			  scrollTop: $("#sectioncinco").offset().top + offset
			}, 1000);
		  });

		  
		  /*Men&uacute; Izquierda del producto (imagenes en miniatura)*/
		  $(".miniaturaImg img").click(function() {

			var img = "<img src='" + $(this).attr("src") + "' />";

			$(".leftImages").html(img);

		  });

		  $(".miniaturaImg img").hover(function() {
			var img = "<img src='" + $(this).attr("src") + "' />";

			$(".leftImages").html(img);

		  });

		  
		  /*Sliders*/
		  /*Videos*/
		  $('.slider').slick({
			slidesToShow: 3,
			slidesToScroll: 3,
			dots: true,
			adaptiveHeight: true,
			autoplay: true,
			autoplaySpeed: 6000,

			responsive: [{
				breakpoint: 1000,
				settings: {
				  slidesToShow: 2,
				  slidesToScroll: 2,
				  adaptiveHeight: true,
				  dots: true,
				  centerMode: false,

				  autoplay: true,
				  autoplaySpeed: 6000
				}
			  }, {
				breakpoint: 800,
				settings: {
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  autoplay: true,
				  autoplaySpeed: 6000
				}
			  }


			]
		  });
 
		  /*Valoraciones*/
		  $('.valoraciones').slick({
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			slidesToShow: 2,
			cssEase: 'linear',
			responsive: [{
			  breakpoint: 1000,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 6000
			  }
			}]
		  });

	});
document.addEventListener("DOMContentLoaded", function(event) { 
	//do work
	$(".miniaturaImg").each(function(index){
		$(this).hover(function(){
			//alert(index);
		});
	});
});

</script>
{/literal}