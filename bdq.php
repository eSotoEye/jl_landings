<?php

/*	
 *	FV Este script sirve para realizar consultas de tipo SELECT a la base de datos de Prestashop
 *	ADVERTENCIA: S�lo SELECT t s�lo desde la oficina.
 *				 Modificar este archivo para realizar cualquier tipo de consulta es muy peligroso.
 *
 *	@param	k	Clave de acceso
 *	@param	q	Consulta a realizar
 */

error_reporting(E_ALL);

include(dirname(__FILE__).'/config/config.inc.php');

const ACCESS_KEY = 'alkwejsnDLjsdn1cSDask';

/* 												*
 *	Envia los email de aviso de error			*
 *	(Por claridad en el c�digo fuente)			*
 *												*/
function enviarEmail($errorCode, $consulta, $ipVis){
	
	// Configuracion de los correos
	$m_para = "soporte3@eyeinversionesonline.com";
	$m_titulo = "BDQ: Consulta a la base de datos de Crunch";
	$m_mensaje = "Consulta realizada:";
	$m_cabeceras = 'From: auto_crunch@eyeinversionesonline.com' . "\r\n" .
		'Reply-To: soporte3@eyeinversionesonline.com' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
	
	switch($errorCode){
		case 0:
			$m_titulo = "Error en consulta Crunch: intento de UPDATE/INSERT/DELETE";
			$m_mensaje = "Se ha intentado realizar una consulta que no es un SELECT. La consulta es: <br>\n";
			$m_mensaje .= " $consulta <br/><br/>\n Desde la IP: " . $ipVis . " el d�a " . date('Y/m/d h:i:s a', time());
		break;
		case 1:
			$m_titulo = "Error en consulta Crunch: intento desde fuera de la oficina";
			$m_mensaje = "Se ha intentado realizar una consulta desde fuera de la oficina. La consulta es: <br>\n";
			$m_mensaje .= " $consulta <br/><br/>\n Desde la IP: " . $ipVis . " el d�a " . date('Y/m/d h:i:s a', time());
		break;
		default:
			$m_titulo = "Error en consulta Crunch";
			$m_mensaje = "Se ha intentado realizar una consulta a BD desde bdq.php. Error por defecto. ";
			$m_mensaje .= "CONSULTA:<br/>$consulta <br/>IP: " . $ipVis . " <br/>FECHA: " . date('Y/m/d h:i:s a', time());
	}
	mail($m_para, $m_titulo, $m_mensaje, $m_cabeceras);
}

/*
 *	Devuelve true si es una consulta v�lida de SQL, es decir, una consulta SELECT �nica
 */
function is_sql_valid_query($consulta){
	$consulta = strtolower($consulta);
	if( strpos($consulta, "delete") !== false || strpos($consulta, "update") !== false || strpos($consulta, "insert") !== false ){
		// Esta consulta no ser�a v�lida
		return false;
	}else if( strpos($consulta,"select") !== false ){
		return true;
	}
	return false;
}


//---------------------------------------------------------------------------------------

// Comprobamos que el c�digo que nos mandan es correcto
if( !isset( $_POST['k'] ) || empty( $_POST['k'] ) || $_POST['k'] != ACCESS_KEY ){
	echo "Error: Acceso denegado";
	exit();
}else{
	
	// Primero obtenemos la ip del visitante y la comparamos con la ip de la oficina
	$ipVisitante = $_SERVER['REMOTE_ADDR'];
	$ipOficina = gethostbyname("eyeinversiones.ddns.net");
	
	// S�lo est� permitido este m�todo de pago desde la Oficina
	//if($ipVisitante == $ipOficina){
		
		// Filtramos la consulta. Se espera un SELECT, SIN ning�n otro tipo de consulta (INSERT/UPDATE/DELETE)
		$consulta = $_POST['q'];
		
		//Se cambia a la funcion is_numeric por que filter_var no interpretaba los codigos que empezar�n por 0
		if( is_sql_valid_query($consulta) !== false ){
			$res = Db::getInstance()->ExecuteS($consulta);
			echo json_encode(array("data" => $res, "shop" => "CRUNCH"));
			// WARNING: La URL puede cambiar en funci�n de la web
		}else{
			echo "Error 2: Ser� reportado al administrador.";
			enviarEmail(0, $consulta, $ipVisitante);
			exit();
		}
	/*
	}else{
		// Si se intenta entrar desde fuera de la oficina, avisamos por email y enviamos la IP del cliente que ha intentado entrar
		echo json_encode(array("u" => "Error: Ser� reportado al administrador.", "veredict" => "error"));
		enviarEmail(1, $consulta, $ipVisitante);
		exit();
	}*/
}
exit();
?>